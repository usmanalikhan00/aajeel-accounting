var { app, BrowserWindow, ipcMain } = require('electron');
var url = require('url');
var path = require('path');
var print_win;
let win = null;

const electron = require('electron')
// const BrowserWindow = electron.BrowserWindow
const Menu = electron.Menu

process.setMaxListeners(0);

// app.require('main.css')

app.on('ready', () => {
 
    win = new BrowserWindow({ width: 1000, height: 600 });
     
    // win.loadURL('http://localhost:4200');

    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
   
    win.on('closed', () => {
        win = null;
    })
   
    // win.webContents.openDevTools();
})
 
app.on('activate', () => {
    if (win == null)
    createWindow()    
})
 
app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        app.quit();
    }
})

ipcMain.on('loadPrintPage', (event, arg) => {
    let invoiceId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printsingleinvoice/"+invoiceId+"'");
    });
});

ipcMain.on('InventoryLedgerPrint', (event, arg) => {
    let productId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/inventoryledger/"+productId+"'");
    });
});

ipcMain.on('PurchasePrint', (event, arg) => {
    let purchaseId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchase/"+purchaseId+"'");
    });
});

ipcMain.on('InwardpassPrint', (event, arg) => {
    let inwardpassId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinwardpass/"+inwardpassId+"'");
    });
});

ipcMain.on('OutwardpassPrint', (event, arg) => {
    let outwardpassId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printoutwardpass/"+outwardpassId+"'");
    });
});

ipcMain.on('CustomerLedgerPrint', (event, arg) => {
    let customerId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/customerledger/"+customerId+"'");
    });
});

ipcMain.on('InvoiceReturnPrint', (event, arg) => {
    let invoicereturnId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinvoicereturn/"+invoicereturnId+"'");
    });
});

ipcMain.on('PurchaseReturnPrint', (event, arg) => {
    let purchasereturnId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchasereturn/"+purchasereturnId+"'");
    });
});

ipcMain.on('PurchasesPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchases/'");
    });
});

ipcMain.on('InvoicesPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinvoices/'");
    });
});

ipcMain.on('ProductsPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printproducts/'");
    });
});

ipcMain.on('CustomersPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printcustomers/'");
    });
});



ipcMain.on('PaymentPrint', (event, arg) => {
    let paymentId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpayment/"+paymentId+"'");
    });
});

ipcMain.on('ReceiptPrint', (event, arg) => {
    let receiptId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printreceipt/"+receiptId+"'");
    });
});

ipcMain.on('AccountLedgerPrint', (event, arg) => {
    let accountId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/accountsledger/"+accountId+"'");
    });
});






ipcMain.on('Print', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintPayment', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1,
  });
});

ipcMain.on('PrintReceipt', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintPurchase', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1,
  });
});

ipcMain.on('PrintInwardpass', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintOutwardpass', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintInventoryLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});



ipcMain.on('PrintAccountLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintPurchaseReturn', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintInvoiceReturn', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintCustomers', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});




ipcMain.on('PrintInvoices', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintPurchases', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintProducts', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintCustomerLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


// THIS IS MENU CODE

let template = [{
  label: 'Edit',
  submenu: [{
    label: 'Undo',
    accelerator: 'CmdOrCtrl+Z',
    role: 'undo'
  }, {
    label: 'Redo',
    accelerator: 'Shift+CmdOrCtrl+Z',
    role: 'redo'
  }, {
    type: 'separator'
  }, {
    label: 'Cut',
    accelerator: 'CmdOrCtrl+X',
    role: 'cut'
  }, {
    label: 'Copy',
    accelerator: 'CmdOrCtrl+C',
    role: 'copy'
  }, {
    label: 'Paste',
    accelerator: 'CmdOrCtrl+V',
    role: 'paste'
  }, {
    label: 'Select All',
    accelerator: 'CmdOrCtrl+A',
    role: 'selectall'
  }]
}, {
  label: 'View',
  submenu: [{
    label: 'Reload',
    accelerator: 'CmdOrCtrl+R',
    click: function (item, focusedWindow) {
      if (focusedWindow) {
        // on reload, start fresh and close any old
        // open secondary windows
        if (focusedWindow.id === 1) {
          BrowserWindow.getAllWindows().forEach(function (win) {
            if (win.id > 1) {
              win.close()
            }
          })
        }
        focusedWindow.reload()
      }
    }
  }, {
    label: 'Toggle Full Screen',
    accelerator: (function () {
      if (process.platform === 'darwin') {
        return 'Ctrl+Command+F'
      } else {
        return 'F11'
      }
    })(),
    click: function (item, focusedWindow) {
      if (focusedWindow) {
        focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
      }
    }
  }, {
    label: 'Toggle Developer Tools',
    accelerator: (function () {
      if (process.platform === 'darwin') {
        return 'Alt+Command+I'
      } else {
        return 'Ctrl+Shift+I'
      }
    })(),
    click: function (item, focusedWindow) {
      if (focusedWindow) {
        focusedWindow.toggleDevTools()
      }
    }
  }, {
    type: 'separator'
  }, {
    label: 'App Menu Demo',
    click: function (item, focusedWindow) {
      if (focusedWindow) {
        const options = {
          type: 'info',
          title: 'Application Menu Demo',
          buttons: ['Ok'],
          message: 'This demo is for the Menu section, showing how to create a clickable menu item in the application menu.'
        }
        electron.dialog.showMessageBox(focusedWindow, options, function () {})
      }
    }
  }]
}, {
  label: 'Window',
  role: 'window',
  submenu: [{
    label: 'Minimize',
    accelerator: 'CmdOrCtrl+M',
    role: 'minimize'
  }, {
    label: 'Close',
    accelerator: 'CmdOrCtrl+W',
    role: 'close'
  }, {
    type: 'separator'
  }, {
    label: 'Reopen Window',
    accelerator: 'CmdOrCtrl+Shift+T',
    enabled: false,
    key: 'reopenMenuItem',
    click: function () {
      app.emit('activate')
    }
  }]
}, {
  label: 'Help',
  role: 'help',
  submenu: [{
    label: 'Learn More',
    click: function () {
      electron.shell.openExternal('http://electron.atom.io')
    }
  }]
}]

function addUpdateMenuItems (items, position) {
  if (process.mas) return

  const version = electron.app.getVersion()
  let updateItems = [{
    label: `Version ${version}`,
    enabled: false
  }, {
    label: 'Checking for Update',
    enabled: false,
    key: 'checkingForUpdate'
  }, {
    label: 'Check for Update',
    visible: false,
    key: 'checkForUpdate',
    click: function () {
      require('electron').autoUpdater.checkForUpdates()
    }
  }, {
    label: 'Restart and Install Update',
    enabled: true,
    visible: false,
    key: 'restartToUpdate',
    click: function () {
      require('electron').autoUpdater.quitAndInstall()
    }
  }]

  items.splice.apply(items, [position, 0].concat(updateItems))
}

function findReopenMenuItem () {
  const menu = Menu.getApplicationMenu()
  if (!menu) return

  let reopenMenuItem
  menu.items.forEach(function (item) {
    if (item.submenu) {
      item.submenu.items.forEach(function (item) {
        if (item.key === 'reopenMenuItem') {
          reopenMenuItem = item
        }
      })
    }
  })
  return reopenMenuItem
}

if (process.platform === 'darwin') {
  const name = electron.app.getName()
  template.unshift({
    label: name,
    submenu: [{
      label: `About ${name}`,
      role: 'about'
    }, {
      type: 'separator'
    }, {
      label: 'Services',
      role: 'services',
      submenu: []
    }, {
      type: 'separator'
    }, {
      label: `Hide ${name}`,
      accelerator: 'Command+H',
      role: 'hide'
    }, {
      label: 'Hide Others',
      accelerator: 'Command+Alt+H',
      role: 'hideothers'
    }, {
      label: 'Show All',
      role: 'unhide'
    }, {
      type: 'separator'
    }, {
      label: 'Quit',
      accelerator: 'Command+Q',
      click: function () {
        app.quit()
      }
    }]
  })

  // Window menu.
  template[3].submenu.push({
    type: 'separator'
  }, {
    label: 'Bring All to Front',
    role: 'front'
  })

  addUpdateMenuItems(template[0].submenu, 1)
}

if (process.platform === 'win32') {
  const helpMenu = template[template.length - 1].submenu
  addUpdateMenuItems(helpMenu, 0)
}

app.on('ready', function () {
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
})

app.on('browser-window-created', function () {
  let reopenMenuItem = findReopenMenuItem()
  if (reopenMenuItem) reopenMenuItem.enabled = false
})

app.on('window-all-closed', function () {
  let reopenMenuItem = findReopenMenuItem()
  if (reopenMenuItem) reopenMenuItem.enabled = true
})

app.on('uncaughtException', function (err) {
  console.log("***WHOOPS TIME****"+err);
});