import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class InwardPassService {
  
  authenticatedUser: any = [];
  public inwardPassDB = new PouchDB('aajeelaccinwardpass');
  

  constructor(private _router: Router){}

  addInwardpass(inwardpassDoc){
    console.log("INWARD PASS DOCUMENT TO PUT:-----", inwardpassDoc)
    var self = this;
    return self.inwardPassDB.put({
        _id: new Date().toISOString(),
        inwardpassnumber: inwardpassDoc.inwardpassnumber, 
        inwardpasstotal: inwardpassDoc.inwardpasstotal, 
        inwardpassproducts: inwardpassDoc.inwardpassproducts, 
        inwardpasscustomer: inwardpassDoc.inwardpasscustomer, 
        status: inwardpassDoc.status, 
        createdby: inwardpassDoc.createdby, 
        createdat: inwardpassDoc.createdat,
        converted: inwardpassDoc.converted,
        inwardpassnotes: inwardpassDoc.inwardpassnotes
    });
  }

  allInwardpass(){
    var self = this;
    return self.inwardPassDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleInwardpass(inwardpassId){
    var self = this;
    return self.inwardPassDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.inwardPassDB.find({
        selector: {_id:{$eq: inwardpassId}}
      });
    })
  }

  updateInwardpass(inwardpassDoc){
    var self = this
    return self.inwardPassDB.put(inwardpassDoc);    
  }

}