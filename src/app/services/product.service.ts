import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// PouchDB.plugin(require('pouchdb-upsert'))
// import * as moment from "moment";
PouchDB.setMaxListeners(50)

@Injectable()
export class ProductService {
  
  authenticatedUser: any = [];
  public productsDB = new PouchDB('aajeelaccproducts');
  public productCategoriesDB = new PouchDB('aajeelaccproductcategories');
  public productSubCategoriesDB = new PouchDB('aajeelaccproductsubcategories');

  constructor(private _router: Router){
  }

  addProduct(values){
    var self = this;
    return self.productsDB.put(values);
  }

  remvoeProduct(product){
    var self = this;
    return self.productsDB.remove(product);
  }



  allProducts(){
    var self = this;
    return self.productsDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }

  addProductCategory(name){
    var self = this;  
    return self.productCategoriesDB.put({
      _id: new Date().toISOString(),
      name: name
    })
  }

  addProductSubCategory(name, catgeoryId){
    var self = this;  
    return self.productSubCategoriesDB.put({
      _id: new Date().toISOString(),
      name: name,
      categoryId: catgeoryId
    })
  }

  
  allProductCategories(){
    var self = this;
    return self.productCategoriesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }
  
  allProductSubCategories(){
    var self = this;
    return self.productSubCategoriesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }
  


  getSingleCategory(categoryId){
    var self = this;
    return self.productCategoriesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.productCategoriesDB.find({
        selector: {_id:{$eq: categoryId}}
      });
    })
  }



  getSingleCategoryItems(categoryId){
    var self = this;
    return self.productSubCategoriesDB.createIndex({
      index: {
        fields: ['categoryId']
      }
    }).then(function(){
      return self.productSubCategoriesDB.find({
        selector: {categoryId: {$eq: categoryId}}
      });
    })
  }

  updateInvoiceProducts(products){
    var self = this
    // var docsToUpdate: any = []
    // for (let item of products){
    //   var object = {
    //     '_id': item._id,
    //     '_rev': item._rev,
    //     'name': item.name,
    //     'batchnumber': item.batchnumber,
    //     'weight': item.weight,
    //     'price': item.price,
    //     'category': item.category,
    //     'categoryname': item.categoryname,
    //     'stockvalue': item.stockvalue,
    //     'type': item.type,
    //     'subcategory': item.subcategory,
    //     'stores': item.stores,
    //     'storeStocks': item.storeStocks,
    //     'netweight': item.netweight,
    //     'networth': item.networth
    //   }
    //   docsToUpdate.push(object)
    // }
    return self.productsDB.bulkDocs(products)
  }

  updateConvertedInvoiceProducts(products){
    var self = this
    return self.productsDB.bulkDocs(products)
  }

  saveEditProduct(product){
    var self = this
    return self.productsDB.put(product)
  }




  getSingleProduct(productId){
    var self = this;
    return self.productsDB.createIndex({
      index: {
        fields: ['categoryId']
      }
    }).then(function(){
      return self.productsDB.find({
        selector: {_id: {$eq: productId}}
      });
    })
  }

}