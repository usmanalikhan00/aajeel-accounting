import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class InvoiceReturnService {
  
  authenticatedUser: any = [];
  public invoiceReturnDB = new PouchDB('aajeelaccinvoicereturn');
  

  constructor(private _router: Router){}

  addInvoiceReturn(invoiceDoc){
    console.log("INVOICE RETURN DOCUMENT TO PUT IN SERVICE:-----", invoiceDoc)
    var self = this;
    return self.invoiceReturnDB.put({
        _id: new Date().toISOString(),
        invoicereturnnumber: invoiceDoc.invoicereturnnumber, 
        invoicereturntotal: invoiceDoc.invoicereturntotal, 
        invoicereturnproducts: invoiceDoc.invoicereturnproducts, 
        invoicereturncustomer: invoiceDoc.invoicereturncustomer, 
        invoicereturncustomerid: invoiceDoc.invoicereturncustomerid, 
        status: invoiceDoc.status, 
        createdby: invoiceDoc.createdby, 
        createdat: invoiceDoc.createdat,
        invoicereturnnotes: invoiceDoc.invoicereturnnotes,
        invoiceref:invoiceDoc.invoiceref, 
        invoicerefid:invoiceDoc.invoicerefid
    });
  }

  allInvocieReturns(){
    var self = this;
    return self.invoiceReturnDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleInvoiceReturn(invoiceReturnId){
    var self = this;
    return self.invoiceReturnDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.invoiceReturnDB.find({
        selector: {_id:{$eq: invoiceReturnId}}
      });
    })
  }

  stockItemDetails(product){
    // PouchDB.debug.enable('pouchdb:find')
    var self = this;
    return self.invoiceReturnDB.createIndex({
      index: {
        fields: ['invoiceproducts']
      }
    }).then(function(){
      return self.invoiceReturnDB.find({
        selector: {
          invoiceproducts:{
            $elemMatch:{_id:{$eq:product._id}}
          }
        }
      });
    })
  }

  customerInvoices(customer){
    // PouchDB.debug.enable('pouchdb:find')
    console.log("CUSTOMER WHOSE INVOOICES WE WANT TO GET IN SERVCIEL_-----", customer)
    var self = this;
    return self.invoiceReturnDB.createIndex({
      index: {
        fields: ['invoicecustomerid']
      }
    }).then(function(){
      return self.invoiceReturnDB.find({
        selector: {
          invoicecustomerid:{$eq:customer._id}
        }
      });
    })
  }




}