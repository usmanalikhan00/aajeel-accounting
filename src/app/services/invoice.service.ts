import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class InvoiceService {
  
  authenticatedUser: any = [];
  public invoicesDB = new PouchDB('aajeelaccinvoice');
  

  constructor(private _router: Router){}

  addInvoice(invoiceDoc){
    console.log("INVOICE DOCUMENT TO PUT:-----", invoiceDoc)
    var self = this;
    return self.invoicesDB.put(invoiceDoc);
  }

  allInvocies(){
    var self = this;
    return self.invoicesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleInvoice(invoiceId){
    var self = this;
    return self.invoicesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.invoicesDB.find({
        selector: {_id:{$eq: invoiceId}}
      });
    })
  }

  stockItemDetails(product){
    // PouchDB.debug.enable('pouchdb:find')
    var self = this;
    return self.invoicesDB.createIndex({
      index: {
        fields: ['invoiceproducts']
      }
    }).then(function(){
      return self.invoicesDB.find({
        selector: {
          invoiceproducts:{
            $elemMatch:{_id:{$eq:product._id}}
          }
        }
      });
    })
  }

  customerInvoices(customer){
    // PouchDB.debug.enable('pouchdb:find')
    console.log("CUSTOMER WHOSE INVOOICES WE WANT TO GET IN SERVCIEL_-----", customer)
    var self = this;
    return self.invoicesDB.createIndex({
      index: {
        fields: ['invoicecustomerid']
      }
    }).then(function(){
      return self.invoicesDB.find({
        selector: {
          invoicecustomerid:{$eq:customer._id}
        }
      });
    })
  }

  invoicesTotals() {
    var self = this
  }

  filterInvoices(toDate, fromDate){
    var self = this;
    console.log(fromDate, toDate)
    return self.invoicesDB.createIndex({
      index: {
        fields: ['_id', 'createdat']
      }
    }).then(function(){
      return self.invoicesDB.find({
        selector: { 
          $and: [
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $lt: toDate
              }
            },
            {
              _id:{
                $gte: fromDate
              }
            }

          ]
        },
        sort: [{_id:'asc'}]
      })
    })
  }

}