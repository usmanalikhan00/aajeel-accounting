import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class CustomersService {
  
  selectedCustomer: any = [];
  public customersDB = new PouchDB('aajeelacccustomers');
  // public productCategoriesDB = new PouchDB('productcategories');
  // public productSubCategoriesDB = new PouchDB('productsubcategories');

  constructor(private _router: Router){}

  addCustomer(values){
    var self = this;
    return self.customersDB.put({
      _id: new Date().toISOString(),
      name: values.name,
      address: values.address,
      email: values.email,
      phone: values.phone,
      fax: values.fax,
      status: values.status,
      obtype: values.obtype,
      openingbalance: values.openingbalance,
      closingbalance: values.closingbalance
    });
  }

  allCustomers(){
    var self = this;
    return self.customersDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  updateCustomerBalance(customerDoc){
    var self = this
    return self.customersDB.put(customerDoc)
  }

  updateCustomers(allCustomers){
    var self = this
    return self.customersDB.bulkDocs(allCustomers)
  }
  

  getSingleCustomer(customerId){
    var self = this;
    return self.customersDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.customersDB.find({
        selector: {_id:{$eq: customerId}}
      });
    })
  }


  

}