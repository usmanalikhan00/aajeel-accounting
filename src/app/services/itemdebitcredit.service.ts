import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class ItemDebitCreditService {
  
  authenticatedUser: any = [];
  public itemDebitCreditDB = new PouchDB('aajeelaccitemdebitcredit');
  

  constructor(private _router: Router){}

  addDebitCredit(products){
    console.log("PRODUCTS FOR DEBIT CREDIT:-----", products)
    var self = this;
    return self.itemDebitCreditDB.bulkDocs(products)
  }
  
  getDebitCredits(product){
    var self = this;
    return self.itemDebitCreditDB.createIndex({
      index: {
        fields: ['productid']
      }
    }).then(function(){
      return self.itemDebitCreditDB.find({
        selector: {
          productid:{
            $eq:product._id
          }
        }
      });
    })
  }

  printDebitCredits(product){
    var self = this;
    return self.itemDebitCreditDB.createIndex({
      index: {
        fields: ['createdat', 'productid']
      }
    }).then(function(){
      return self.itemDebitCreditDB.find({
        selector: { 
          $and: [
            {
              productid:{ 
                $eq: product._id
              }
            },
            {
              createdat:{
                $exists:true
              }
            },
            {
              createdat:{
                $gt:null
              }
            }
          ]
        },
        sort: [{createdat:'asc'}]
      })
    })
  }



}