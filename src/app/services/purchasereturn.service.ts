import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class PurchaseReturnService {
  
  authenticatedUser: any = [];
  public purchaseReturnDB = new PouchDB('aajeelaccpurchasereturn');
  

  constructor(private _router: Router){}

  addPurchaseReturn(purchaseDoc){
    console.log("INVOICE RETURN DOCUMENT TO PUT IN SERVICE:-----", purchaseDoc)
    var self = this;
    return self.purchaseReturnDB.put(purchaseDoc);
  }

  allPurchaseReturns(){
    var self = this;
    return self.purchaseReturnDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSinglePurchaseReturn(invoiceReturnId){
    var self = this;
    return self.purchaseReturnDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.purchaseReturnDB.find({
        selector: {_id:{$eq: invoiceReturnId}}
      });
    })
  }

  stockItemDetails(product){
    // PouchDB.debug.enable('pouchdb:find')
    var self = this;
    return self.purchaseReturnDB.createIndex({
      index: {
        fields: ['invoiceproducts']
      }
    }).then(function(){
      return self.purchaseReturnDB.find({
        selector: {
          invoiceproducts:{
            $elemMatch:{_id:{$eq:product._id}}
          }
        }
      });
    })
  }

  customerPurchases(customer){
    // PouchDB.debug.enable('pouchdb:find')
    console.log("CUSTOMER WHOSE INVOOICES WE WANT TO GET IN SERVCIEL_-----", customer)
    var self = this;
    return self.purchaseReturnDB.createIndex({
      index: {
        fields: ['invoicecustomerid']
      }
    }).then(function(){
      return self.purchaseReturnDB.find({
        selector: {
          purchasecustomerid:{$eq:customer._id}
        }
      });
    })
  }




}