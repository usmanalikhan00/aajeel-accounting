import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { OutwardPassService } from '../../services/outwardpass.service'
import { InwardPassService } from '../../services/inwardpass.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { CustomersService } from '../../services/customers.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from "moment";
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'convert-inward',
    providers: [LoginService, 
                ProductService,
                InvoiceService, 
                InwardPassService, 
                PurchaseService, 
                StoreSettingsService, 
                ItemDebitCreditService, 
                CustomerDebitCreditService, 
                CustomersService, 
                OutwardPassService],
    templateUrl: 'convertinward.html',
    styleUrls: ['convertinward.css']
})

export class convertinward {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  inwardpassId: any;
  selectedInward: any;
  cartProducts: any[] = []
  cartTotal: any = 0
  purchaseNotes: any;
  invoiceProducts: any = []

  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;

  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  
  cartStates: Observable<any[]>;
  customerToUpdate: any = null
  customerDebitCredit: any = null
  authUser: any = null
  
  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _outwardPassService: OutwardPassService, 
              private _inwardPassService: InwardPassService, 
              private _purchaseService: PurchaseService, 
              private _storeSettingsService: StoreSettingsService,
              private _customersService: CustomersService,
              private _invoiceService: InvoiceService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }

  ngOnInit(){
    var self = this;
    // this._activatedRouter.paramMap
    //   .switchMap((params: ParamMap) => )
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.inwardpassId = params['inwardpassId']; // (+) converts string 'id' to a number
      console.log("SELECTED inWARD NUMBER:-----", self.inwardpassId)
      self.getInwardpass()
      self.getCartProducts()
      self.getAllStores()
      self.getAllCustomers()
    });
  }
  
  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });

      self.customers = []
      for (let item of self.allCustomers){
        
          var object = {
            "name":item.name,
            "_id":item._id,
            "_rev":item._rev,
            "address":"",
            "phone":"",
            "email":"",
            "balance":item.balance
          }          

        self.customers.push(object)
      }
      self.customerStates = self.addCustomerForm.controls['customerCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.customers.slice());
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }
  
  getAllStores(){
    var self = this
    self.allStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStores(state) : self.allStores.slice());
      console.log("ALL Stores:=====", self.allStores);
    }).catch(function(err){

    })
  }
  
  getInwardpass(){
    var self = this
    self._inwardPassService.getSingleInwardpass(self.inwardpassId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedInward = result.docs[0]
      console.log("RESULT FROM SINLE GATE inWARD:------", self.selectedInward)
      self.cartProducts = self.selectedInward.inwardpassproducts
      self.calculateTotal(self.cartProducts)
      self.getAllProducts()
    }).catch(function(err){
        console.log(err)
    })
  }

  getAllProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
      });
      console.log("ALL PRODUCTS:=====", self.invoiceProducts);
    }).catch(function(err){
      console.log(err);
    })
  }

  getCartProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
      });
      // console.log("ALL PRODUCTS:=====", self.invoiceProducts);
      self.states = [];
      for (let item of self.invoiceProducts){
        
          var object = {
            "name":item.name,
            "weight":item.weight,
            "batchnumber":item.batchnumber,
            "stockvalue":item.stockvalue,
            "storeStocks":item.storeStocks,
            "_id":item._id,
            "_rev":item._rev,
            "category":item.category,
            "subcategory":item.subcategory,
            "price":item.price,
            "cartweight":0,
            "calcweight":0,
            "type":item.type,
            "stores":item.stores,
            "cartstores":"",
            "cartquantity":0,
            "cartprice":0
          }          

        self.states.push(object)
      }
      console.log("ALL PRODUCTS:=====", self.states);
      self.filteredStates = self.addInvoiceForm.controls['stateCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStates(state) : self.states.slice());
    }).catch(function(err){
      console.log(err);
    })
  }


  selectCartProduct(item){
    // console.log("AUTO SELCT CHANGE FUNCTION", item)
    this.selecteProduct(item)
    this._buildAddInvoiceForm()

  }

  selecteProduct(product){
    console.log("CHANGE INPUT VALUE SELECED:_+_", product)
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.cartquantity = product.cartquantity + 1
        if (product.type == 'Weighted'){
          // product.calcweight = product.cartquantity * product.cartweight
          product.cartweight = product.calcweight/product.cartquantity
          product.cartworth = product.cartquantity * product.cartweight * product.cartprice
        }else{
          product.calcweight = null
          product.cartworth = product.cartquantity * product.cartprice
        }
        this.cartProducts.push(product)
        this.cartProducts = _.reverse(this.cartProducts)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].cartquantity = this.cartProducts[i].cartquantity + 1
            if (this.cartProducts[i].type == 'Weighted'){
              // this.cartProducts[i].calcweight = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight
              this.cartProducts[i].cartweight = this.cartProducts[i].calcweight/this.cartProducts[i].cartquantity
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight * this.cartProducts[i].cartprice
            }else{
              this.cartProducts[i].calcweight = null
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.cartquantity = product.cartquantity + 1
          if (product.type == 'Weighted'){
            // product.calcweight = product.cartquantity * product.cartweight
            product.cartweight = product.calcweight/product.cartquantity
            product.cartworth = product.cartquantity * product.cartweight * product.cartprice
          }else{
            product.calcweight = null
            product.cartworth = product.cartquantity * product.cartprice
          }
          this.cartProducts.push(product)
          this.cartProducts = _.reverse(this.cartProducts)
          this.calculateTotal(this.cartProducts)
        }
      }

      this._buildAddInvoiceForm()
      this.getCartProducts()
    }
    
  }

  addPurchase(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    var ID = Math.floor(Math.random()*1000) + 100000
    self.selectedInward.inwardpasscustomer.balance = self.cartTotal
    var purchaseDoc = {
      "_id": new Date().toISOString(),
      "purchasenumber": ID, 
      "purchasetotal": self.cartTotal, 
      "purchaseproducts": self.cartProducts, 
      "purchasecustomer": self.selectedInward.inwardpasscustomer, 
      "purchasecustomerid": self.selectedInward.inwardpasscustomer._id, 
      "status": null, 
      "createdby": user, 
      "createdat": moment().format(),
      "purchasenotes":self.purchaseNotes 
    }
    console.log("PURCHASE DOC TO PUSH:=====", purchaseDoc);
    self._purchaseService.addPurchase(purchaseDoc).then(function(result){
      console.log("RESULT AFTER ADDING PURCHASE=======", result)
      var productDocs: any = []
      var purchaseDocs: any = []
      var itemDebitCredit: any = []
      for (let product of self.invoiceProducts){
        for (let doc of self.cartProducts){
          if (product._id === doc._id){
            console.log("MATCHED:-");
            var sum = 0
            if (doc.module === "inwardpass"){
              console.log("CONVERTED CART PRODUCT")
              doc.stores = product.stores
              doc._rev = product._rev
            }
            var object = {
              _id:(Math.floor(Math.random()*1000) + 100000).toString(),
              name:doc.name,
              productid:doc._id,
              productrev:doc._rev,
              debit:null,
              credit:null,
              createdat:result.id,
              dcref: purchaseDoc.purchasenumber,
              dctype: "purchase",
              customer:self.selectedInward.inwardpasscustomer,
              stockvalue:null
            }
            for (let obj of doc.stores){
              if (obj._id === doc.cartstores._id){
                console.log("IN STORE MATCHED IF:-", obj);
                if (doc.passquantity){
                  if (doc.passquantity === doc.cartquantity){
                    console.log("PASS QUANTITY MATCHED CART QUANTITY")
                  }else if (doc.passquantity > doc.cartquantity){
                    console.log("PASS QUANTITY GREATER THAN CART QUANTITY")
                    var diff = doc.cartquantity - doc.passquantity
                    object.debit = -diff
                    obj.stock = obj.stock + diff
                  }else if (doc.passquantity < doc.cartquantity){
                    console.log("PASS QUANTITY LESS THAN CART QUANTITY")
                    var diff = doc.cartquantity - doc.passquantity
                    object.credit = diff
                    obj.stock = obj.stock + diff
                  }
                }else{
                  doc.passquantity = null
                  doc.module = "inwardpass"
                  obj.stock = obj.stock + doc.cartquantity
                  object.credit = doc.cartquantity
                }
              }  
              sum = sum + obj.stock
            }
            doc.totatcartweight = doc.cartquantity * doc.weight
            doc.stockvalue = sum
            object.stockvalue = doc.stockvalue
            if (product.type === "Weighted"){
              doc.netweight = doc.stockvalue * doc.weight
              doc.networth = doc.stockvalue * doc.weight * doc.price
            }
            if (product.type === "Single Unit"){
              doc.networth = doc.stockvalue * product.price
            }
            if (doc.passquantity){
              if (doc.passquantity !== doc.cartquantity){
                itemDebitCredit.push(object)
              }
            }else{
              itemDebitCredit.push(object)
            }
            purchaseDocs.push(doc)
            productDocs.push(product)
          }
        }
      }
      console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
      console.log("PRODUCT DOCS IN PURCHASE:=====", purchaseDocs);
      console.log("ITEM DEBIT CREDIT ARRAY OBJECT:=====", itemDebitCredit);
      // let productsToUpdate: any = []
      // for (let doc of purchaseDocs){
      //   var product = {
      //     "_id":doc._id,
      //     "_rev":doc._rev,
      //     "name":doc.name,
      //     "price":doc.price,
      //     "weight":doc.weight,
      //     "category":doc.category,
      //     "stores":doc.stores,
      //     "stockvalue":doc.stockvalue,
      //     "netweight":doc.netweight,
      //     "networth":doc.networth,
      //     "batchnumber":doc.batchnumber,
      //     "subcategory":doc.subcategory,
      //     "type":doc.type
      //   }
      //   productsToUpdate.push(product)
      // }
      // console.log("PRODUCTS TO FINNALY UPDATE:=====", productsToUpdate);
      self.customerToUpdate = _.filter(self.allCustomers, {"_id":self.selectedInward.inwardpasscustomer._id})
      // self.customerToUpdate[0].balance = self.customerToUpdate[0].balance + self.cartTotal
      self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - self.cartTotal
      self.customerDebitCredit = {
        "_id": new Date().toISOString(),
        "name": self.customerToUpdate[0].name,
        "address": self.customerToUpdate[0].address,
        "phone": self.customerToUpdate[0].phone,
        "fax": self.customerToUpdate[0].fax,
        "email": self.customerToUpdate[0].email,
        "customerid": self.customerToUpdate[0]._id,
        "customerrev": self.customerToUpdate[0]._rev,
        "openingbalance": self.customerToUpdate[0].openingbalance,
        "closingbalance": self.customerToUpdate[0].closingbalance,
        "dcref": purchaseDoc.purchasenumber,
        "dctype": "purchase",
        "status": null,
        "debit": null,
        "credit": null
      }
      if (self.customerToUpdate[0].closingbalance > 0){
        self.customerToUpdate[0].status = 'debit'
        self.customerDebitCredit.status = 'debit'
        self.customerDebitCredit.credit = -self.cartTotal
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }else if (self.customerToUpdate[0].closingbalance < 0){
        self.customerToUpdate[0].status = 'credit'
        self.customerDebitCredit.status = 'credit'
        self.customerDebitCredit.credit = -self.cartTotal
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }       
      console.log("CUATOMER TO UPDATE FOR PURCHSE:=====", self.customerToUpdate);
      self._productService.updateConvertedInvoiceProducts(purchaseDocs).then(function(result){
      console.log("RESULT AFTER UPDATEING PURCHASE PRODUCTS:--------", result)
      // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
        self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
          console.log("CUATOMER BALANCES AFTER UPDATE:-----", result)
          self.selectedInward.converted = true
          self._inwardPassService.updateInwardpass(self.selectedInward).then(function(result){
            console.log("RESULT AFTER UPDATEING OUTWARD DOCUMENT:--------", result)
            if (itemDebitCredit.length != 0){
              self._itemDebitCreditService.addDebitCredit(itemDebitCredit).then(function(result){
                console.log("RESULT AFTER ITEM DEBIT CREDIT UPDATE:--------", result)
                self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
                  console.log("RESULT AFTER UPDATING THE CUSTOMER DEBIT CREDIT:=====", result)
                  self.cartProducts = []
                  self.selectedCustomer = null
                  self.cartTotal = 0
                  self.purchaseNotes = null
                  self.customerToUpdate = null
                  self.customerDebitCredit = null
                  self._router.navigate(['/inwardpass'])
                }).catch(function(err){
                  console.log(err)
                })
              }).catch(function(err){
                console.log(err)
              })
            }else{
              self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
                console.log("RESULT AFTER UPDATING THE CUSTOMER DEBIT CREDIT:=====", result)
                self.cartProducts = []
                self.selectedCustomer = null
                self.cartTotal = 0
                self.purchaseNotes = null
                self.customerToUpdate = null
                self.customerDebitCredit = null
                self._router.navigate(['/inwardpass'])
              }).catch(function(err){
                console.log(err)
              })
            }
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })

  }




  calculateTotal(carItems){
    var total = 0  
    for (let item of carItems){
      total = total + item.cartworth
    }
    this.cartTotal = total
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  changeCalcWeight(calcweight, product){
    console.log("CALC WEIGHT CAHFGES:_+_", calcweight, product)
    product.calcweight = calcweight
    product.cartweight = product.calcweight/product.cartquantity
  }

  changePrice(price, product){
    product.cartprice = price
    if (product.type == 'Weighted'){
      // product.calcweight = product.cartquantity * product.cartweight
      product.cartweight = product.calcweight/product.cartquantity
      product.cartworth = product.cartquantity * product.cartweight * product.cartprice
    }else{
      product.calcweight = null
      product.cartworth = product.cartquantity * product.cartprice
    }

    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    this.calculateTotal(this.cartProducts)
  }

  changeWeight(weight, product){
    product.cartweight = weight
    product.calcweight = product.cartquantity * product.cartweight
    product.cartworth = product.calcweight * product.cartprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    if (weight <= 0){
      product.cartweight = product.weight
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.calcweight * product.cartprice
    }
    console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.cartquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight
        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      this.calculateTotal(this.cartProducts)
    }
    if (quantity <= 0){
      product.cartquantity = 1
    }
  }


  setProductStore($event, product){
    // this.selectedStore = $event.value
    // console.log("Selected Store:----", $event.value)
    for (let store of product.stores){
      if (store._id === $event.value){
        product.cartstores = store
        // this.selectedStore = store
      }
    }
    // product.cartstores = this.selectedStore
  }


  filterStates(name: string) {
    console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers(name: string) {
    console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.customers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    console.log("ALL Stores:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  goBack(){
    this._router.navigate(['productcategories'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
