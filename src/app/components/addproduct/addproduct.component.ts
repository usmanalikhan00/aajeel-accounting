import { Component, ElementRef, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import * as _ from 'lodash'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatSnackBar  } from "@angular/material";

@Component({
    selector: 'add-product',
    providers: [LoginService, 
                ProductService,
                StoreSettingsService],
    templateUrl: 'addproduct.html',
    styleUrls: ['addproduct.css']
})

export class addproduct {
  
  public productsDB = new PouchDB('products');
  @Output() addproductevent = new EventEmitter;

  addProductForm: FormGroup;
  users: any = [];


  allCategories: any = [];
  allStores: any = [];
  allSubCategories: any = [];
  selectedStores: any = [];
  storeCount: any = 0;
  selectedCategory: any;
  selectedSubCategory: any;
  enableProductForm: boolean = false
  stockFlag: boolean = false
  productTypes = [
    'Weighted',
    'Single Unit'
  ];
  toppings = new FormControl();
  toppingList = [];

  categoryToAdd: any = null
  subCategoryToAdd: any = null
  authUser: any = null


  constructor(private _loginService: LoginService,
              private _productService: ProductService,
              private _storeSettingsService: StoreSettingsService,
              private _matSnackBar: MatSnackBar,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();
  }

  private _buildAddProductForm(){
    
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      stores: [''],
      // storeStocks: [''],
      type: ['', Validators.required]
    })

  }

  ngOnInit(){
    this.getAllCategories()
    this.getAllStores()
  }

  // getAllCategories(){
  //   var self = this;
  //   self._productService.allProductCategories().then(function(result){
  //     self.allCategories = []
  //     result.rows.map(function(row){
  //       self.allCategories.push(row.doc)
  //     })
  //     console.log("Result from all product Categories:===", self.allCategories)
  //   }).catch(function(err){
  //     console.log(err)
  //   })
  // }

  selectCategory($event){
    var self = this;
    // console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self.categoryToAdd = _.find(self.allCategories, {'_id': self.selectedCategory})
    // console.log("CATEGORY TO ADD", self.categoryToAdd)
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }  

  selectSubCategory($event){
    var self = this;
    // console.log("Sub Category selected", $event);
    self.selectedSubCategory = $event.value
    self.subCategoryToAdd = _.find(self.allSubCategories, {'_id': self.selectedCategory})
    // console.log("SUB CATEGORY TO ADD", self.subCategoryToAdd[0])
    // self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
    //   self.allSubCategories = [];
    //   for (let row of result.docs){
    //     self.allSubCategories.push(row)
    //   }
    // }).catch(function(err){
    //   console.log(err)
    // })
  }  


  deleteStores(i, selectedStores){
    // console.log("DELETE CART ITEM CALLED:-", i, selectedStores)
    selectedStores.splice(i, 1)
    this.selectedStores = selectedStores
  }


  changeStoreStocks($event, store){
    // store.stock = stock.toString()
    // console.log("ALL STORES AFETR STOCK CHANGE:---", store, $event)
    // console.log("ALL STORES AFETR CHANGING:---", this.selectedStores)

  }

  getProductType($event){
    // console.log("PRODUCT TYPE CHANGE CALLED:-------", $event.value)
    if ($event.value === 'Weighted'){
      (<FormGroup>this.addProductForm).patchValue({'weight':''}, { onlySelf: true });
    }else if ($event.value === 'Single Unit'){
      (<FormGroup>this.addProductForm).patchValue({'weight':'none'}, { onlySelf: true });
    }
  }  

  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      // console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      self.toppingList = []
      for (let store of self.allStores){
        var doc = {
          name:store.name,
          stock:null,
          _id:store._id,
          _rev:store._rev
        }
        self.toppingList.push(doc)        
      }
      // console.log("ALL Stores:=====", self.allStores);
    }).catch(function(err){

    })
  }

  addProduct(values){

    var self = this;
    
    var ID = Math.floor(Math.random()*100000000) + 800000000
    // console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    // console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    // console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    // console.log("MULTI SELECT STORE VALUES:--", self.toppings.value);

    values.categoryname = self.categoryToAdd.name
    values._id = new Date().toISOString()

    if (!self.stockFlag){
      console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
      // console.log("----:STOCK VALUES ENTERED:---")
      self._productService.addProduct(values).then(function(result){
        // console.log("PRODUCT ADDED:===", result);
          self.addProductForm.reset();
          self.toppings.reset();
          // self.loadSourceData()
          for (let store of self.selectedStores){
            store.stock = ""
          }
          self.selectedStores = []
             self.addproductevent.emit({'done':true})
      }).catch(function(err){
        console.log(err);
      })
    }else{
      // console.log("----:MUST ENETER STOCK VALUES:---")
      this._matSnackBar.open('Must enter stock values', 'Undo', {
        duration: 3500
      });
    }

  }

  multiSelectStore(values){
    // console.log("MULTISELECT CHANGE CALLED", values)
    this.selectedStores = []
    for (let store of values.value){
      this.selectedStores.push(store)
    }
  }

  selectSubCategoty($event){
    var self = this;
    console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }

  goBack(){
    this._router.navigate(['products'])
  }

  logout(){
    this._loginService.logout()
  }  
}