import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router, ActivatedRoute } from  '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as _ from 'lodash'
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import { ElectronService } from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'customer-ledger',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                CustomersService, 
                ElectronService, 
                MediaMatcher, 
                CustomerDebitCreditService],
    templateUrl: 'customerledger.html',
    styleUrls: ['customerledger.css']
})

export class customerledger {
  

  allCustomers: any = []
  customersToUpdate: any = []
  productTotalStock: any = 0
  itemDetailFlag: boolean = false

  balanceTypes = ['Credit', 'Debit']

  allDebitCredits: any = null
  customerId: any = null
  sub: any = null
  selectedCustomer: any = null

  toDate: any = null
  fromDate: any = null


  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _customersService: CustomersService, 
              private _electronService: ElectronService, 
              private _activatedRoute: ActivatedRoute, 
              private _cdr: ChangeDetectorRef, 
              private _media: MediaMatcher, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    // this.mobileQuery = _media.matchMedia('(max-width: 600px)');
    // this._mobileQueryListener = () => _cdr.detectChanges();
    // this.mobileQuery.addListener(this._mobileQueryListener);
  }



  ngOnInit(){
    var self = this;
    self.sub = self._activatedRoute.params.subscribe(params => {
      self.customerId = params['customerId']; // (+) converts string 'id' to a number
      // console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.productId)
      self._customersService.getSingleCustomer(self.customerId).then(function(result){
        console.log("SINGLE CUSTOMER:====", result)
        self.selectedCustomer = result.docs[0]
        self.allDebitCredits =[]
        self._customerDebitCreditService.printDebitCredits(result.docs[0]).then(function(result){
          console.log("RESULT FROM SINGLE CUSTOMER DEBTI CREDIT:====", result)
          result.docs.forEach(function(doc){
            self.allDebitCredits.push(doc)
          })
          // self.selectedProduct = result.docs[0]
          // self.allDebitCredits = _.orderBy(self.allDebitCredits, ['createdat'], ['asc']);
          console.log("ALL DEBIT CREDITS TO PRINT:------", self.allDebitCredits)
          self._electronService.ipcRenderer.send('PrintCustomerLedger')
        }).catch(function(err){
            console.log(err)
        })
      }).catch(function(err){
          console.log(err)

      })
    })
  }


  logout(){
    this._loginService.logout()
  }

  ngOnDestroy(): void {
    // this.mobileQuery.removeListener(this._mobileQueryListener);
  }  

}
