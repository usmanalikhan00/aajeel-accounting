import { Component, ElementRef, ViewChild, HostListener, Renderer2 } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import {ElectronService} from 'ngx-electron'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  invoicenumber ? : string;
  createdby ? : string;
  invoicecustomer ? : string;
  productcount ? : number;
  invoicenotes ? : string;
  invoicedate ? : string;
  invoicetotal ? : number;
  billno ? : number;
  challanno ? : number;
  othercharges ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'invoice',
    providers: [LoginService, ProductService, InvoiceService, ElectronService],
    templateUrl: 'invoice.html',
    styleUrls: ['invoice.css']
})

export class invoice {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  public invoicesDB = new PouchDB('aajeelaccinvoice');
  public localInvoicesDB = new PouchDB('http://localhost:5984/localsteelinvoices');
  public cloudantInvoicesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccinvoice', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  
  allInvoices: any = [];
  invoicesTotal: any = null;
  
  dateFilterForm: FormGroup;

  displayedColumns = [
    "createdat",
    'invoicenumber',
    "challanno", 
    "billno", 
    'createdby', 
    'invoicecustomer', 
    'productcount', 
    "invoicenotes",
    "invoicetotal"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null

  authUser: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _formBuilder: FormBuilder, 
              private _rendrer: Renderer2, 
              private _electronService: ElectronService, 
              private _router: Router) {
    this._buildAddProductForm();
    this.loadDataSource()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddProductForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }

  ngOnInit(){
    var self = this
    // self.syncDb()
    self.checkscreen()
  }

  checkscreen(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
  }



  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.invoicesDB.replicate.from(self.cloudantInvoicesDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY INVOICES REPLICATION:--", info)
      self.invoicesDB.sync(self.cloudantInvoicesDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY INVOICES SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
          self.loadDataSource()
        // }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC INVOICES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY INVOICES SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }


  loadDataSource(){
    var self = this;
    self._invoiceService.allInvocies().then(function(result){
      // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
      // console.log("ALL INVOICES:=====", self.allInvoices);
      const users: UserData[] = [];
      self.allInvoices = [];
      result.rows.map(function(row){
        users.push(createNewUser(row.doc))
        self.allInvoices.push(row.doc)
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      self.getInvoicesTotal(self.allInvoices)
      // console.log("ALL INVOCIES ON NGONINIT:===", self.allInvoices)
    }).catch(function(err){
      console.log(err);
    })

  }

  goToPrint(){
    // this._router.navigate(['printinvoices'])
    this._electronService.ipcRenderer.send('InvoicesPrint')
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
      var self = this
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      self.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      self.toDate = event.value.toISOString()
    }
    // console.log("FROM DATE:----", "\n", self.fromDate, "TO DATE:----", self.toDate)

    if (self.fromDate < self.toDate){
      // console.log("FROM DATE  SMALL")
    }else{
      // console.log("FROM DATE LARGER")
    }
    if (self.toDate < self.fromDate){
      // console.log("TO DATE  SMALL")
    }else{
      // console.log("TO DATE LARGER")
    }

    if (self.toDate != null && self.fromDate != null){

      self._invoiceService.filterInvoices(self.toDate, self.fromDate).then(function(result){
        // console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        // self.allInvoices = []
        // result.docs.forEach(function(doc){
        //   self.allInvoices.push(doc)
        // })
        const users: UserData[] = [];
        self.allInvoices = [];
        result.docs.forEach(function(row){
          users.push(createNewUser(row))
          self.allInvoices.push(row)
        })
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getInvoicesTotal(self.allInvoices)
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.loadDataSource()
    this.toDate = null
    this.fromDate = null
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getInvoicesTotal(allInvoices){
    var sum = 0
    for (let doc of allInvoices){
      sum = sum + doc.invoicetotal
    }
    this.invoicesTotal = sum
    // this.invoicesTotal = Number(this.invoicesTotal)
    // console.log("SUM FROM INVOICES TOTAL:===", sum, this.invoicesTotal)
  }

  getSingleInvoice(invoice){
    // console.log("Selected Invoice:===", invoice)
    this._router.navigate(['/invoice', invoice._id])
  }


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {

  // console.log("ROW TO SHOW FOR PAYMENTS", row)

  return {
    "_id": row._id,
    "_rev": row._rev,
    "invoicecustomer" : row.invoicecustomer.name.toString(),
    "createdat" : row.createdat.toString(),
    "createdby" : row.createdby._id.toString(),
    "invoicenotes" : row.invoicenotes,
    "invoicenumber" : row.invoicenumber.toString(),
    "invoicetotal" : row.invoicetotal,
    "billno" : row.billno,
    "challanno" : row.challanno,
    "invoicedate" : row.invoicedate,
    "productcount" : row.invoiceproducts.length
  };
}