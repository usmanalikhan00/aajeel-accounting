import { Component, ElementRef, ViewChild, HostListener, Renderer2, Input, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { StoreSettingsService } from '../../../services/storesettings.service'
import { CustomersService } from '../../../services/customers.service'
import { InvoiceService } from '../../../services/invoice.service'
import { ItemDebitCreditService } from '../../../services/itemdebitcredit.service'
import { CustomerDebitCreditService } from '../../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { MatSidenav, MatSnackBar } from "@angular/material";
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { UUID } from 'angular2-uuid';

@Component({
    selector: 'add-invoice',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                ItemDebitCreditService, 
                CustomerDebitCreditService, 
                CustomersService],
    templateUrl: 'addInvoice.html',
    styleUrls: ['addInvoice.css']
    // ,
    // host: {
    //   '(document:keypress)': 'handleKeyboardEvents($event)'
    // }
})

export class addInvoice {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  
  navMode = 'side';
  // public productsDB = new PouchDB('steelinvoice');
  public invoicesDB = new PouchDB('steelcustomerinvoice');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  invoiceProducts: any = [];
  allInvoices: any= [];
  
  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;
  
  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  cartProducts: any[] = []
  cartTotal: any = 0
  cartWeight: any = 0
  
  cartStates: Observable<any[]>;
  invoiceNotes: any = null;

  customerToUpdate: any = null
  customerDebitCredit: any = null

  autofiller: any = 1
  challanno: any = null
  billno: any = null
  othercharges: any = null
  invoicedate: any = null
  authUser: any = null

  shortKey: any = null
  cartIndex: any = null
  invoiceId: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService,
              private _rendrer: Renderer2,
              public _matSnackBar: MatSnackBar,  
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }



  ngOnInit(){
    // this.invoiceId = Math.floor(Math.random()*1000) + 100000
    this.invoiceId = UUID.UUID().substr(30, 35)
    this.getCartProducts()
    this.getAllStores()
    this.getAllCustomers()
  }
  

  setFocus(){
    const element = this._rendrer.selectRootElement('#customername');

    setTimeout(() => element.focus(), 0);
  }

  handleKeyboardEvents(event: KeyboardEvent) {
    var self = this;
    if (self.shortKey == null){
      self.shortKey = event.key;
    }else{
      self.shortKey = self.shortKey+event.key
    }

    // if (event.charCode != 13)
    //   this.barCode = this.barCode+this.key;
    // else{
    //   console.log(this.barCode);
    //   self._service.searchProduct(this.barCode).then(function(result){
    //     console.log(result);
    //     result.docs.map(function (row) {
    //       self.barCodeProduct = row;
    //     });
    //     console.log(self.barCodeProduct);
    //     self.getProduct(self.barCodeProduct);
    //     self.barCodeProduct = '';
    //     // console.log('self.allOrders from orders service', self.allOrders);
    //   }).catch(function(err){
    //       console.log(err);
    //   });
    //   this.barCode = '';
    // }
      console.log("KEY PRESSED:=====", self.shortKey, event);
      if (self.shortKey === '11'){
        self.setFocus()
        self.shortKey = null
      }
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self.customers = []
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        var object = {
          "name":row.doc.name,
          "_id":row.doc._id,
          "_rev":row.doc._rev,
          "address":"",
          "phone":"",
          "email":"",
          "balance":row.doc.balance
        }          
        self.allCustomers.push(row.doc); 
        self.customers.push(object)
      });

      // for (let item of self.allCustomers){
        
      //     var object = {
      //       "name":item.name,
      //       "_id":item._id,
      //       "_rev":item._rev,
      //       "address":"",
      //       "phone":"",
      //       "email":"",
      //       "balance":item.balance
      //     }          

      //   self.customers.push(object)
      // }
      self.customerStates = self.addCustomerForm.controls['customerCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.customers.slice());
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getInvoiceNotes($event){
    this.invoiceNotes = $event
    // console.log("INVOICE NOTES ARE:-----", $event ,this.invoiceNotes)
  }

  sortCart(cartProducts){

    var sortByProperty = function (property) {

        return function (x, y) {

            return ((parseInt(x[property]) === parseInt(y[property])) ? 0 : ((parseInt(x[property]) < parseInt(y[property])) ? 1 : -1));

        };

    };
    cartProducts.sort(sortByProperty('sortorder'))


  }
  addInvoice(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    self.invoiceId = Math.floor(Math.random()*1000) + 100000
    self.customerDebitCredit = null
    var invoiceDoc: any = null
    if (self.invoicedate){
      invoiceDoc = {
        "_id": new Date().toISOString(),
        "invoicenumber": Math.floor(Math.random()*1000) + 100000, 
        "invoicetotal": self.cartTotal + self.othercharges, 
        "invoiceweight": self.cartWeight, 
        "invoiceproducts": self.cartProducts, 
        "invoicecustomer": self.selectedCustomer, 
        "invoicecustomerid": self.selectedCustomer._id, 
        "status": null, 
        "createdby": user, 
        "createdat": moment().format(),
        "othercharges":self.othercharges,
        "challanno":self.challanno,
        "billno":self.billno,
        "invoicedate":self.invoicedate.toISOString(),
        "invoicenotes":self.invoiceNotes 
      }
    }else{
      invoiceDoc = {
        "_id": new Date().toISOString(),
        "invoicenumber": self.invoiceId, 
        "invoicetotal": self.cartTotal + self.othercharges, 
        "invoiceweight": self.cartWeight, 
        "invoiceproducts": self.cartProducts, 
        "invoicecustomer": self.selectedCustomer, 
        "invoicecustomerid": self.selectedCustomer._id, 
        "status": null, 
        "createdby": user, 
        "createdat": moment().format(),
        "othercharges":self.othercharges,
        "challanno":self.challanno,
        "billno":self.billno,
        "invoicedate":self.invoicedate,
        "invoicenotes":self.invoiceNotes 
      }
    }
    var storeFlag = false
    for (let doc of invoiceDoc.invoiceproducts){
      // console.log("DOC TO ADD:-----", doc)
      if (doc.cartstores === ""){
        storeFlag = true
        break
      }else{
        storeFlag = false
      }
    }
    // console.log("STORE FLAG VALUE:-----", storeFlag)

    var priceFlag = false
    for (let doc of invoiceDoc.invoiceproducts){
      // console.log("DOC TO ADD:-----", doc)
      if (doc.cartprice === 0){
        priceFlag = true
        break
      }else{
        priceFlag = false
      }
    }
    // console.log("PRICE FLAG VALUE:-----", priceFlag)
    // console.log("INVOICE DOC TO PUSH:-----", invoiceDoc)

    if (!storeFlag && !priceFlag){
      // self.selectedCustomer.balance = self.cartTotal
      self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + self.cartTotal + self.othercharges
      self.customerDebitCredit = {
        "_id": new Date().toISOString(),
        "name": self.customerToUpdate[0].name,
        "address": self.customerToUpdate[0].address,
        "phone": self.customerToUpdate[0].phone,
        "fax": self.customerToUpdate[0].fax,
        "email": self.customerToUpdate[0].email,
        "customerid": self.customerToUpdate[0]._id,
        "customerrev": self.customerToUpdate[0]._rev,
        "openingbalance": self.customerToUpdate[0].openingbalance,
        "closingbalance": self.customerToUpdate[0].closingbalance,
        "dcref": invoiceDoc.invoicenumber,
        "dcrefid": invoiceDoc._id,
        "dcrefdate": invoiceDoc.invoicedate,
        "billno": invoiceDoc.billno,
        "challanno": invoiceDoc.challanno,
        "dctype": "invoice",
        "status": null,
        "debit": null,
        "credit": null
      }
      if (self.customerToUpdate[0].closingbalance > 0){
        self.customerToUpdate[0].status = 'debit'
        self.customerDebitCredit.status = 'debit'
        self.customerDebitCredit.debit = self.cartTotal + self.othercharges
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }else if (self.customerToUpdate[0].closingbalance < 0){
        self.customerToUpdate[0].status = 'credit'
        self.customerDebitCredit.status = 'credit'
        self.customerDebitCredit.debit = self.cartTotal + self.othercharges
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }
      // console.log("INVOICE DOC TO PUSH:=====", invoiceDoc);
      // console.log("CUSTOMER BALANCE TO UPDATE::=====", self.customerToUpdate[0]);
      // console.log("CUSTOMER CREDIT DEBIT DOCUMENT::=====", self.customerDebitCredit);

      self._invoiceService.addInvoice(invoiceDoc).then(function(result){
        // console.log("RESULT AFTER ADDING INVOICE=======", result)
        // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
        var productDocs: any = []
        var invoiceDocs: any = []
        var itemDebitCredit: any = []
        for (let product of self.invoiceProducts){
          for (let doc of self.cartProducts){
            if (product._id === doc._id){
              var sum = 0
              for (let obj of doc.stores){
                if (obj._id === doc.cartstores._id){
                  obj.stock = obj.stock - doc.cartquantity
                }  
                sum = sum + obj.stock
              }
              doc.totatcartweight = doc.cartquantity * doc.weight
              doc.stockvalue = sum
              if (product.type === "Weighted"){
                doc.netweight = doc.stockvalue * doc.weight
                doc.networth = doc.stockvalue * doc.weight * doc.price
              }
              if (product.type === "Single Unit"){
                doc.networth = doc.stockvalue * product.price
              }
              var things = ['a', 'b', 'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r'];
              var thing = things[Math.floor(Math.random()*things.length)];
              var object = {
                "_id":UUID.UUID().substr(28, 35),
                "name":doc.name,
                "productid":doc._id,
                "productrev":doc._rev,
                "debit":doc.cartquantity,
                "credit":null,
                "createdat":new Date().toISOString(),
                "customer":self.selectedCustomer,
                "dcref": invoiceDoc.invoicenumber,
                "dcrefid": invoiceDoc._id,
                "dcrefdate": invoiceDoc.invoicedate,
                "dctype": "invoice",
                "billno": invoiceDoc.billno,
                "challanno": invoiceDoc.challanno,
                "stockvalue":doc.stockvalue
              }
              itemDebitCredit.push(object)
              var productDoc = {
                '_id': doc._id,
                '_rev': doc._rev,
                'name': doc.name,
                'batchnumber': doc.batchnumber,
                'weight': doc.weight,
                'price': doc.price,
                'category': doc.category,
                'categoryname': doc.categoryname,
                'stockvalue': doc.stockvalue,
                'type': doc.type,
                'subcategory': doc.subcategory,
                'stores': doc.stores,
                'storeStocks': doc.storeStocks,
                'netweight': doc.netweight,
                'networth': doc.networth
              }             
              invoiceDocs.push(productDoc)
              productDocs.push(product)
            }
          }
        }
        // console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
        // console.log("PRODUCT DOCS IN INVOICE:=====", invoiceDocs);
        // console.log("ITEM DEBIT CREDIT ARRAY:=====", itemDebitCredit);
        self._productService.updateInvoiceProducts(invoiceDocs).then(function(result){
          // console.log("RESULT AFTER UPDATEING PRODUCTS:--------", result)
          var routerId = result.id
          self.cartProducts = []
          // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
          self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
            // console.log("CUATOMER BALANCES AFTER UPDATE:-----", result)
            self._itemDebitCreditService.addDebitCredit(itemDebitCredit).then(function(result){
              // console.log("RESULT AFTER ADDING DEIT CREDIT FOR ITEM:-----", result)
              self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
                // console.log("RESULT AFTER UPDATING THE CUSTOMER DEBIT CREDIT:=====", result)
                  self.selectedCustomer = null
                  itemDebitCredit = []
                  self.cartTotal = 0
                  self.cartWeight = 0
                  self.invoiceNotes = null
                  self.customerDebitCredit = null
                  self.customerToUpdate = null
                  self.autofiller = 1
                  self.challanno = null
                  self.billno = null
                  self.othercharges = null
                  self.invoicedate = null
                  self.invoiceId = UUID.UUID().substr(30, 35)
                  // self.invoiceId = Math.floor(Math.random()*1000) + 100000
                  self.getCartProducts()
                  self.getAllCustomers()
              }).catch(function(err){
                console.log(err)
              })
            }).catch(function(err){
              console.log(err)
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }
    if (storeFlag){
      this._matSnackBar.open('Must select a store', 'Undo', {
        duration: 3500
      });
    }
    if (priceFlag){
      this._matSnackBar.open('Price cannot be 0', 'Undo', {
        duration: 3500
      });
    }

    // self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
    //   console.log("RESULT AFTER UPDATING THE SALE CUSTOMER:=====", result)
    // }).catch(function(err){
    //   console.log(err)
    // })
  }


  newProductAdded($event){
    // console.log("EVENT FROM ADD PRODUCT FORM:-----", $event)
    this.getCartProducts()
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStores(state) : self.allStores.slice());
      // console.log("ALL Stores:=====", self.allStores);
    }).catch(function(err){

    })
  }


  selectProductStore(product, state){
    for (let store of product.stores){
      if (store._id === state._id){
        product.cartstores = state
      }
    }
    // console.log("SELCTED STORE ON SELCTED PRODUCT", product)
    var sum = 0
    for (let store of product.stores){
      if (store._id === state._id){
        // console.log("STORE MATCHED:---", store)
        store.stock = store.stock - product.cartquantity
        product.stockvalue = product.stockvalue - product.cartquantity
      }
    }
  }

  setProductStore($event, product){
    // this.selectedStore = $event.value
    // console.log("Selected Store:----", $event.value)
    for (let store of product.stores){
      if (store._id === $event.value){
        product.cartstores = store
        // this.selectedStore = store
      }
    }
    // product.cartstores = this.selectedStore
  }

  selectCustomer(state){

    this.selectedCustomer = state
    // console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate)
    this._buildAddCustomerForm()
    this.getAllCustomers()
  }

  getCartProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self.states = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
        var object = {
          "name":row.doc.name,
          "weight":row.doc.weight,
          "batchnumber":row.doc.batchnumber,
          "stockvalue":row.doc.stockvalue,
          "storeStocks":row.doc.storeStocks,
          "_id":row.doc._id,
          "_rev":row.doc._rev,
          "category":row.doc.category,
          "categoryname":row.doc.categoryname,
          "subcategory":row.doc.subcategory,
          "price":row.doc.price,
          "cartweight":0,
          "calcweight":0,
          "type":row.doc.type,
          "stores":row.doc.stores,
          "cartstores":"",
          "cartquantity":0,
          "cartprice":0
        }          
        self.states.push(object)
      });
      // console.log("ALL PRODUCTS:=====", self.invoiceProducts);
      // for (let item of self.invoiceProducts){

      // }
      // console.log("ALL PRODUCTS:=====", self.states);
      self.filteredStates = self.addInvoiceForm.controls['stateCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStates(state) : self.states.slice());
    }).catch(function(err){
      console.log(err);
    })
  }

  selectCartProduct(item){
    // console.log("AUTO SELCT CHANGE FUNCTION", item)
    this.cartIndex = null
    this.selecteProduct(item)
    this._buildAddInvoiceForm()

  }

  changeCalcWeight(calcweight, product){
    product.calcweight = calcweight
    // console.log("CALC WEIGHT CAHFGES:_+_", calcweight, product)
    product.cartweight = calcweight/product.cartquantity
    this.calculateTotal(this.cartProducts)
  }

  selecteProduct(product){
    // console.log("CHANGE INPUT VALUE SELECED:_+_", product)
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.cartquantity = product.cartquantity + 1
        if (product.type == 'Weighted'){
          // product.calcweight = product.cartquantity * product.cartweight
          product.cartweight = product.calcweight/product.cartquantity
          product.cartworth = product.cartquantity * product.cartweight * product.cartprice
        }else{
          product.calcweight = null
          product.cartworth = product.cartquantity * product.cartprice
        }
        product.sortorder = this.autofiller.toString()
        this.autofiller++
        this.cartProducts.push(product)
        // this.cartProducts = _.reverse(this.cartProducts)
        this.sortCart(this.cartProducts)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].cartquantity = this.cartProducts[i].cartquantity + 1
            if (this.cartProducts[i].type == 'Weighted'){
              // this.cartProducts[i].calcweight = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight
              this.cartProducts[i].cartweight = this.cartProducts[i].calcweight/this.cartProducts[i].cartquantity
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight * this.cartProducts[i].cartprice
            }else{
              this.cartProducts[i].calcweight = null
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            // console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.cartquantity = product.cartquantity + 1
          if (product.type == 'Weighted'){
            // product.calcweight = product.cartquantity * product.cartweight
            product.cartweight = product.calcweight/product.cartquantity
            product.cartworth = product.cartquantity * product.cartweight * product.cartprice
          }else{
            product.calcweight = null
            product.cartworth = product.cartquantity * product.cartprice
          }
          product.sortorder = this.autofiller.toString()
          this.autofiller++
          this.cartProducts.push(product)
          // this.cartProducts = _.reverse(this.cartProducts)
          this.sortCart(this.cartProducts)
          this.calculateTotal(this.cartProducts)
        }
      }

      this._buildAddInvoiceForm()
      this.getCartProducts()
      // this.filteredStates = this.addInvoiceForm.controls['stateCtrl'].valueChanges
      //   .startWith(null)
      //   .map(state => state ? this.filterStates(state) : this.states.slice());
    }
    
  }

  changeWeight(weight, product){
    product.cartweight = weight
    product.calcweight = product.cartquantity * product.cartweight
    product.cartworth = product.calcweight * product.cartprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    // console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    if (weight <= 0){
      product.cartweight = product.weight
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.calcweight * product.cartprice
      this._matSnackBar.open('Weight < 0', 'Undo', {
        duration: 3500
      });
    }
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    // console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.cartquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight
        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      this.calculateTotal(this.cartProducts)
    }
    if (quantity <= 0){
      product.cartquantity = 1
      this._matSnackBar.open('Quantity < 0', 'Undo', {
        duration: 3500
      });
    }
  }

  changePrice(price, product){
    product.cartprice = price
    if (product.type == 'Weighted'){
      // product.calcweight = product.cartquantity * product.cartweight
      product.cartweight = product.calcweight/product.cartquantity
      product.cartworth = product.cartquantity * product.cartweight * product.cartprice
    }else{
      product.calcweight = null
      product.cartworth = product.cartquantity * product.cartprice
    }

    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    if (price < 0){
      product.cartprice = 0
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight

        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      this._matSnackBar.open('Price < 0', 'Undo', {
        duration: 3500
      });
    }
    this.calculateTotal(this.cartProducts)
  }



  calculateTotal(carItems){
    var total = 0
    var weight = 0  
    for (let item of carItems){
      total = total + item.cartworth
      if (item.type == 'Weighted')
        weight = weight + item.calcweight
    }
    this.cartTotal = total
    this.cartWeight = weight
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  deleteCartItem(i, cartProducts){
    // console.log("DELETE CART ITEM CALLED:-", i, cartProducts)
    cartProducts.splice(i, 1)
    this.cartProducts = cartProducts
    this.calculateTotal(this.cartProducts)

  }

  getProduct(name){
    let arr: any = []
    if (name){
      arr = _.filter(this.states, function(row, index) {
        return row.name.indexOf(name) > -1
      }) 
      // console.log("FILTERED ARRAY:__", arr);
      return arr[0]
    }
    return false
  }

  filterStates(name: string) {
    // console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCart(name: string) {
    // console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.cartProducts.filter(state =>
      state.cartindex.indexOf(name) > -1);
  }



  filterCustomers(name: string) {
    // console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.customers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    // console.log("ALL Stores:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }

}
