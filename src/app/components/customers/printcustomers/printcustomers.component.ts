import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { CustomersService } from '../../../services/customers.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  purchasenumber ? : string;
  createdby ? : string;
  purchasecustomer ? : string;
  productcount ? : number;
  purchasenotes ? : string;
  purchasetotal ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'print-customers',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                ElectronService, 
                CustomersService],
    templateUrl: 'printcustomers.html',
    styleUrls: ['printcustomers.css']
})

export class printcustomers {
  
  allCustomers: any = [];
  totalDebits: any = null;
  totalCredits: any = null;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _customersService: CustomersService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loadDataSource()
  }

  loadDataSource(){
    var self = this;
    // self.allInvoices = [];
    self._customersService.allCustomers().then(function(result){
      self.allCustomers = [];
      result.rows.map(function(row){
        self.allCustomers.push(row.doc); 
      })
      console.log("allCustomers", self.allCustomers)
      self.debitCreditTotal(self.allCustomers)
      // self.calculateStockWeight(self.allProducts)
      self._electronService.ipcRenderer.send('PrintCustomers')
    }).catch(function(err){
      console.log(err);
    })

  }

  debitCreditTotal(customers){
    var debit = 0
    var credit = 0
    for (let item of customers){
      if (item.status === "debit"){
        debit = debit + item.closingbalance
      }
      if (item.status === "credit"){
        credit = credit + item.closingbalance
      }
    }
    this.totalDebits = debit
    this.totalCredits = credit
    console.log("TOTAL DEBITS:--", this.totalDebits, "\n", "TOTAL CREDITS:--", this.totalCredits, "\n");
  }

  // calculateStockWeight(products){
  //   var sum = 0
  //   for (let item of products){
  //     sum = sum + item.netweight
  //   }
  // }



  // getSinglePurchase(purchase){
  //   console.log("Selected Purchase:===", purchase)
  //   this._router.navigate(['/purchase', purchase._id])
  // }

  logout(){
    this._loginService.logout()
  }  
}
