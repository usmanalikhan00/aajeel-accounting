import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
import { CustomersService } from '../../services/customers.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
import * as _ from "lodash";
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray, FormGroupDirective } from '@angular/forms';
import { MatSidenav } from "@angular/material";
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import {DataSource} from '@angular/cdk/collections'
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  name ? : string;
  openingbalance ? : string;
  closingbalance ? : string;
  remainbalance ? : string;
  type ? : string;
  accountnumber ? : string;
  customername ? : string;
  voucherno ? : string;
  _id ? : string;
  _rev ? : string;
  expirydate ? : string;
  expirystatus ? : string;
  level ? : string;
  obtype ? : string;
  refnumber ? : string;
  status ? : string;
}


@Component({
    selector: 'cash-settings',
    providers: [LoginService, 
                AccountTypesService, 
                AccountDebitCreditService, 
                CustomersService,
                CustomerDebitCreditService],
    templateUrl: 'cashsettings.html',
    styleUrls: ['cashsettings.css']
})

export class cashsettings {
  
  // public outwardPassDB = new PouchDB('steelcustomeroutwardpass');
  public subAccountsDB = new PouchDB('aajeelaccsubaccounts');
  public localSubAccountTypesDB = new PouchDB('http://localhost:5984/aajeelaccsubaccounts');

  
  public accountTypesDB = new PouchDB('aajeelaccaccounttypes');
  public localAccountTypesDB = new PouchDB('http://localhost:5984/aajeelaccaccounttypes');



  public accountDebitCreditDB = new PouchDB('aajeelaccaccountdebitcredit');
  public localAccountDebitCreditDB = new PouchDB('http://localhost:5984/aajeelaccaccountdebitcredit');


  public cloudantSubAccountTypesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccsubaccounts', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });
  public cloudantAccountTypesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccaccounttypes', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });
  public cloudantAccountDebitCreditDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccaccountdebitcredit', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });


  @ViewChild(FormGroupDirective) myForm;
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  
  addAccountTypeForm: FormGroup;
  allAccountTypes: any = []
  accountDetailFlag: boolean = false
  accountDebitCredits: any = []
  accountDetail: any = null
  
  selectedBank: any = null
  bankStates: Observable<any[]>;  

  customerStates: Observable<any[]>;  
  allCustomers: any = []
  selectedCustomer: any = null
  customerToUpdate: any = null
  customerDebitCredit: any = null

  balanceTypes = ['Credit', 'Debit']
  allTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
    {
      'name':'Post Dated Cheque',
      'type':'post_dated_cheque'
    },
    {
      'name':'Parchi',
      'type':'parchi'
    },
    {
      'name':'Card',
      'type':'card'
    },
    {
      'name':'Cheque',
      'type':'cheque'
    }
  ]

  dateMsg: any = null
  // displayedColumns = ['name', 'balance', 'type', 'status'];
  // dataSource = new AccountsDataSource(this._accountTypesService);
  allParentAccount: any = []
  allChildAccount: any = []
  allBankAccounts: any = []


  displayedColumns = [
    'name', 
    'customername',
    'voucherno', 
    'openingbalance', 
    'closingbalance', 
    'type',
    'status'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark', scrollButtons: { enable: true } };

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  storedate: any = null
  authUser: any = null

  constructor(private _loginService: LoginService,
              private _accountTypesService: AccountTypesService, 
              private _accountDebitCreditService: AccountDebitCreditService,
              private _customersService: CustomersService, 
              private _malihuScrollbarService: MalihuScrollbarService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    var self = this;
    self._buildAddAccountTypeForm();
    self.loadSourceData()
    self.authUser = JSON.parse(localStorage.getItem('user'))
  }


  loadSourceData(){
    var self = this;
    const users: UserData[] = [];
    // self._buildAddAccountTypeForm();
    // self.loadSourceData()
    self._accountTypesService.getParentLevelAccounts().then(function(result){
      // console.log("Reuslt after fetching all Parent accounts:---", result)
      self.allParentAccount = []
      result.docs.forEach(function (row) { 
        users.push(createNewUser(row)); 
      });
      self.dataSource = new MatTableDataSource(users);
      // self.dataSource = new MatTableDataSource(this.allAccountTypes);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      // console.log("ALL PARENT ACCONT TYPES:(((((((((((", self.allParentAccount)
    }).catch(function(err){
      console.log(err)
    })
  }

  getAllBankAccounts(){
    var self = this
    self.allBankAccounts = []
    self._accountTypesService.getAllBankAccounts().then(function(result){
      self.allBankAccounts = result.docs
      console.log("ALL BANK ACCOUNTS:---", self.allBankAccounts)
      self.bankStates = self.addAccountTypeForm.controls['banks'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterBanks(state) : self.allBankAccounts.slice());
    }).catch(function(err){
      console.log(err)
    })    
  }

  typingBankAccount($event){
    console.log($event, this.addAccountTypeForm.controls['banks'].value)
  }

  setDates(){
    
    var currentyear = parseInt(moment().format("YYYY"))
    var currentmonth = parseInt(moment().format("MM"))
    var currentday = parseInt(moment().format("DD"))
    // console.log("STATS OF TODAY:--", currentyear, typeof(currentyear), currentmonth, typeof(currentmonth), currentday, typeof(currentday))
    this.minDate = new Date(currentyear, currentmonth-1, currentday);

  }

  private _buildAddAccountTypeForm(){
    this.addAccountTypeForm = this._formBuilder.group({
      name: ['', Validators.required],
      accounttype: ['', Validators.required],
      accountnumber: ['', Validators.required],
      expirydate: ['', Validators.required],
      obtype: ['', Validators.required],
      customer: ['', Validators.required],
      refnumber: ['', Validators.required],
      banks: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }

  ngOnInit(){
    // if (window.innerWidth < 886) {
    //   this.navMode = 'over';
    // }
    this.getAccountTypes()
    this.getAllCustomers()
    this.getAllBankAccounts()
    this.setDates()
    // this.syncDb()
    // this.syncSubAccountDb()
    // this.syncAccountDebitCreditDb()
    // console.log("Reuslt after fetching all the company info:---", this.allTypes)
  }


  syncDb(){
    var self = this
    var sync = PouchDB.sync(self.accountTypesDB, self.cloudantAccountTypesDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadSourceData()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("PAUSE EVENT FROM ACC. TYPES:--", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }


  syncSubAccountDb(){
    var self = this
    var sync = PouchDB.sync(self.subAccountsDB, self.cloudantSubAccountTypesDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadSourceData()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("PAUSE EVENT FROM SUB ACC. TYPES:--", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }

  syncAccountDebitCreditDb(){
    var self = this
    var sync = PouchDB.sync(self.accountDebitCreditDB, self.cloudantAccountDebitCreditDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadSourceData()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("***********************DEBIT CREDIT HOYA HAI:--", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }



  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      self.customerStates = self.addAccountTypeForm.controls['customer'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.allCustomers.slice());
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getAccountTypes(){
    var self = this;
    self._accountTypesService.getAccountTypes().then(function(result){
      // console.log("Reuslt after fetching all the accounts:---", result)
      self.allAccountTypes = []
      result.rows.map(function (row) { 
        self.allAccountTypes.push(row.doc); 
      });
      // self.dataSource = new MatTableDataSource(this.allAccountTypes);
      // console.log("ALL ACCOUNT TYPES:(((((((((((", self.allAccountTypes)
    }).catch(function(err){
      console.log(err)
    })    
  }

  getParentAccounts(){
    var self = this;
    self._accountTypesService.getParentLevelAccounts().then(function(result){
      console.log("Reuslt after fetching all Parent accounts:---", result)
      self.allParentAccount = []
      result.docs.forEach(function (row) { 
        self.allParentAccount.push(row); 
      });
      // self.dataSource = new MatTableDataSource(this.allAccountTypes);
      // console.log("ALL PARENT ACCONT TYPES:(((((((((((", self.allParentAccount)
    }).catch(function(err){
      console.log(err)
    })    
  }



  selectCustomer(state){
    (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedCustomer = state
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate[0])
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this.getAllCustomers()
  }

  selectBank(state){
    (<FormGroup>this.addAccountTypeForm).patchValue({'banks':''}, { onlySelf: true });
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedBank = state
    // this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate[0])
    console.log("SELECTED BANK IS: ------", this.selectedBank)
    this.getAllBankAccounts()
  }




  addAccountType(values, $event){
    var self = this;
    values._id = new Date().toISOString()
    // values.closingbalance = values.openingbalance
    if (values.accounttype === 'bank' || values.accounttype === 'cash'){
      if (values.obtype === 'Debit'){
        if (values.openingbalance != 0){
          values.closingbalance = values.openingbalance
          values.status = 'debit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }else if (values.obtype === 'Credit'){
        if (values.openingbalance != 0){
          values.openingbalance = -values.openingbalance
          values.closingbalance = values.openingbalance
          values.status = 'credit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }
      values.level = 'parent'
      values.parentid = null
      
      // console.log("VALUES FROM ADD ACCOUNT TYPE FORM:--", values)
      self._accountTypesService.addAccountType(values).then(function(result){
        // console.log("result after adding account:---", result)
        // self.getAccountTypes()
        self.loadSourceData()
        self._buildAddAccountTypeForm()
      }).catch(function(err){
        console.log(err)
      })
    }else{
      values.expirystatus = 'pending'
      values.customer = this.selectedCustomer
      values.customerid = values.customer._id
      values.remainbalance = values.openingbalance
      values.level = 'parent'
      values.parentid = null

      if (values.obtype === 'Debit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - values.openingbalance
          
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "dctype": values.accounttype,
          "status": null,
          "debit": null,
          "credit": null
        }
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }       
      }else if (values.obtype === 'Credit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + values.openingbalance
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "dctype": values.accounttype,
          "status": null,
          "debit": null,
          "credit": null
        }      
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        } 
      }
      // values.expirydate = values.expirydate
      // (values.expirydate)
      values.expirydate = values.expirydate.toISOString()

      // console.log("VALUES FROM ADD ACCOUNT TYPE FORM:--", values)
      // console.log("CUSTOMER DEBIT CREDIT OBJECT:========", self.customerDebitCredit)
      // console.log("ACTUAL CUSTOMER TO UPDATE:========", self.customerToUpdate[0])
      self._accountTypesService.addAccountType(values).then(function(result){
        // console.log("result after adding account:---", result)
        self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
          // console.log("result after adding customer debit credit object:---", result)
          self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
            // console.log("result after updating customer balance:---", result)
            // self.getAccountTypes()
            // self._buildAddAccountTypeForm()
            self.myForm.resetForm()
            self.loadSourceData()
            self.getAllCustomers()
            self.selectedCustomer = null
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }
  }

  dateChange($event){
    this.dateMsg = null
    // console.log("DATE CHANGES:----", $event.target.value)
    var inputyear = parseInt(moment($event.target.value,"YYYY/MM/DD").format("YYYY"))
    var inputmonth = parseInt(moment($event.target.value,"YYYY/MM/DD").format("MM"))
    var inputday = parseInt(moment($event.target.value,"YYYY/MM/DD").format("DD"))
    // console.log("YEAR AFTER FORMATING INPUT DATE:--\n", "INPUT YEAR\n", inputyear, typeof(inputyear), "INPUT MONTH\n", inputmonth, typeof(inputmonth), "INPUT DAYY\n", inputday, typeof(inputday))
    var currentyear = parseInt(moment().format("YYYY"))
    var currentmonth = parseInt(moment().format("MM"))
    var currentday = parseInt(moment().format("DD"))
    // console.log("STATS OF TODAY:--", currentyear, typeof(currentyear), currentmonth, typeof(currentmonth), currentday, typeof(currentday))

    // var dateMsg = null
    if (inputyear < currentyear){
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
      this.dateMsg = "YEAR IS NOT CORRECT"
    }
    if (inputmonth != 1){
      if (currentmonth > inputmonth){
        (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
        this.dateMsg = "MONTH NOT CORRECT"
      }
    }
    if (currentday > inputday){
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
      this.dateMsg = "DAY IS NOT CORRECT"
    }
    console.log("DATE ERROR MESSAGE:--", this.dateMsg)
  }

  updateCompanyInfo(values){
    var self = this;
  }

  accountTypeChange(value){
    (<FormGroup>this.addAccountTypeForm).patchValue({'obtype':''}, { onlySelf: true });
    (<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''}, { onlySelf: true });
    this.selectedCustomer = null
    this.selectedBank = null
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    if (value === 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'banks':'none'}, { onlySelf: true });

    }
    if (value !== 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':''}, { onlySelf: true });
      if (value === 'post_dated_cheque' || value === 'cheque'){
        (<FormGroup>this.addAccountTypeForm).patchValue({'banks':''}, { onlySelf: true });
      }else{
        (<FormGroup>this.addAccountTypeForm).patchValue({'banks':'none'}, { onlySelf: true });
      }
    }
    if (value === 'cash'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'banks':'none'}, { onlySelf: true });
    }

  }


  getAccountDebitCredits(account){
    var self = this
    self.accountDetail = account
    // self.syncAccountDebitCreditDb()
    self._accountDebitCreditService.getDebitCredits(account).then(function(result){
      // console.log("RESULT AFTER ALL ACCOUNY DEBIT/CREDITS:-----", result)
      self.accountDebitCredits = []
      result.docs.forEach(function(doc){
        self.accountDebitCredits.push(doc)
      })
      // console.log("RESULT AFTER ALL CUSTOMER DEBIT CREDITS:-----", self.accountDebitCredits)
      self.accountDebitCredits = _.orderBy(self.accountDebitCredits, ['_id'], ['asc']);
      self.accountDetailFlag = true
    }).catch(function(err){
      console.log(err)
    })
  }

  hideAccountDetail(){
    this.accountDetailFlag = false
    this.loadSourceData()
  }

  filterCustomers(name: string) {
    // console.log("ALL Customers FROM FILTER:=====", this.addAccountTypeForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterBanks(name: string) {
    // console.log("ALL Customers FROM FILTER:=====", this.addAccountTypeForm.controls['customer'].value);
    return this.allBankAccounts.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }



  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  showSubAccounts(type){
    var self = this
    console.log("Selected Accoun Type:===", type)
    // self.syncSubAccountDb()
    self._router.navigate(['/subaccounts', type._id])
    // if (type.accounttype === 'bank' || type.accounttype === 'cash'){
    //   self._router.navigate(['/subaccounts', type._id])
    // }else{
    //   self._accountTypesService.getChildLevelAccounts(type).then(function(result){
    //     console.log("RESULT AFTER GETTING ALL CHILD ACCOUNTS:-", result)
    //     self.allChildAccount = []
    //     result.docs.forEach(function(doc){
    //       self.allChildAccount.push(doc)
    //     })
    //     console.log("ALL CHILD ACCOUNTS TO SHOW:-", self.allChildAccount)
    //     self._router.navigate(['/subaccounts', type._id])
    //   }).catch(function(err){
    //     console.log(err)
    //   })
    // }
  }

  logout(){
    this._loginService.logout()
  }  




  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}

/** Builds and returns a new User. */
function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  // console.log("ROW TPO SHOW", row)
    if (row.accounttype === 'bank' || row.accounttype === 'cash'){

      return {
        name: row.name.toString(),
        _id: row._id,
        openingbalance: row.openingbalance.toString(),
        closingbalance: row.closingbalance.toString(),
        status: row.status,
        customername: row.customer.name,
        voucherno: row.voucherno,
        type:row.accounttype
      };
    }
    if (row.accounttype === 'card' || 
        row.accounttype === 'parchi' || 
        row.accounttype === 'cheque' || 
        row.accounttype === 'post_dated_cheque'){
      
      return {
        _id: row._id,
        name: row.name.toString(),
        openingbalance: row.openingbalance.toString(),
        closingbalance: row.remainbalance.toString(),
        customername: row.customer.name,
        status: row.expirystatus,
        voucherno: row.voucherno,
        type:row.accounttype
      };
    }

}