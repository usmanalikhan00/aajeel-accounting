import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
import { InvoiceReceiptService } from '../../services/invoicereceipt.service'
import { PurchasePaymentService } from '../../services/purchasepayment.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray, FormGroupDirective } from '@angular/forms';
import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import {Observable} from 'rxjs/Observable';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ElectronService } from 'ngx-electron'

export interface UserData {
  name ? : string;
  openingbalance ? : string;
  closingbalance ? : string;
  remainbalance ? : string;
  type ? : string;
  accountnumber ? : string;
  customername ? : string;
  _id ? : string;
  _rev ? : string;
  expirydate ? : string;
  expirystatus ? : string;
  level ? : string;
  obtype ? : string;
  refnumber ? : string;
  status ? : string;
}

export interface UserData2 {
  debit ? : number;
  credit ? : number;
  closingbalance ? : number;
  openingbalance ? : number;
  type ? : string;
  accountnumber ? : string;
  customername ? : string;
  voucherno ? : string;
  dcref ? : string;
  dctype ? : string;
  datetime ? : string;
  _id ? : string;
  _rev ? : string;
  obtype ? : string;
  status ? : string;
}



@Component({
    selector: 'sub-account-type',
    providers: [LoginService, 
                AccountTypesService, 
                ElectronService, 
                AccountDebitCreditService, 
                PurchasePaymentService, 
                InvoiceReceiptService],
    templateUrl: 'subaccounttype.html',
    styleUrls: ['subaccounttype.css']
})

export class subAccountType {
  
	@ViewChild(FormGroupDirective) myForm;
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  public productSubCategoriesDB = new PouchDB('productsubcategories');

  addBankAccountForm: FormGroup;
  addWrittenDocAccountForm: FormGroup;
  addPostDatedAccountForm: FormGroup;
  addChequeAccountForm: FormGroup;
  addCardAccountForm: FormGroup;
  addCashAccountForm: FormGroup;
  addAccountTypeForm: FormGroup;
  
  allSubAccounts: any = []
  sub: any;
  accountId: any;
  selectedAccount: any;
  accounttype: any = null
  accountToUpdate: any = null
  accountToAdd: any = null

  public accountDebitCreditDB = new PouchDB('aajeelaccaccountdebitcredit');
  public localAccountDebitCreditDB = new PouchDB('http://localhost:5984/aajeelaccaccountdebitcredit');
  public ibmAccountDebitCreditDB = new PouchDB('https://dewansteel.cloudant.com/aajeelaccaccountdebitcredit', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });


  customerStates: Observable<any[]>;  
  allCustomers: any = []
  // selectedCustomer: any = null
  // customerToUpdate: any = null
  // customerDebitCredit: any = null

  balanceCheckFlag: boolean = false

  allTypes = [
    {
      'name':'Post Dated Cheque',
      'type':'post_dated_cheque'
    },
    {
      'name':'Parchi',
      'type':'parchi'
    },
    {
      'name':'Card',
      'type':'card'
    },
    {
      'name':'Cheque',
      'type':'cheque'
    }
  ]

  dateMsg: any = null
  accountDebitCredits: any = []
  accountDetail: any = null

  allRefAccountPayment: any = []
  allChildAccount: any = []
  
  displayedColumns = [
    'name', 
    'openingbalance', 
    'closingbalance', 
    'type',
    'status'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns2 = [
    'datetime',
    'voucherno',
    'customername', 
    'debit', 
    'credit', 
    'dctype', 
    'dcref', 
    'closingbalance', 
    'status', 
  ];
  dataSource2: MatTableDataSource<UserData2>;

  @ViewChild(MatPaginator) paginator2: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);



  authUser: any = null
  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark', scrollButtons: { enable: true } };

  constructor(private _loginService: LoginService,
              private _accountTypesService: AccountTypesService, 
              private _invoiceReceiptService: InvoiceReceiptService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _purchasePaymentService: PurchasePaymentService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _electronService: ElectronService, 
              private _router: Router) {
    var self = this;
    self._buildaddAccountTypeForm()
    self.loadDefaults()
    self.authUser = JSON.parse(localStorage.getItem('user'))
    // self.sub = self._activatedRouter.params.subscribe(params => {
    //   self.accountId = params['accountId']; // (+) converts string 'id' to a number
    //   console.log("ACCount ID TO ADD SUB ACCOUNT:-----", self.accountId)
    //   self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
    //   // console.log("RESULT FROM SINLE CATEGORY", result)
	   //    self.selectedAccount = result.docs[0]
	   //    console.log("RESULT FROM SINLE ACCOUNT", self.selectedAccount)
	   //    self.accounttype = self.selectedAccount.accounttype
    //     console.log("ALL SUB ACCOUNT OF THIS TYPE:----", self.allSubAccounts)
    //     // self.getChildLevelAccounts(self.selectedAccount)
    //     if (self.accounttype === 'post_dated_cheque' || 
    //     		self.accounttype === 'parchi' || 
    //     		self.accounttype === 'cheque' || 
    //     		self.accounttype === 'card'){
	   //    	self.loadSourceData(self.selectedAccount)
    //     }else{
    //     	self.getAccountDebitCredits(self.selectedAccount)
    //     }
	   //    // self.getAllSubAccounts()
	   //  }).catch(function(err){
	   //      console.log(err)
	   //  })
    // })

  }

  _buildaddAccountTypeForm(){
    this.addAccountTypeForm = this._formBuilder.group({
      name: ['', Validators.required],
      expirydate: ['', Validators.required],
      accounttype: ['', Validators.required],
      refnumber: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }

  loadDefaults(){
			var self = this
			self.sub = self._activatedRouter.params.subscribe(params => {
      self.accountId = params['accountId']; // (+) converts string 'id' to a number
      console.log("ACCount ID TO ADD SUB ACCOUNT:-----", self.accountId)
      self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
	      self.selectedAccount = result.docs[0]
        self.minDate = new Date(self.selectedAccount.expirydate)
	      console.log("RESULT FROM SINLE ACCOUNT", self.selectedAccount)
	      self.accounttype = self.selectedAccount.accounttype
        console.log("ALL SUB ACCOUNT OF THIS TYPE:----", self.allSubAccounts)
        // self.getChildLevelAccounts(self.selectedAccount)
        if (self.accounttype === 'post_dated_cheque' || 
        		self.accounttype === 'parchi' || 
        		self.accounttype === 'cheque' || 
        		self.accounttype === 'card'){
	      	self.loadSourceData(self.selectedAccount)
        }else{
        	self.getAccountDebitCredits(self.selectedAccount)
        }
	      // self.getAllSubAccounts()
	    }).catch(function(err){
	        console.log(err)
	    })
    })  	
  }

  ngOnInit(){
    var self = this;
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.accountId = params['accountId']; // (+) converts string 'id' to a number
      console.log("ACCount ID TO ADD SUB ACCOUNT:-----", self.accountId)
      // self.getAllSubAccounts()
      // self.syncAccountDebitCreditDb()
    });
  }
  
  loadSourceData(type){
    var self = this;
    const users: UserData[] = [];
    // self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
    //   // console.log("RESULT FROM SINLE CATEGORY", result)
    //   self.selectedAccount = result.docs[0]
    //   console.log("RESULT FROM SINLE ACCOUNT", self.selectedAccount)
    //   self.accounttype = self.selectedAccount.accounttype
	    self._accountTypesService.getChildLevelAccounts(type).then(function(result){
	      console.log("RESULT AFTER GETTING ALL CHILD ACCOUNTS:-", result)
	      // self.allChildAccount = []
	      result.docs.forEach(function (row) { 
	        users.push(createNewUser(row)); 
	      });
	      self.dataSource = new MatTableDataSource(users);
	      self.dataSource.paginator = self.paginator;
	      self.dataSource.sort = self.sort;
	      // console.log("ALL CHILD ACCOUNTS TO SHOW:-", self.allChildAccount)
        if (type.obtype === 'Credit'){
	        console.log("CREDIT ENTRY:----", self.selectedAccount)
          self.getPurhaseAccountPayments(self.selectedAccount)
          
        }else{
          console.log("DEBIT ENTRY:----", self.selectedAccount)
          self.getRefAccountPayments(self.selectedAccount)
        }
      }).catch(function(err){
        console.log(err)
	    })
    // }).catch(function(err){
    	
    // })
  }
  
  getPurhaseAccountPayments(type){
    var self = this
    // self.accountDetail = account
    console.log("********ACCOUNT DETAIL FROM FUCNITON:-----", self.accountDetail)
    // self.selectedAccount = account
    self._purchasePaymentService.getRefAccountPayments(type._id).then(function(result){
      console.log("RESULT AFTER ALL PURCHASE BANK CASH PAYMENTS:-----", result)
      self.allRefAccountPayment = []
      result.docs.forEach(function(doc){
        self.allRefAccountPayment.push(doc)
      })
      // self.allRefAccountPayment = _.orderBy(self.accountDebitCredits, ['_id'], ['asc']);
      console.log("RESULT AFTER ALL BANK CASH PAYMENTS FROM PURCAHSE:-----", self.allRefAccountPayment)
      // self.accountDetailFlag = true
    }).catch(function(err){
      console.log(err)
    })
  }

  syncAccountDebitCreditDb(){
    var self = this
    var sync = PouchDB.sync(self.accountDebitCreditDB, self.ibmAccountDebitCreditDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadDefaults()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("***********************DEBIT CREDIT HOYA HAI:--", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  applyFilter2(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource2.filter = filterValue;
  }



  goToPrint(){
    // this._router.navigate(['accountsledger', this.selectedAccount._id])
    this._electronService.ipcRenderer.send('AccountLedgerPrint', this.selectedAccount._id)
  }

  getAllSubAccounts(){
    var self = this;
    self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedAccount = result.docs[0]
      console.log("RESULT FROM SINLE ACCOUNT", self.selectedAccount)
      self.accounttype = self.selectedAccount.accounttype
      self._accountTypesService.getSingleAccountItems(self.accountId).then(function(doc){
        console.log(doc)
        self.allSubAccounts = [];
        for (let row of doc.docs){
          self.allSubAccounts.push(row)
        }
        console.log("ALL SUB ACCOUNT OF THIS TYPE:----", self.allSubAccounts)
        self.getAccountDebitCredits(self.selectedAccount)
        self.getRefAccountPayments(self.selectedAccount)
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
        console.log(err)
    })

  }

  getChildLevelAccounts(type){
  	var self = this
    self._accountTypesService.getChildLevelAccounts(type).then(function(result){
      console.log("RESULT AFTER GETTING ALL CHILD ACCOUNTS:-", result)
      self.allChildAccount = []
      result.docs.forEach(function(doc){
        self.allChildAccount.push(doc)
      })
      console.log("ALL CHILD ACCOUNTS TO SHOW:-", self.allChildAccount)
      self._router.navigate(['/subaccounts', type._id])
    }).catch(function(err){
      console.log(err)
    })
  }

  getRefAccountPayments(account){

    var self = this
    // self.accountDetail = account
    console.log("********ACCOUNT DETAIL FROM FUCNITON:-----", self.accountDetail)
    // self.selectedAccount = account
    
    self._invoiceReceiptService.getRefAccountPayments(account._id).then(function(result){
      console.log("RESULT AFTER ALL BANK CASH PAYMENTS:-----", result)
      self.allRefAccountPayment = []
      result.docs.forEach(function(doc){
        self.allRefAccountPayment.push(doc)
      })
      
      // self.allRefAccountPayment = _.orderBy(self.accountDebitCredits, ['_id'], ['asc']);
      console.log("RESULT AFTER ALL BANK CASH PAYMENTS:-----", self.allRefAccountPayment)
      // self.accountDetailFlag = true
    }).catch(function(err){
      console.log(err)
    })    
  }

  getAccountDebitCredits(account){
    var self = this
    self.accountDetail = account
    console.log("********ACCOUNT DETAIL FROM FUCNITON:-----", self.accountDetail)
    const users2: UserData2[] = [];
    self._accountDebitCreditService.getDebitCredits(account).then(function(result){
      console.log("RESULT AFTER ALL ACCOUNY DEBIT/CREDITS:-----", result)
      result.docs.forEach(function (row) { 
        users2.push(createNewUser2(row)); 
      });
      self.dataSource2 = new MatTableDataSource(users2);
      self.dataSource2.paginator = self.paginator2;
      self.dataSource2.sort = self.sort2;
    }).catch(function(err){
      console.log(err)
    })
  }

  accountTypeChange(value){
    (<FormGroup>this.addAccountTypeForm).patchValue({'obtype':''}, { onlySelf: true });
    (<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''}, { onlySelf: true });
  	(<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, { onlySelf: true });
  	(<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':''}, { onlySelf: true });
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
  }

  dateChange($event){
    this.dateMsg = null
    console.log("DATE CHANGES:----", $event.target.value)
    var inputyear = parseInt(moment($event.target.value,"YYYY/MM/DD").format("YYYY"))
    var inputmonth = parseInt(moment($event.target.value,"YYYY/MM/DD").format("MM"))
    var inputday = parseInt(moment($event.target.value,"YYYY/MM/DD").format("DD"))
    console.log("YEAR AFTER FORMATING INPUT DATE:--\n", "INPUT YEAR\n", inputyear, typeof(inputyear), "INPUT MONTH\n", inputmonth, typeof(inputmonth), "INPUT DAYY\n", inputday, typeof(inputday))
    var currentyear = parseInt(moment().format("YYYY"))
    var currentmonth = parseInt(moment().format("MM"))
    var currentday = parseInt(moment().format("DD"))
    console.log("STATS OF TODAY:--", currentyear, typeof(currentyear), currentmonth, typeof(currentmonth), currentday, typeof(currentday))

    // var dateMsg = null
    if (inputyear < currentyear){
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
      this.dateMsg = "YEAR IS NOT CORRECT"
    }
    if (inputmonth != 1){
      if (currentmonth > inputmonth){
        (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
        this.dateMsg = "MONTH NOT CORRECT"
      }
    }
    if (currentday > inputday){
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, {onlySelf: true})
      this.dateMsg = "DAY IS NOT CORRECT"
    }
    console.log("DATE ERROR MESSAGE:--", this.dateMsg)
  }


  checkRemainBalance($event){
  	console.log("BALANCE ENTERED IS:----", $event.target.value)
  	if (parseInt($event.target.value) < 0){
			(<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''},{onlySelf: true})
  	}else{
			console.log("REMAINING BALCE:----", this.selectedAccount.remainbalance)
	  	if (parseInt($event.target.value) > this.selectedAccount.remainbalance){
				(<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''},{onlySelf: true})
				this.balanceCheckFlag = true
	  	}else{
  			this.balanceCheckFlag = false
	  	}
  	}
  }


  addSubAccount(values){
    var self = this;
    values._id = new Date().toISOString()
    values.accountId = self.selectedAccount._id
    values.accountref = self.selectedAccount
    values.expirystatus = 'pending'
    values.customer = self.selectedAccount.customer
    values.customerid = self.selectedAccount.customer._id
    values.accountnumber = 'none'
    values.expirydate = values.expirydate.toISOString()
    console.log("SUB ACCOUN TO ADD:=====", values);
    self.accountToUpdate = {
    	"_id":self.selectedAccount._id,
    	"_rev":self.selectedAccount._rev,
    	"accounttype":self.selectedAccount.accounttype,
    	"accountnumber":self.selectedAccount.accountnumber,
    	"customer":self.selectedAccount.customer,
    	"customerid":self.selectedAccount.customerid,
    	"expirydate":self.selectedAccount.expirydate,
    	"expirystatus":self.selectedAccount.expirystatus,
    	"name":self.selectedAccount.name,
    	"obtype":self.selectedAccount.obtype,
    	"openingbalance":self.selectedAccount.openingbalance,
    	"level":self.selectedAccount.level,
    	"parentid":self.selectedAccount.parentid,
    	"remainbalance":self.selectedAccount.remainbalance - values.openingbalance,
    	"refnumber":self.selectedAccount.refnumber
    }
    self.accountToAdd = {
    	"_id":values._id,
    	"accounttype":values.accounttype,
    	"accountnumber":values.accountnumber,
    	"customer":values.customer,
    	"customerid":values.customerid,
    	"expirydate":values.expirydate,
    	"expirystatus":values.expirystatus,
    	"name":values.name,
    	"obtype":self.selectedAccount.obtype,
    	"level":'child',
    	"parentid":self.selectedAccount._id,
    	"openingbalance":values.openingbalance,
    	"remainbalance":values.openingbalance,
    	"refnumber":values.refnumber
    }
    console.log("PARENT ACCOUNT TO ADD:=====", self.accountToAdd);
    console.log("SELECTED ACCOUNT:=====", self.selectedAccount);
    if (self.accountToUpdate.remainbalance === 0){
    	self.accountToUpdate.expirystatus = 'process'
    }
    console.log("ACTUAL PARENT ACCOUNT TO UPDATE:=====", self.accountToUpdate);
    self.selectedAccount.openingbalance = self.selectedAccount.openingbalance - values.openingbalance
    self._accountTypesService.addSubAccount(values).then(function(result){
      console.log("SUN ACCOUNT ADDED:---------", result)
      // self._accountTypesService.addAccountType()
      self._accountTypesService.updateAccountBalance(self.accountToUpdate).then(function(result){
      	console.log("RESULT AFTER UPDATING ACCOUNT:---------", result)
      	self._accountTypesService.addAccountType(self.accountToAdd).then(function(result){
      		console.log("RESULT AFTER ADDING PARENT ACCOUNT:---------", result)
	      	// self.addAccountTypeForm.reset()
	      	self.myForm.resetForm()
	      	// self.getAllSubAccounts()
	      	self.loadDefaults()
	        self.getAccountDebitCredits(self.selectedAccount)
	        self.getRefAccountPayments(self.selectedAccount)
      	}).catch(function(err){
      		console.log(err)
      	})
      }).catch(function(err){
      	console.log(err)
      })
    }).catch(function(err) {
      console.log(err)
    })

  }

  filterCustomers(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addAccountTypeForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
      if (event.target.innerWidth < 886) {
          this.navMode = 'over';
          this.sidenav.close();
      }
      if (event.target.innerWidth > 886) {
         this.navMode = 'side';
         this.sidenav.open();
      }
    }

  showSubAccounts(type){
    console.log("Selected Accoun Type:===", type)
    this._router.navigate(['/subaccounts', type._id])
  }

  goBack(){
    this._router.navigate(['cashsettings'])
  }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

/** Builds and returns a new User. */
function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  console.log("ROW TPO SHOW", row)
    if (row.accounttype === 'bank' || row.accounttype === 'cash'){

      return {
        name: row.name.toString(),
        _id: row._id,
        openingbalance: row.openingbalance.toString(),
        closingbalance: row.closingbalance.toString(),
        // remainbalance: null,
        status: row.status,
        customername: row.customer.name,
        type:row.accounttype
      };
    }
    if (row.accounttype === 'card' || row.accounttype === 'parchi' || row.accounttype === 'cheque' || row.accounttype === 'post_dated_cheque'){

      return {
        _id: row._id,
        name: row.name.toString(),
        openingbalance: row.openingbalance.toString(),
        closingbalance: row.remainbalance.toString(),
        customername: row.customer.name,
        status: row.expirystatus,
        // remainbalance: row.remainbalance.toString(),
        type:row.accounttype
      };
    }

}

function createNewUser2(row): UserData2 {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  console.log("ROW TPO SHOW FOR DEBIT CREDIT", row)
  if (!row.voucherno){

    return {
      _id: row._id,
      closingbalance: row.closingbalance,
      debit: row.debit,
      credit: row.credit,
      status: row.status,
      customername: row.customer.name.toString(),
      datetime: row._id,
      dctype: row.dctype.toString(),
      dcref: row.dcref.toString(),
      voucherno: null,
      obtype:row.obtype
    };  
  }else{
    return {
      _id: row._id,
      closingbalance: row.closingbalance,
      debit: row.debit,
      credit: row.credit,
      status: row.status,
      customername: row.customer.name.toString(),
      datetime: row._id,
      dctype: row.dctype.toString(),
      dcref: row.dcref.toString(),
      voucherno: row.voucherno.toString(),
      obtype:row.obtype
    };


  }
    

}

