import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'login-form',
    providers: [LoginService],
    templateUrl: 'login.html',
    styleUrls: ['login.css']
})

export class Login {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');

  users: any = [];
  loginForm: FormGroup;
  authUser: any = null
  email: string = null;
  password: string = null;


  public usersDB = new PouchDB('users');
  public localUsersDB = new PouchDB('http://localhost:5984/localusers');
  public ibmUsersDB = new PouchDB('https://dewansteel.cloudant.com/dewanusers', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
                                                // 50c48281-2175-4509-b1f1-85ed263a98b4-bluemix.cloudant.com
  public cloudantUsersDB = new PouchDB('https://50c48281-2175-4509-b1f1-85ed263a98b4-bluemix.cloudant.com/aajeelusers/', {
    auth: {
      "apikey": "dNUAtkCm-F9yLRp6U_Bk5fRz6OIQTIzmu7lF2pRUgZy8",
      "host": "50c48281-2175-4509-b1f1-85ed263a98b4-bluemix.cloudant.com",
      "iam_apikey_description": "Auto generated apikey during resource-key operation for Instance - crn:v1:bluemix:public:cloudantnosqldb:us-south:a/18b7d0d1f1c54fef86499d0c66f1aec8:cf32582f-214a-4da5-b821-c56845b22c7f::",
      "iam_apikey_name": "auto-generated-apikey-c4f6f1cf-97f6-4419-995f-62edf5c21f97",
      "iam_role_crn": "crn:v1:bluemix:public:iam::::serviceRole:Manager",
      "iam_serviceid_crn": "crn:v1:bluemix:public:iam-identity::a/18b7d0d1f1c54fef86499d0c66f1aec8::serviceid:ServiceId-3b0ab81b-db98-427e-a277-3401fa7f85a8",
      "password": "6bafc065f244fc998a7731dfea6c302ed23b0d0a8a05a1d7413e4c056fc92f79",
      "port": 443,
      "url": "https://50c48281-2175-4509-b1f1-85ed263a98b4-bluemix:6bafc065f244fc998a7731dfea6c302ed23b0d0a8a05a1d7413e4c056fc92f79@50c48281-2175-4509-b1f1-85ed263a98b4-bluemix.cloudant.com",
      "username": "50c48281-2175-4509-b1f1-85ed263a98b4-bluemix"
    }
  });

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildLoginForm();
    this.syncDb()

  }

  private _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    console.log("LOCAL STORAGE ID:--------", user)
    if (user){
      if (user._id === "admin"){
        self._router.navigate(['admindashboard'])
      }
      if (user._id === "user"){
        self._router.navigate(['userdashboard'])
      }
      if (user._id === "deo"){
        self._router.navigate(['deodashboard'])
      }
    }
    // self._loginService.addUsers().then(function (result) {
    //   console.log("RESULT AFTER ADDING:=======", result);
    // }).catch(function (err) {
    //   console.log(err);
    // });
  }

  syncDb(){
    var self = this
    var sync = PouchDB.sync(self.userDB, self.cloudantUsersDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        // self.getAllStores()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("***********************USERS SYNC HOYE HAIN:--", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }

  getCredentials(values){
    var self = this;
    self.authUser = []
    console.log("GET CREDENTIALS:------", values);
    self._loginService.checkCredentials(values).then(function (doc) {
      console.log("RESULT AFTER CHECK CREDENTIALS:+++", doc)
      
      for (let row of doc.docs){
        self.authUser.push(row)
      }

      if (self.authUser.length != 0){
        localStorage.setItem('user', JSON.stringify(self.authUser[0]))
        var user = JSON.parse(localStorage.getItem('user'))
        console.log("LOCAL STORAGE ID:--------", user)
      }else{
        self.errorMsg = 'Invalid Credentials'
      }


      if (user._id === "admin"){
        self._router.navigate(['admindashboard'])
      }
      if (user._id === "user"){
        self._router.navigate(['userdashboard'])
      }
      if (user._id === "deo"){
        self._router.navigate(['deodashboard'])
      }

    }).catch(function (err) {
      console.log(err);
    });
  }

  navigateUser(userId, user){
    console.log("USER ID TO NAVIGATE", userId, user);
      this._router.navigate(['admindashboard'])
    // if (userId === "admin"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   console.log("TRUE ADMIN")
    //   // this._router.navigate(['admindashboard'])
    //   // location.href = '#/admindashboard'
    // }
    // if (userId === "user"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   // this._router.navigate(['admindashboard'])
    //   console.log("TRUE USER")
    // }
    // if (userId === "deo"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   // this._router.navigate(['admindashboard'])
    //   console.log("TRUE DEO")
    // }

  }
}