import { Component, ElementRef, ViewChild, HostListener, ChangeDetectorRef, Inject } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
// import { PurchasesService } from '../../services/purchase.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { InwardPassService } from '../../services/inwardpass.service'
import { OutwardPassService } from '../../services/outwardpass.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { Router } from  '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as _ from 'lodash'
import {Http, Headers, RequestOptions} from '@angular/http';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { ElectronService } from 'ngx-electron';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
// require('events').EventEmitter.prototype._maxListeners = 100;
// require('events').EventEmitter.defaultMaxListeners = 100;
// PouchDB.setMaxListeners(50)


export interface UserData {
  name ? : string;
  price ? : number;
  stockvalue ? : number;
  networth ? : number;
  netweight ? : number;
  batchnumber ? : string;
  categoryname ? : string;
  category ? : string;
  type ? : string;
  subcategory ? : string;
  stores ? : string[];
  weight ? : number;
  storeStocks ? : number;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'products',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                MediaMatcher, 
                ElectronService,
                InwardPassService,
                OutwardPassService, 
                ItemDebitCreditService],
    templateUrl: 'products.html',
    styleUrls: ['products.css']
})

export class products {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  
  // public productsDB = new PouchDB('products');
  public productsDB = new PouchDB('aajeelaccproducts');
  public localProductsDB = new PouchDB('http://localhost:5984/localsteelproducts');
  public cloudantProductsDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccproducts', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });



  public itemDebitCreditDB = new PouchDB('aajeelaccitemdebitcredit');
  public localItemDebitCreditDB = new PouchDB('http://localhost:5984/localitemdebitcredit');
  public cloudantItemDebitCreditDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccitemdebitcredit', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });



  users: any = [];
  addProductFlag: boolean = false;
  public invoiceForm: FormGroup;

  addProductForm: FormGroup;
  editProductForm: FormGroup;
  allProducts: any = [];

  allCategories: any = [];
  allStores: any = [];
  allSubCategories: any = [];
  selectedStores: any = [];
  storeCount: any = 0;
  selectedCategory: any;
  selectedSubCategory: any;
  enableProductForm: boolean = false
  editProductFlag: boolean = false
  stockFlag: boolean = false
  productTypes = [
    'Weighted',
    'Single Unit'
  ];

  toppings = new FormControl();
  toppingList = [];
  productTotalStock: any = 0
  productTotalWeight: any = 0

  stockItemDetails: any = []
  itemDebitCredit: any = []
  itemDetailFlag: boolean = false
  selectedItem: any = null


  displayedColumns = [
    'name', 
    'stores', 
    'category', 
    'weight', 
    'price', 
    'stockvalue', 
    'netweight',
    'networth',
    'action'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark', scrollButtons: { enable: true } };

  categoryToAdd: any = null
  subCategoryToAdd: any = null
  authUser: any = null
  productToEdit: any = null

  animal: string;
  name: string;

  selectedCategoryName: any = null
  itemToRemove: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _outwardPassService: OutwardPassService, 
              private _storeSettingsService: StoreSettingsService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _electronService: ElectronService, 
              private _matSnackBar: MatSnackBar, 
              private _cdr: ChangeDetectorRef, 
              private _media: MediaMatcher, 
              private _formBuilder: FormBuilder,
              public _dialog: MatDialog, 
              private _router: Router) {
    var self = this
    self._buildAddProductForm();
    self._buildEditProductForm();
    self.loadSourceData()
    // self.getAllProducts()
    self.authUser = JSON.parse(localStorage.getItem('user'))
  }

  private _buildAddProductForm(){
    
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      stores: [''],
      // storeStocks: [''],
      type: ['', Validators.required]
    })

  }

    private _buildEditProductForm(){
    
    this.editProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      stores: [''],
      // storeStocks: [''],
      type: ['', Validators.required]
    })

  }



  loadSourceData(){
    var self = this;
    var sum = 0
    var totalweight = 0
    const users: UserData[] = [];
    self._productService.allProducts().then(function(result){
      console.log("Reuslt after fetching all Parent accounts:---", result)
      self.allProducts = []
      result.rows.map(function (row) {
        // if (!row.doc.categoryname){
        //   row.doc.categoryname = null
        // }
        users.push(createNewUser(row.doc)); 
      })
      // for (let user of users){
      //   self._productService.getSingleCategory(user.category).then(function(result2){
      //     user.categoryname = result2.docs[0].name
      //     self.dataSource = new MatTableDataSource(users);
      //     self.dataSource.paginator = self.paginator;
      //     self.dataSource.sort = self.sort;
      //   }).catch(function(err){
      //       console.log(err)
      //   }) 
      // }
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;      
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc); 
        sum = sum + row.doc.networth
        if (row.doc.type === "Weighted"){
          totalweight = totalweight + row.doc.netweight
          // console.log("IN MAP LOOP:(((((((((((*******", row.doc.netweight)
        }
      })
      self.productTotalStock = sum
      self.productTotalWeight = totalweight
      // console.log("TOTAL WEIGHT AND WORTH:(((((((((((*******", self.productTotalWeight, self.productTotalStock)
    }).catch(function(err){
      console.log(err)
    })
  }

  ngOnInit(){
    var self = this;
    if (window.innerWidth < 886) {
      self.navMode = 'over';
    }
    // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
    self.getAllCategories()
    self.getAllStores()
    // self.syncDb()
    // self.syncItemDebitCredit()
  }

  cloudantSync(){
    var self = this
    var opts = { live: true, retry: true };
    self.productsDB.replicate.to(self.cloudantProductsDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY PRODUCT REPLICATION:--", info)
      // self.productsDB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
      //   console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT PRODUCTS SYNC:--", info)
      //   if (info.direction === 'pull' || info.direction === 'push'){
      //     self.loadSourceData()
      //   }
      // }).on('paused', function (err) {
      //   console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT PRODUCTS", err)
      // }).on('active', function () {
      //   console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS CLOUDANT SYNC!!")
      // }).on('denied', function (err) {
      //   console.log("DENIED DENIED !!", err)
      // }).on('complete', function (info) {
      //   console.log("COMPLETED !!", info)
      // }).on('error', function (err) {
      //   console.log("ERROR ERROR FROM SYNC CLOUDANT PRODUCTS!!", err)
      // })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT PRODUCTS !!", err)
    })
  }

  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.productsDB.replicate.from(self.cloudantProductsDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY PRODUCT REPLICATION:--", info)
      self.productsDB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY PRODUCTS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.loadSourceData()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC PRODUCTS", err)
        // self.productsDB.sync(self.ibmProductsDB).cancel()
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC PRODUCTS!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION PRODUCTS !!", err)
    })

  }

  syncItemDebitCredit(){
    var self = this
    var opts = { live: true, retry: true };
    self.itemDebitCreditDB.replicate.from(self.cloudantItemDebitCreditDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY DEBIT/CREDIT REPLICATION:--", info)
      self.itemDebitCreditDB.sync(self.cloudantItemDebitCreditDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY DEBIT/CREDIT SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          console.log("----PULL FROM ITEM TWO-WAY DEBIT/CREDIT-----")
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT TWO-WAY SYNC ITEM DEBIT/CREDIT", err)
      }).on('active', function () {
        console.log("****ACTIVE ACTIVE TWO-WAY SYNC ITEM DEBIT/CREDIT****")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    });
  }

  openDialog(product): void {
    console.log("PRODUCT CLICKED:------", product)
    let dialogRef = this._dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: { name: product.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
      console.log('The dialog was closed', result, this.animal);
      if (this.animal){
        // alert(1)
        // console.log('YES TYPED');
        this.removeProduct(product)
      }
    })
  }
  
  openLedgerDialog(item): void {
    var self = this
    console.log("ITEM CLICKED:------", item)
    if (item.dctype === "purchase"){
      self._purchaseService.getSinglePurchase(item.dcrefid).then(function(result){

        console.log("RESULT FROM SINGLE PURCHASE:--", result);
        let dialogRef = self._dialog.open(ledgerDetailDialog, {
          width: '100%',
          data: { info: result.docs[0], dcref: 'purchase' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // self.animal = result;
          console.log('The Ledger detail was closed', result);
          // if (this.animal){
          //   // alert(1)
          //   // console.log('YES TYPED');
          //   this.removeProduct(product)
          // }
        })
      }).catch(function(err){

        console.log(err);
      })
    }
    if (item.dctype === "invoice"){
      self._invoiceService.getSingleInvoice(item.dcrefid).then(function(result){

        console.log("RESULT FROM SINGLE INVOICE:--", result);
        let dialogRef = self._dialog.open(ledgerDetailDialog, {
          width: '100%',
          data: { info: result.docs[0], dcref: 'invoice' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // self.animal = result;
          console.log('The Ledger detail was closed', result);
          // if (this.animal){
          //   // alert(1)
          //   // console.log('YES TYPED');
          //   this.removeProduct(product)
          // }
        })
      }).catch(function(err){

        console.log(err);
      })
    }


  }



  goToPrint(){
    // this._router.navigate(['/printproducts'])
    this._electronService.ipcRenderer.send('ProductsPrint')
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      self.toppingList = []
      for (let store of self.allStores){
        var doc = {
          name:store.name,
          stock:null,
          _id:store._id,
          _rev:store._rev
        }
        self.toppingList.push(doc)        
      }
      // console.log("ALL Stores:=====", self.allStores);
    }).catch(function(err){

    })
  }

  multiSelectStore(values){
    // console.log("MULTISELECT CHANGE CALLED", values)
    this.selectedStores = []
    for (let store of values.value){
      this.selectedStores.push(store)
    }
  }


  getProductType($event){
    // console.log("PRODUCT TYPE CHANGE CALLED:-------", $event.value)
    if ($event.value === 'Weighted'){
      (<FormGroup>this.addProductForm).patchValue({'weight':''}, { onlySelf: true });
    }else if ($event.value === 'Single Unit'){
      (<FormGroup>this.addProductForm).patchValue({'weight':'none'}, { onlySelf: true });
    }
  }

  setProductStore($event){
    (<FormGroup>this.addProductForm).patchValue({'stores':''}, { onlySelf: true });
    var tempStore = $event.value
    var productStore: any = null
    // console.log("RESET STORE EVENT", tempStore)
    for (let item of this.allStores){
      if (item._id === tempStore){
        productStore = item
      }
    }
    // console.log("PRODUCT STORE TO PUSH", productStore)
    var existFlag: boolean = false
    if (this.selectedStores.length === 0){
      console.log("RESET STORE EVENT First", tempStore)
      productStore.stock = null
      this.selectedStores.push(productStore)
      this.stockFlag = true
    }else{
      // console.log("RESET STORE EVENT SECONDNNNNN", productStore)
      for (let i=0; i<this.selectedStores.length; i++){
        var key = this.selectedStores[i]
        if (key._id == productStore._id){
          key.stock = productStore.stock
          existFlag = true
          break
        }else{
          existFlag = false
        }

      }
      if(!existFlag){
        productStore.stock = null
        this.selectedStores.push(productStore)
        this.stockFlag = true
      }else{
        // console.log("VALUES EXISTS")
      }
    }
    this._cdr.detectChanges()
    // console.log("ALL STORES TO SAVE", this.selectedStores)
  }


  changeStoreStocks($event, store){
    // store.stock = stock.toString()
    // console.log("ALL STORES AFETR STOCK CHANGE:---", store, $event)
    // console.log("ALL STORES AFETR CHANGING:---", this.selectedStores)

  }

  getAllProducts(){

    var self = this;
    self._productService.allProducts().then(function(result){
      self.allProducts = [];
      result.rows.map(function (row) {
        // for (let obj of row.doc.stores){
        //   obj.stock = 0
        // }
        // row.doc.stockvalue = 0 
        // row.doc.storeStocks = 0
        if (row.doc.type == "Weighted"){
          row.doc.weight = 0 
        }else{
          row.doc.weight = 'none'
        }
        row.doc.netweight = 0 
        row.doc.networth = 0 
        self.allProducts.push(row.doc); 
      });
      // self.calculateStockTotal(self.allProducts)
      self._productService.updateInvoiceProducts(self.allProducts).then(function(result){
        console.log("ALL PRODUCTS UPDATE CLOUDANT:=====", self.allProducts);

      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err);
    })

  }


  calculateStockTotal(products){
    var sum = 0
    for (let item of products){
      sum = sum + item.networth
    }
    this.productTotalStock = sum
  }

  addProduct(values){

    var self = this;
    
    var ID = Math.floor(Math.random()*100000000) + 800000000
    // console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    // console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    // console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    // console.log("MULTI SELECT STORE VALUES:--", self.toppings.value);

    values.categoryname = self.categoryToAdd.name
    // console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
    values._id = new Date().toISOString()

    if (!self.stockFlag){
      // console.log("----:STOCK VALUES ENTERED:---")
      self._productService.addProduct(values).then(function(result){
        // console.log("PRODUCT ADDED:===", result);
          self.addProductForm.reset();
          self.toppings.reset();
          self.loadSourceData()
          for (let store of self.selectedStores){
            store.stock = ""
          }
          self.selectedStores = []
      }).catch(function(err){
        console.log(err);
      })
    }else{
      // console.log("----:MUST ENETER STOCK VALUES:---")
      this._matSnackBar.open('Must enter stock values', 'Undo', {
        duration: 3500
      });
    }

  }

  editProduct(product){
    var self = this
    // console.log("EDIT PRIDYC CLICKED:---", product)
    // self.productToEdit = product
    self._productService.getSingleProduct(product._id).then(function(result){
      // console.log("RESULT FROM SINGLE PRODUCT:--", result)
      self.productToEdit = result.docs[0]
      console.log("PRODUCT TO UPDATE:--", self.productToEdit)
      var categoryname = _.filter(self.allCategories, {"_id":self.productToEdit.category})
      // self.productToEdit.categoryname = product.categoryname
      console.log("CTAEGORY NAME TO ADD:--", categoryname[0].name)
      self.productToEdit.categoryname = categoryname[0].name
      // product.categoryname = categoryname[0].name
      // console.log("PRODUCT TO UPDATE AFTER CTAEGORY NAME:--", self.productToEdit)
      self.editProductFlag = true
      self.setValues(self.productToEdit)
      // self.loadSourceData()
    }).catch(function(err){
      console.log(err)
    })

  }

  setValues(product){
    (<FormGroup>this.editProductForm).patchValue({'name':product.name,
                                                 'weight':product.weight,
                                                 'price':product.price,
                                                 'category':product.category,
                                                 'subcategory':product.subcategory,
                                                 'type':product.type,
                                                 'stores':product.stores,}, {onlySelf: true})
    this.toppingList = product.stores
    this.selectedStores = product.stores
    // for (let obj of product.stores){
    //   this.selectedStores.push(obj)
    // }
    this.selectedCategoryName = product.categoryname
    this.toppings.setValue(product.stores, {onlySelf: true})
    

  }

  saveEditProduct(values){
    var self = this
    values.categoryname = self.selectedCategoryName
    console.log("PRODUCT TO SAVE AFTER UPDATE:--", self.productToEdit)

    var sum = 0
    for (let obj of values.stores){
      sum = sum + obj.stock
    }
    values.stockvalue = sum
    values.storeStocks = values.stockvalue
    
    values.batchnumber = self.productToEdit.batchnumber
    values._id = self.productToEdit._id
    values._rev = self.productToEdit._rev
    values.categoryname = this.selectedCategoryName
    if (values.type == 'Single Unit'){
      values.weight = 'none'
      values.networth = values.stockvalue * values.price
      values.netweight = ''
    }else{
      values.networth = values.stockvalue * values.price * values.weight
      values.netweight = values.weight * values.stockvalue
    }
    console.log("PRODUCT TO UPDATE AFTER ALL FUNCTIONS:--", values)
    // (<FormGroup>self.editProductForm).patchValues({'name':self.productTypes.name,}, {onlySelf: true})
    self._productService.saveEditProduct(values).then(function(result){
      console.log("RESULT AFTER UPDATING PRODUCT", result)
      self.editProductFlag = false
      // self.loadSourceData()
      self.toppings.reset()
      self.selectedStores = []
      self.getAllStores()
    }).catch(function(err){
      console.log(err)
    })
  }

  removeProduct(product){
    var self = this
    // console.log("PRODUCT TO REMOVE:---", product)
    // self.productToEdit = product
    self._productService.remvoeProduct(product).then(function(result){
      // console.log("RESULT FROM REMOVING PRODUCT:--", result)
      // self.productToEdit = result.docs[0]
      // self.productToEdit.categoryname = product.categoryname
      // console.log("PRODUCT TO UPDATE:--", self.productToEdit)
      // self.editProductFlag = true
    }).catch(function(err){
      console.log(err)
    })

  }



  selectCategory($event){
    var self = this;
    // console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self.categoryToAdd = _.find(self.allCategories, {'_id': self.selectedCategory})
    if (self.categoryToAdd)
      self.selectedCategoryName = self.categoryToAdd.name
    console.log("CATEGORY TO ADD", self.selectedCategoryName)
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }  

  selectSubCategory($event){
    var self = this;
    // console.log("Sub Category selected", $event);
    self.selectedSubCategory = $event.value
    self.subCategoryToAdd = _.find(self.allSubCategories, {'_id': self.selectedCategory})
    // console.log("SUB CATEGORY TO ADD", self.subCategoryToAdd[0])
    // self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
    //   self.allSubCategories = [];
    //   for (let row of result.docs){
    //     self.allSubCategories.push(row)
    //   }
    // }).catch(function(err){
    //   console.log(err)
    // })
  }  

  hideEdit(){
    this.editProductFlag = false
    this.loadSourceData()
  }

  deleteStores(i, selectedStores){
    // console.log("DELETE CART ITEM CALLED:-", i, selectedStores)
    selectedStores.splice(i, 1)
    this.selectedStores = selectedStores
  }

  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      // console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }



  stockItemDeatils(product){
    var self = this
    // PouchDB.debug.enable('pouchdb:find')
    console.log("STOCK ITEM CLICKED:=====", product)
    // self.syncItemDebitCredit()
    self.itemDebitCredit = []
    self._productService.getSingleCategory(product.category).then(function(category){
      product.categoryname = category.docs[0].name
      self.selectedItem = product
      self._itemDebitCreditService.getDebitCredits(product).then(function(result){
        // console.log("RESULT AFTER PRODUCT DEBITS/CREDITS:==========", result)
        result.docs.forEach(function(doc){
          self.itemDebitCredit.push(doc)
        })
        self.itemDebitCredit = _.orderBy(self.itemDebitCredit, ['createdat'], ['asc']);
        self.itemDetailFlag = true
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })
  }

  gotoPrint(){
    // this._router.navigate(['/inventoryledger', this.selectedItem._id])
    this._electronService.ipcRenderer.send('InventoryLedgerPrint', this.selectedItem._id)
  }

  hideItemDetails(){
    this.itemDetailFlag = false
    this.loadSourceData()
  }

  logout(){
    this._loginService.logout()
  }


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}

/** Builds and returns a new User. */
function createNewUser(row): UserData {
  var self = this
  // console.log("ROW TO SHOW", row)
  if (row.type === 'Weighted'){
    // if (row.categoryname){

      return {
        _id: row._id,
        _rev: row._rev,
        name: row.name.toString(),
        category: row.category,
        price: row.price,
        weight: row.weight,
        type: row.type,
        subcategory: row.subcategory,
        batchnumber: row.batchnumber,
        stockvalue: row.stockvalue,
        netweight: row.netweight,
        storeStocks: row.storeStocks,
        categoryname: row.categoryname,
        stores: row.stores,
        networth:row.networth
      };
    // }else
  }
  if (row.type === 'Single Unit'){
    return {
      _id: row._id,
      _rev: row._rev,
      name: row.name.toString(),
      category: row.category,
      price: row.price,
      weight: row.weight,
      type: row.type,
      subcategory: row.subcategory,
      batchnumber: row.batchnumber,
      stockvalue: row.stockvalue,
      storeStocks: row.storeStocks,
      categoryname: row.categoryname,
      stores: row.stores,
      netweight: 0,
      networth:row.networth
    };
  }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html'
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}

@Component({
  selector: 'ledger-detail',
  templateUrl: 'ledger-detail.html',
  styleUrls: ['products.css']
})
export class ledgerDetailDialog {

  constructor(
    public dialogRef: MatDialogRef<ledgerDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(){
    // console.log("POP UP START DATA:---", this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}

