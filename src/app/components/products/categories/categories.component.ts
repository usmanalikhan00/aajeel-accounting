import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'product-categories',
    providers: [LoginService, ProductService],
    templateUrl: 'categories.html',
    styleUrls: ['categories.css']
})

export class productcategories {
  
  // public productsDB = new PouchDB('steelproducts');
  // public localStoresDB = new PouchDB('http://localhost:5984/localstores');
  // public ibmStoresDB = new PouchDB('https://dewansteel.cloudant.com/dewanstores', {
  //   auth: {
  //     'username': 'dewansteel', 
  //     'password': 'dewansteel@eyetea.co', 
  //   }
  // });
  
  public productCategoriesDB = new PouchDB('productcategories');
  // public productCategoriesDB = new PouchDB('productcategories');
  public localProductCategoriesDB = new PouchDB('http://localhost:5984/localproductcategories');
  public ibmProductCategoriesDB = new PouchDB('https://dewansteel.cloudant.com/dewanproductcategories', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });

  public cloudantProductCategoriesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewanproductcategories', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  addCategoryForm: FormGroup;
  allCategories: any = []
  authUser: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCategoryForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddCategoryForm(){
    this.addCategoryForm = this._formBuilder.group({
      name: ['', Validators.required]
    })
  }

  ngOnInit(){
    this.getAllCategories()
    // this.syncDb()
  }

  syncDb(){
    // var self = this
    // var sync = PouchDB.sync(self.productCategoriesDB, self.cloudantProductCategoriesDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull'){
    //     self.getAllCategories()
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("***********************CATEGORIES SYNC HOYE HAI:--", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var self = this
    var opts = { live: true, retry: true };
    self.productCategoriesDB.replicate.from(self.cloudantProductCategoriesDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY CATEGORIES REPLICATION:--", info)
      self.productCategoriesDB.sync(self.cloudantProductCategoriesDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT CATEGORIES SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.getAllCategories()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT CATEGORIES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY CATEGORIES CLOUDANT SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC CLOUDANT CATEGORIES!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT CATEGORIES!!", err)
    })

  }

  getAllCategories(){
    var self = this;
    self.allCategories = [];
    self._productService.allProductCategories().then(function(result){
      result.rows.map(function (row) { 
        self.allCategories.push(row.doc); 
      });
      console.log("ALL PRODUCT CATEGORIES:=====", self.allCategories);
    }).catch(function(err){
      console.log(err);
    })
  }
  
  addCategory(values){
    var self = this;
    self._productService.addProductCategory(values.name).then(function(result){
      console.log("CATEGORY ADDED:---------", result)
      self.addCategoryForm.reset()
      // self.addCategoryForm.controls['name'].untouched
      // self.addCategoryForm.controls['name']
      self.addCategoryForm.controls['name'].markAsPristine();
      self.addCategoryForm.controls['name'].markAsUntouched();
      self.addCategoryForm.controls['name'].updateValueAndValidity();
      self.getAllCategories()
    }).catch(function(err) {
      console.log(err)
    })
  }

  showSubCategory(item){
    console.log("Selected Category:===", item)
    this._router.navigate(['/subcategories', item._id])
  }

  goBack(){
    this._router.navigate(['adminsettings'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
    
  logout(){
    this._loginService.logout()
  }  
}
