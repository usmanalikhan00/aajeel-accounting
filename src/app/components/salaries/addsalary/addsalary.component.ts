import { Component, ElementRef, ViewChild, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { SalaryService } from '../../../services/salary.service'
import { EmployeeService } from '../../../services/employees.service'
import { AccountTypesService } from '../../../services/accounttypes.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'add-salary',
    providers: [SalaryService, EmployeeService, AccountTypesService],
    templateUrl: 'addsalary.html',
    styleUrls: ['addsalary.css']
})

export class addSalary {
  
  // public customersDB = new PouchDB('steelcustomers');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  
  @Output() addsalary = new EventEmitter();

  addSalaryForm: FormGroup;
  
  allEmployees: any = []
  employeeStates: Observable<any[]>;
  employeeForm: FormGroup;
  selectedEmployee: any = null
  

  allAccounts: any = []
  accountStates: Observable<any[]>;
  accountForm: FormGroup;
  selectedAccount: any = null

  constructor(private _salaryService: SalaryService,
              private _employeeService: EmployeeService,
              private _accountTypesService: AccountTypesService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddSalaryForm();
    this._buildAccountForm();
    this._buildEmployeeForm();
  }
  
  private _buildEmployeeForm(){
    this.employeeForm = this._formBuilder.group({
      employeeCtrl: ['']
    })
  }
  
  private _buildAccountForm(){

    this.accountForm = this._formBuilder.group({
      accountCtrl: ['']
    })

  }

  private _buildAddSalaryForm(){
    this.addSalaryForm = this._formBuilder.group({
      employee: ['', Validators.required],
      account: ['', Validators.required],
      amount: ['', Validators.required],
      date: ['',  Validators.required],
      notes: ['', Validators.required],
    })
  }

  ngOnInit(){
    var self = this;
    self.getAllAccounts()
    self.getAllEmployees()
  }

  addSalary(values){
    var self = this;
    values._id = new Date().toISOString()
    console.log("ADD SALARY CALLED:--", values);
    // self._salaryService.addSalary(values).then(function(result){
    //   console.log("SALARY ADDED:===", result);
    //   self.addSalaryForm.reset();
    //   self.addsalary.emit({'success': 'done'})
    // }).catch(function(err){
    //   console.log(err);
    // })
  }

  getAllAccounts(){
    var self = this
    self.allAccounts = []
    self._accountTypesService.getAccountTypes().then(function(result){
      result.rows.map(function(row){
        if (row.doc.accounttype == 'bank' || row.doc.accounttype == 'cash')
          self.allAccounts.push(row.doc)
      })
      console.log("ALL ACCOUNTS:=====", self.allAccounts);
      self.accountStates = self.addSalaryForm.controls['account'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterAccounts(state) : self.allAccounts.slice());
    }).catch(function(err){
      console.log(err);
    })
  }


  selectAccount(state){
    this.selectedAccount = state
    console.log("ACCOUNT SELECTED IS:---", this.selectedAccount)
  }



  getAllEmployees(){
    var self = this;
    self._employeeService.allEmployees().then(function(result){
      self.allEmployees = []
      result.rows.map(function(row){
        self.allEmployees.push(row.doc)
      })
      console.log("ALL RESULTS FROM EMPLOYEES:=====", self.allEmployees);
      self.employeeStates = self.addSalaryForm.controls['employee'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterEmployees(state) : self.allEmployees.slice());

    }).catch(function(err){
      console.log(err);
    })
  }

  selectEmployee(state){
    this.selectedEmployee = state
    console.log("Employee SELECTED IS:---", this.selectedEmployee)
    // this.employeeForm.reset()
  }



  goBack(){
    this._router.navigate(['/admindashboard'])
  }

  filterAccounts(name: string) {
    // console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.allAccounts.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterEmployees(name: string) {
    // console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.allEmployees.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }



  // @HostListener('window:resize', ['$event'])
  //   onResize(event) {
  //       if (event.target.innerWidth < 886) {
  //           this.navMode = 'over';
  //           this.sidenav.close();
  //       }
  //       if (event.target.innerWidth > 886) {
  //          this.navMode = 'side';
  //          this.sidenav.open();
  //       }
  //   }
 
}