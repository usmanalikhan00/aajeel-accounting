import { Component, ElementRef, ViewChild, HostListener, Input, Output, EventEmitter } from '@angular/core';
import {EmployeeService} from '../../services/employees.service'
import {SalaryService} from '../../services/salary.service'
import {AccountTypesService} from '../../services/accounttypes.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
// import { MatSidenav } from "@angular/material";

export interface UserData {
  employee ? : object;
  account ? : object;
  amount ? : number;
  date ? : string;
  notes ? : string;
  _id ? : string;
  _rev ? : string;

}


@Component({
    selector: 'salaries',
    providers: [EmployeeService, AccountTypesService, SalaryService],
    templateUrl: 'salaries.html',
    styleUrls: ['salaries.css']
})

export class Salaries {

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  authUser: any = null

  allSalaries: any = []
  // allAccounts: any = []

  displayedColumns = [
    'employee', 
    'account', 
    'amount', 
    'date', 
    "notes",
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _employeeService: EmployeeService,
              private _accountTypesService: AccountTypesService,
              private _salaryService: SalaryService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this.authUser = JSON.parse(localStorage.getItem('user'))
    // this.syncDb()
  }

  ngOnInit(){
    var self = this;
    this.loadSourceData()

  }

  loadSourceData(){
    var self = this;
    self._salaryService.allSalaries().then(function(result){
      console.log("RESULTS FROM ALL SALARIES:=====", result);
      const users: UserData[] = [];
      self.allSalaries = []
      result.rows.map(function(row){
        self.allSalaries.push(row.doc)
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
    }).catch(function(err){
      console.log(err);
    })
  }



  refreshData($event){
    this.loadSourceData()
  }

}

function createNewUser(row): UserData {
  return {
    "_id": row._id,
    "_rev": row._rev,
    "employee" : row.employee,
    "account" : row.account,
    "amount" : row.amount,
    "date" : row.date,
    "notes" : row.notes
  };
}