import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { InwardPassService } from '../../services/inwardpass.service'
import { OutwardPassService } from '../../services/outwardpass.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  outwardpassnumber ? : string;
  vendor ? : string;
  productcount ? : string;
  converted ? : string;
  createdat ? : string;
  createdby ? : string;
  outwardpassnotes ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'outwardpass',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                OutwardPassService, 
                InwardPassService],
    templateUrl: 'outwardpass.html',
    styleUrls: ['outwardpass.css']
})

export class outwardpass {
  
  public outwardPassDB = new PouchDB('aajeelaccoutwardpass');
  public localOutwardPassDB = new PouchDB('http://localhost:5984/aajeelaccoutwardpass');
  public cloudantOutwardPassDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccoutwardpass', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  }); 

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  
  allOutwardpass: any= [];
  authUser: any= null;

  displayedColumns = [
    'outwardpassnumber',
    'vendor',
    'productcount',
    'converted',
    'createdat'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _outwardPassService: OutwardPassService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this.loadSourceData()
    this.authUser = JSON.parse(localStorage.getItem('user'))  
  }

  ngOnInit(){
    var self = this;
    // self.syncDb()
  }

  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.outwardPassDB.replicate.from(self.cloudantOutwardPassDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY OUTWARD-PASS REPLICATION:--", info)
      self.outwardPassDB.sync(self.cloudantOutwardPassDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY OUTWARD-PASS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.loadSourceData()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC OUTWARD-PASS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY OUTWARD-PASS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }

  loadSourceData(){
    var self = this;
    // self.purchaseDB.replicate.from('http://localhost:5984/steelpurchases', {live: true});
    self.allOutwardpass = [];
    const users: UserData[] = [];
    self._outwardPassService.allOutwardpass().then(function(result){
      result.rows.map(function (row) { 
        self.allOutwardpass.push(row.doc); 
        users.push(createNewUser(row.doc)); 
      });
      // self.purchaseDB.replicate.to('http://localhost:5984/steelpurchases', {live: true});
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      console.log("ALL OUT WARD PASSES:=====", self.allOutwardpass);
    }).catch(function(err){
      console.log(err);
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getSingleOutwardpass(outwardpass){
    console.log("Selected OUT WARD PASS:===", outwardpass)
    this._router.navigate(['/convertoutward', outwardpass])
  }

  showOutwardPass(pass){
    console.log("Selected OUT WARD PASS:===", pass)
    this._router.navigate(['/outwardpass', pass._id])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  // console.log("ROW TO SHOW FOR PAYMENTS", row)
  if (row.inwardpassnotes){
    return {
      "_id": row._id,
      "_rev": row._rev,
      "vendor" : row.outwardpasscustomer.name.toString(),
      "createdat" : row.createdat.toString(),
      "createdby" : row.createdby._id.toString(),
      "outwardpassnotes" : row.outwardpassnotes.toString(),
      "converted" : row.converted,
      "outwardpassnumber" : row.outwardpassnumber.toString(),
      "productcount" : row.outwardpassproducts.length
    };
  }
  if (!row.inwardpassnotes){
    return {
      "_id": row._id,
      "_rev": row._rev,
      "vendor" : row.outwardpasscustomer.name.toString(),
      "createdat" : row.createdat.toString(),
      "createdby" : row.createdby._id.toString(),
      "outwardpassnotes" : null,
      "outwardpassnumber" : row.outwardpassnumber.toString(),
      "converted" : row.converted,
      "productcount" : row.outwardpassproducts.length
    };
  }

}