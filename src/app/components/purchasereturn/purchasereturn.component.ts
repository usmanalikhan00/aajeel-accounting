import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { CustomersService } from '../../services/customers.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { InvoiceReturnService } from '../../services/invoicereturn.service'
import { PurchaseReturnService } from '../../services/purchasereturn.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
// import { MatSidenav, MatSnackBar } from "@angular/material";
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

export interface UserData {
  purchaseref ? : string;
  datetime ? : string;
  customer ? : string;
  number ? : string;
  itemscount ? : string;
  total ? : number;
  status ? : string;
  _id ? : string;
  _rev ? : string;

}

@Component({
    selector: 'purchase-return',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                ItemDebitCreditService, 
                InvoiceReturnService, 
                CustomerDebitCreditService, 
                CustomersService,
                PurchaseReturnService],
    templateUrl: 'purchasereturn.html',
    styleUrls: ['purchasereturn.css']
})

export class purchasereturn {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // public productsDB = new PouchDB('steelinvoice');
  public invoicesDB = new PouchDB('steelcustomerinvoice');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  invoiceProducts: any = [];
  allPurchases: any= [];
  invoiceStates: Observable<any[]>;
  selectedPurchase: any = null;
  
  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;
  invoiceForm: FormGroup;
  
  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  cartProducts: any[] = []
  cartTotal: any = 0
  
  cartStates: Observable<any[]>;
  invoiceNotes: any;

  customerToUpdate: any = null
  authUser: any = null
  customerDebitCredit: any = null
  addReturnFlag: boolean = false

  allPurchaseReturns: any[] = []
  
  displayedColumns = [
    'number', 
    'customer', 
    'purchaseref', 
    'itemscount', 
    "datetime",
    "total"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public purchaseReturnDB = new PouchDB('aajeelaccpurchasereturn');
  public localPurchaseReturnDB = new PouchDB('http://localhost:5984/aajeelaccpurchasereturn');
  public cloudantPurchaseReturnDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccpurchasereturn', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  
  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService, 
              public _matSnackBar: MatSnackBar,
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _invoiceReturnService: InvoiceReturnService, 
              private _purchaseReturnService: PurchaseReturnService, 
              private _purchaseService: PurchaseService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    this._buildInvoiceForm();
    this.loadSourceData()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }
  private _buildInvoiceForm(){
    this.invoiceForm = this._formBuilder.group({
      invoiceCtrl: ['']
    })
  }




  ngOnInit(){
    this.getCartProducts()
    this.getAllCustomers()
    this.getAllPurchases()
    // this.syncDb()
  }
  
  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.invoicesReceiptsDB, self.ibmInvoicesReceiptsDB, {
    //   live: true,
    //   retry: true
    // })
    var sync = self.purchaseReturnDB.sync(self.cloudantPurchaseReturnDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadSourceData()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("********PAUSE FROM PURCHASE RETURNS*********", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });  
  }
  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  showAddReturnFlag(){
    this.addReturnFlag = true
  }
  
  hdieAddReturnFlag(){
    this.addReturnFlag = false
    this.cartProducts = []
    this.selectedCustomer = null
    this.selectedPurchase = null
    this.loadSourceData()
  }


  showPurchaseReturn(pass){
    this._router.navigate(['singlepurchasereturn', pass._id])
  }

  loadSourceData(){
    var self = this
    self._purchaseReturnService.allPurchaseReturns().then(function(result){
      console.log("RESULT FROM ALL PURCAHSE RETURN:0000---", result)
      // self.allPurchaseReturns = []
      // result.rows.map(function(row){
      //   self.allPurchaseReturns.push(row.doc)
      // })
      // console.log("RESULT AFTER PURCAHSE RETURN---", self.allPurchaseReturns)
      const users: UserData[] = [];
      result.rows.map(function(row){
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      // self.invoiceStates = self.invoiceForm.controls['invoiceCtrl'].valueChanges
      //     .startWith(null)
      //     .map(state => state ? self.filterInvoices(state) : self.allInvoiceReturns.slice());
    }).catch(function(err){
      console.log(err)
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getInvoiceNotes($event){
    this.invoiceNotes = $event
    console.log("INVOICE NOTES ARE:-----", $event ,this.invoiceNotes)
  }


  addInvoice(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    var ID = Math.floor(Math.random()*1000) + 100000
    self.customerDebitCredit = null
    // self.selectedCustomer.balance = self.cartTotal
    var purchaseReturnDoc = {
      "_id": new Date().toISOString(),
      "purchasereturnnumber": ID, 
      "purchasereturntotal": self.cartTotal, 
      "purchasereturnproducts": self.cartProducts, 
      "purchasereturncustomer": self.selectedCustomer, 
      "purchasereturncustomerid": self.selectedCustomer._id, 
      "status": null, 
      "createdby": user, 
      "createdat": moment().format(),
      "purchasereturnnotes":self.invoiceNotes, 
      "purchaseref":self.selectedPurchase, 
      "purchaserefid":self.selectedPurchase._id
    }
    self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + self.cartTotal
    self.customerDebitCredit = {
      "_id": new Date().toISOString(),
      "name": self.customerToUpdate[0].name,
      "address": self.customerToUpdate[0].address,
      "phone": self.customerToUpdate[0].phone,
      "fax": self.customerToUpdate[0].fax,
      "email": self.customerToUpdate[0].email,
      "customerid": self.customerToUpdate[0]._id,
      "customerrev": self.customerToUpdate[0]._rev,
      "openingbalance": self.customerToUpdate[0].openingbalance,
      "closingbalance": self.customerToUpdate[0].closingbalance,
      "dcref": purchaseReturnDoc.purchasereturnnumber,
      "dctype": "purchase return",
      "status": null,
      "debit": null,
      "credit": null
    }
    if (self.customerToUpdate[0].closingbalance > 0){
      self.customerToUpdate[0].status = 'debit'
      self.customerDebitCredit.status = 'debit'
      self.customerDebitCredit.debit = self.cartTotal
      self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
    }else if (self.customerToUpdate[0].closingbalance < 0){
      self.customerToUpdate[0].status = 'credit'
      self.customerDebitCredit.status = 'credit'
      self.customerDebitCredit.debit = self.cartTotal
      self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
    }
    console.log("INVOICE DOC TO PUSH:=====", purchaseReturnDoc);
    console.log("CUSTOMER BALANCE TO UPDATE::=====", self.customerToUpdate[0]);
    console.log("CUSTOMER CREDIT DEBIT DOCUMENT::=====", self.customerDebitCredit);
    self._purchaseReturnService.addPurchaseReturn(purchaseReturnDoc).then(function(result){
      console.log("RESULT AFTER ADDING INVOICE RETURN=======", result)
      // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
      var productDocs: any = []
      var invoiceDocs: any = []
      var itemDebitCredit: any = []
      for (let product of self.invoiceProducts){
        for (let doc of self.cartProducts){
          if (product._id === doc._id){
            var sum = 0
            doc.stores = product.stores
            doc._id = product._id
            doc._rev = product._rev
            for (let obj of doc.stores){
              if (obj._id === doc.cartstores._id){
                obj.stock = obj.stock - doc.returnquantity
              }  
              sum = sum + obj.stock
            }
            doc.totatcartweight = doc.returnquantity * doc.weight
            doc.stockvalue = sum
            // doc.storeStocks = sum
            if (product.type === "Weighted"){
              doc.netweight = doc.stockvalue * doc.weight
              doc.networth = doc.stockvalue * doc.weight * doc.price
            }
            if (product.type === "Single Unit"){
              doc.networth = doc.stockvalue * product.price
            }
            var things = ['a', 'b', 'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r'];
            var thing = things[Math.floor(Math.random()*things.length)];
            var object = {
              "_id":(Math.floor(Math.random()*1000) + 100000).toString()+thing,
              "name":doc.name,
              "productid":doc._id,
              "productrev":doc._rev,
              "debit":doc.returnquantity,
              "credit":null,
              "createdat":result.id,
              "customer":self.selectedCustomer,
              "dcref": purchaseReturnDoc.purchasereturnnumber,
              "dctype": "purchase return",
              "stockvalue":doc.stockvalue
            }
            itemDebitCredit.push(object)             
            invoiceDocs.push(doc)
            productDocs.push(product)
          }
        }
      }
      console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
      console.log("PRODUCT DOCS IN INVOICE RETURN:=====", invoiceDocs);
      console.log("ITEM DEBIT CREDIT ARRAY:=====", itemDebitCredit);
      self._productService.updateInvoiceProducts(invoiceDocs).then(function(result){
        console.log("RESULT AFTER UPDATEING PRODUCTS:--------", result)
        // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
        self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
          console.log("CUATOMER BALANCES AFTER UPDATE:-----", result)
          self._itemDebitCreditService.addDebitCredit(itemDebitCredit).then(function(result){
            console.log("RESULT AFTER ADDING DEIT CREDIT FOR ITEMs:-----", result)
            self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
              console.log("RESULT AFTER UPDATING THE CUSTOMER DEBIT CREDIT:=====", result)
              self._customerDebitCreditService.getDebitCredits(self.customerToUpdate[0]).then(function(result){
                console.log("RESULT FROM ALL CUSTOMER DEBIT CREDITS:--------------", result)
                self.cartProducts = []
                self.selectedCustomer = null
                self.cartTotal = 0
                self.invoiceNotes = null
                self.customerDebitCredit = null
                self.customerToUpdate = null
                self.states = null
              }).catch(function(err){
                console.log(err)
              })
            }).catch(function(err){
              console.log(err)
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })
    // self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
    //   console.log("RESULT AFTER UPDATING THE SALE CUSTOMER:=====", result)
    // }).catch(function(err){
    //   console.log(err)
    // })
  }


  // getAllStores(){
  //   var self = this
  //   self.allStores = []
  //   self._storeSettingsService.allStores().then(function(result){
  //     result.rows.map(function (row) { 
  //       self.allStores.push(row.doc); 
  //     });
  //     self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
  //         .startWith(null)
  //         .map(state => state ? self.filterStores(state) : self.allStores.slice());
  //     console.log("ALL Stores:=====", self.allStores);
  //   }).catch(function(err){

  //   })
  // }


  getAllPurchases(){
    var self = this
    self._purchaseService.allPurchases().then(function(result){
      self.allPurchases = []
      // console.log("RESULT FROM ALL INVOICES:0000---", result)
      result.rows.map(function(row){
        self.allPurchases.push(row.doc)
      })
      console.log("RESULT AFTER MAPING ALL PURCHASES---", self.allPurchases)
      self.invoiceStates = self.invoiceForm.controls['invoiceCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterInvoices(state) : self.allPurchases.slice());
    }).catch(function(err){
      console.log(err)
    })
  }

  selectInvoice(state){
    (<FormGroup>this.invoiceForm).patchValue({'invoiceCtrl':''}, {onlySelf: true})
    // console.log("INVOICE FROM INPUT IS:-------", state)
    this.selectedPurchase = state
    console.log("SELECTED PURCHAE IS:-------", this.selectedPurchase)
    this.refreshProducts()
    this.selectedCustomer = this.selectedPurchase.purchasecustomer
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    console.log("CUSTOMER TO UPDATE:-------", this.customerToUpdate[0])
    // this.calculateTotal(this.cartProducts)
    this.getAllPurchases()
  }

  refreshProducts(){
    console.log("REFRESH PRODUCTS AGAIN")
    this.states = []
    for (let product of this.selectedPurchase.purchaseproducts){
      var object = {
        "_id":product._id,
        "_rev":product._rev,
        "category":product.category,
        "batchnumber":product.batchnumber,
        "name":product.name,
        "price":product.price,
        "weight":product.weight,
        "subcategory":product.subcategory,
        "type":product.type,
        "stockvalue":product.stockvalue,
        "storeStocks":product.storeStocks,
        "stores":product.stores,
        "cartweight":product.cartweight,
        "cartprice":product.cartprice,
        "calcweight":product.calcweight,
        "cartworth":product.cartworth,
        "cartquantity":product.cartquantity,
        "returnweight":product.cartweight,
        "returnprice":product.cartprice,
        "returncalcweight":product.cartweight,
        "returnquantity":0,
        "cartstores":product.cartstores
      }
      this.states.push(object)
    }
    // this.cartProducts = this.selectedInvoice.invoiceproducts
    this.filteredStates = this.addInvoiceForm.controls['stateCtrl'].valueChanges
        .startWith(null)
        .map(state => state ? this.filterStates(state) : this.states.slice());
  }


  getCartProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
      });
      console.log("ALL PRODUCTS:=====", self.invoiceProducts);
    }).catch(function(err){
      console.log(err);
    })
  }

  selectCartProduct(item){
    // console.log("AUTO SELCT CHANGE FUNCTION", item)
    this.selecteProduct(item)
    this._buildAddInvoiceForm()

  }

  selecteProduct(product){
    console.log("CHANGE INPUT VALUE SELECED:_+_", product)
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.returnquantity = product.returnquantity + 1
        if (product.type == 'Weighted'){
          product.returncalcweight = product.returnquantity * product.returnweight
          product.returnworth = product.returnquantity * product.returnweight * product.returnprice
        }else{
          product.returncalcweight = null
          product.returnworth = product.returnquantity * product.returnprice
        }
        this.cartProducts.push(product)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].returnquantity = this.cartProducts[i].returnquantity + 1
            if (this.cartProducts[i].returnquantity > this.cartProducts[i].cartquantity){
              this.cartProducts[i].returnquantity = 1
              this._matSnackBar.open('Sold value Reached', 'Undo', {
                duration: 3000
              });
            }            
            if (this.cartProducts[i].type == 'Weighted'){
              this.cartProducts[i].returncalcweight = this.cartProducts[i].returnquantity * this.cartProducts[i].returnweight
              this.cartProducts[i].returnworth = this.cartProducts[i].returnquantity * this.cartProducts[i].returnweight * this.cartProducts[i].returnprice
            }else{
              this.cartProducts[i].returncalcweight = null
              this.cartProducts[i].returnworth = this.cartProducts[i].returnquantity * this.cartProducts[i].returnprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.returnquantity = product.returnquantity + 1
          if (product.returnquantity > product.cartquantity){
            product.returnquantity = 1
            this._matSnackBar.open('Sold value Reached', 'Undo', {
              duration: 3000
            });
          }
          if (product.type == 'Weighted'){
            product.returncalcweight = product.returnquantity * product.returnweight
            product.returnworth = product.returnquantity * product.returnweight * product.returnprice
          }else{
            product.returncalcweight = null
            product.returnworth = product.returnquantity * product.returnprice
          }
          this.cartProducts.push(product)
          this.calculateTotal(this.cartProducts)
        }
      }
      this.refreshProducts()
      // this._buildAddInvoiceForm()
      // this.selectInvoice(this.selectedInvoice)
      // this.filteredStates = this.selectedInvoice.invoiceproducts
      // this.filteredStates = this.addInvoiceForm.controls['stateCtrl'].valueChanges
      //   .startWith(null)
      //   .map(state => state ? this.filterStates(state) : this.states.slice());
      // this.selectInvoice(this.selectedPurchase)
      console.log("********ALL CART PRODUCTS************:-", this.cartProducts)
    }
    
  }

  changeWeight(weight, product){
    product.returnweight = weight
    product.returncalcweight = product.returnquantity * product.returnweight
    product.returnworth = product.returncalcweight * product.returnprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    if (weight <= 0){
      product.returnweight = product.cartweight
      product.returncalcweight = product.returnquantity * product.returnweight
      product.returnworth = product.returncalcweight * product.returnprice
      this._matSnackBar.open('Weight Cannot be 0', 'Undo', {
        duration: 3000
      });
    }
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.returnquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        product.returncalcweight = product.returnquantity * product.returnweight
        product.returnworth = product.returnquantity * product.returnweight * product.returnprice
      }else{
        product.returncalcweight = null
        product.returnworth = product.returnquantity * product.returnprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      this.calculateTotal(this.cartProducts)
    }
    if (quantity <= 0 || quantity > product.cartquantity){
      product.returnquantity = 1
      if (quantity > product.cartquantity){
        this._matSnackBar.open('Sold value Reached', 'Undo', {
          duration: 3000
        });
      }
      if (quantity <= 0){
        this._matSnackBar.open('Quantity Cannot be 0', 'Undo', {
          duration: 3000
        });
      }

    }
  }

  changePrice(price, product){
    product.returnprice = price
    if (product.type == 'Weighted'){
      product.returncalcweight = product.returnquantity * product.returnweight
      product.returnworth = product.returnquantity * product.returnweight * product.returnprice
    }else{
      product.returncalcweight = null
      product.returnworth = product.returnquantity * product.returnprice
    }
    if (price <= 0){
      product.returnprice = product.cartprice
      // product.returncalcweight = product.returnquantity * product.returnweight
      // product.returnworth = product.returncalcweight * product.returnprice
      if (product.type == 'Weighted'){
        product.returncalcweight = product.returnquantity * product.returnweight
        product.returnworth = product.returnquantity * product.returnweight * product.returnprice
      }else{
        product.returncalcweight = null
        product.returnworth = product.returnquantity * product.returnprice
      }
      this._matSnackBar.open('Price Cannot be 0', 'Undo', {
        duration: 3000
      });
    }
    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    this.calculateTotal(this.cartProducts)
  }



  calculateTotal(carItems){
    var total = 0  
    for (let item of carItems){
      total = total + item.returnworth
    }
    this.cartTotal = total
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  deleteCartItem(i, cartProducts){
    // console.log("DELETE CART ITEM CALLED:-", i, cartProducts)
    cartProducts[i].returnquantity = 0
    cartProducts.splice(i, 1)
    this.cartProducts = cartProducts
    this.calculateTotal(this.cartProducts)

  }

  getProduct(name){
    let arr: any = []
    if (name){
      arr = _.filter(this.states, function(row, index) {
        return row.name.indexOf(name) > -1
      }) 
      console.log("FILTERED ARRAY:__", arr);
      return arr[0]
    }
    return false
  }

  filterStates(name: string) {
    console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers(name: string) {
    console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.customers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    console.log("ALL Stores:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterInvoices(name: string) {
    console.log("ALL INVOICES FILTER:=====", this.invoiceForm.controls['invoiceCtrl'].value);
    return this.allPurchases.filter(state =>
      state.purchasenumber.toString().indexOf(name.toString()) > -1);
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }

}

function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    "_id": row._id,
    "_rev": row._rev,
    "number" : row.purchasereturnnumber,
    "customer" : row.purchasereturncustomer.name,
    "total" : row.purchasereturntotal,
    "itemscount" : row.purchasereturnproducts.length.toString(),
    "status" : row.status,
    "purchaseref" : row.purchaseref.purchasenumber,
    "datetime":row.createdat
  };


}