import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { PurchaseReturnService } from '../../../services/purchasereturn.service'
import { PurchaseService } from '../../../services/purchase.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import {ElectronService} from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'print-purchasereturn',
    providers: [LoginService, 
                ProductService, 
                PurchaseReturnService, 
                ElectronService, 
                PurchaseService],
    templateUrl: 'printpurchasereturn.html',
    styleUrls: ['printpurchasereturn.css']
})

export class printpurchasereturn {
  

  sub: any;
  purchasereturnId: any;
  selectedPurchaseReturn: any;
  invoiceNotes: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _purchaseReturnService: PurchaseReturnService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _purchaseService: PurchaseService, 
              private _electronService: ElectronService, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.purchasereturnId = params['purchasereturnId']; // (+) converts string 'id' to a number
      console.log("PURCHASE RETURN ID:-----", self.purchasereturnId)
      self._purchaseReturnService.getSinglePurchaseReturn(self.purchasereturnId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
        self.selectedPurchaseReturn = result.docs[0]
        console.log("RESULT FROM SINLE PURCHASE RETURN", self.selectedPurchaseReturn)
        self._electronService.ipcRenderer.send('PrintPurchaseReturn')
      }).catch(function(err){
          console.log(err)
      })
    });
  }
  
  // getSingleInvoice(){
  //   var self = this;
  //   self._invoiceService.getSingleInvoice(self.purchaseId).then(function(result){
  //     // console.log("RESULT FROM SINLE CATEGORY", result)
  //     self.selectedPurchase = result.docs[0]
  //     console.log("RESULT FROM SINLE CATEGORY", self.selectedPurchase)
  //   }).catch(function(err){
  //       console.log(err)
  //   })
  // }

  // goBack(){
  //   this._router.navigate(['invoicereturn'])
  // }

  // @HostListener('window:resize', ['$event'])
  //   onResize(event) {
  //       if (event.target.innerWidth < 886) {
  //           this.navMode = 'over';
  //           this.sidenav.close();
  //       }
  //       if (event.target.innerWidth > 886) {
  //          this.navMode = 'side';
  //          this.sidenav.open();
  //       }
  //   }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
