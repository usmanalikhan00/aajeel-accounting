import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import { CustomersService } from '../../services/customers.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { InvoiceReceiptService } from '../../services/invoicereceipt.service'
import { PurchasePaymentService } from '../../services/purchasepayment.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray, FormGroupDirective } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  paymentaccountname ? : string;
  paymentaccounttype ? : string;
  paymentnumber ? : string;
  purchaserefnumber ? : string;
  paymentdate ? : string;
  paymentamount ? : number;
  accountrefname ? : string;
  voucherno ? : string;
  customername ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'purchase-payment',
    providers: [LoginService, 
                CustomersService,
                AccountTypesService,
                InvoiceReceiptService,
                AccountDebitCreditService,
                PurchasePaymentService,
                PurchaseService,
                InvoiceService,
                ElectronService,
                CustomerDebitCreditService],
    templateUrl: 'purchasepayment.html',
    styleUrls: ['purchasepayment.css']
})

export class purchasepayment {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild(FormGroupDirective) myForm;
  navMode = 'side';

  addReceiptForm: FormGroup;
  addCustomerForm: FormGroup;
  addAccountTypeForm: FormGroup;

  customerStates: Observable<any[]>;
  accCustomerStates: Observable<any[]>;
  accountStates: Observable<any[]>;
  invoiceStates: Observable<any[]>;
  customerAccountStates: Observable<any[]>;

  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  customerToUpdate: any = null
  customerDebitCredit: any = null

  accountDebitCredit: any = null
  selectedAccountTypes: any = []
  selectedAccount: any = null
  accountToUpdate: any = null
  
  allPurchases: any = []
  selectedPurchase: any = null

  allTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    }

  ]
  allAccTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
    {
      'name':'Post Dated Cheque',
      'type':'post_dated_cheque'
    },
    {
      'name':'Parchi',
      'type':'parchi'
    },
    {
      'name':'Card',
      'type':'card'
    },
    {
      'name':'Cheque',
      'type':'cheque'
    }
  ]
  receiptTypes = [
    {
      'name':'Purchase',
      'type':'invoice'
    },
    {
      'name':'BPCC',
      'type':'bpcc'
    }
  ];

  paymentToSave = {
    _id:new Date().toISOString(),
    paymentnumebr:Math.floor(Math.random()*1000) + 100000,
    paymentcustomer:null,
    paymentcustomerid:null,
    paymentaccount:null,
    paymentaccountid:null,
    paymentamount:null,
    purchaseref:null,
    purchaserefid:null,
    accountref:null,
    paymenttype:null,
    accountrefid: null,
    paymentdate: null,
    paymentnotes: null,
    voucherno: null
  }

  selectedRefAccount: any = null
  refAccountToUpdate: any = null
  allCustomerAccounts: any = []
  accountTypeChangeValue: any = null
  
  addReceiptFlag: boolean = false
  addCustomerFlag: boolean = false
  addAccountFlag: boolean = false

  allPayments: any = []

  displayedColumns = [
    "datetime",
    "voucherno",
    'paymentnumber', 
    'customername', 
    'paymentaccounttype', 
    'paymentaccountname', 
    "paymentamount",
    "purchaserefnumber",
    "accountrefname",
    "action"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  public purchasePaymentsDB = new PouchDB('aajeelaccpurchasepayments');
  public localPurchasePaymentsDB = new PouchDB('http://localhost:5984/aajeelaccpurchasepayments');
  public cloudantPurchasePaymentsDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccpurchasepayments', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  balanceTypes = ['Credit', 'Debit']
  authUser: any = null  
  paymentNotes: any = null  

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _customersService: CustomersService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _accountTypesService: AccountTypesService, 
              private _invoiceReceiptService: InvoiceReceiptService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _purchasePaymentService: PurchasePaymentService, 
              private _electronService: ElectronService, 
              private _purchaseService: PurchaseService, 
              private _invoiceService: InvoiceService, 
              private _router: Router) {
    var self = this
    self._buildAddReceiptForm();
    self._buildAddAccountTypeForm()
    self._buildAddCustomerForm()
    self.loadDateSource()
    self.authUser = JSON.parse(localStorage.getItem('user')) 

  }

  private _buildAddAccountTypeForm(){
    this.addAccountTypeForm = this._formBuilder.group({
      name: ['', Validators.required],
      accounttype: ['', Validators.required],
      accountnumber: ['', Validators.required],
      expirydate: ['', Validators.required],
      obtype: ['', Validators.required],
      customer: ['', Validators.required],
      refnumber: ['', Validators.required],
      openingbalance: ['', Validators.required],
      voucherno: [null]
    })
  }

  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }

  private _buildAddReceiptForm(){
    this.addReceiptForm = this._formBuilder.group({
      customer: ['', Validators.required],
      cashtype: ['', Validators.required],
      account: ['', Validators.required],
      invoiceref: [''],
      receipttype: [''],
      customeraccref:[''],
      paymentdate:[''],
      voucherno: [null]
    })
  }

  ngOnInit(){
    this.getAllCustomers()
    // this.getAllPayments()
    // this.getAllInvoices()
    // this.syncDb()
  }

  syncDb(){
    var self = this
    var sync = PouchDB.sync(self.purchasePaymentsDB, self.cloudantPurchasePaymentsDB, {
      live: true,
      retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull'){
        self.loadDateSource()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("PAUSE EVENT FROM PURCHASE PAYMENTS", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });
  }

  loadDateSource(){
    var self = this
    const users: UserData[] = [];
    self._purchasePaymentService.allPurchasePayments().then(function(result){
      console.log("RESULT AFTER ALL INVOICE RECEIPTS:---", result)
      // self.allPayments = []
      result.rows.map(function (row) { 
        users.push(createNewUser(row.doc)); 
      });
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      // console.log("ALL PAYEMNTS FROM ONITNI:---", self.allPayments)
    }).catch(function(err){
      console.log(err)
    })

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getAllPayments(){
    var self = this
    self._purchasePaymentService.allPurchasePayments().then(function(result){
      console.log("RESULT AFTER ALL INVOICE RECEIPTS:---", result)
      self.allPayments = []
      result.rows.map(function(row){
        self.allPayments.push(row.doc)
      })
      console.log("ALL PAYEMNTS FROM ONITNI:---", self.allPayments)
    }).catch(function(err){
      console.log(err)
    })
  }

  goToPrint(row){
    console.log("GO TO PRINT RESULT:000", row)
    // this._router.navigate(['printpayment', row._id])
    // this._electr
    this._electronService.ipcRenderer.send('PaymentPrint', row._id)
  }

  showAddReceipt(){
    this.addReceiptFlag = true
  }
  
  hideAddReceipt(){
    this.addReceiptFlag = false
    this.loadDateSource()
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      self.customerStates = self.addReceiptForm.controls['customer'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.allCustomers.slice());
      self.accCustomerStates = self.addAccountTypeForm.controls['customer'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers2(state) : self.allCustomers.slice());
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }


  addCustomer(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }
    console.log("ADD CUSTOMER CALLED:--", values);
    self._customersService.addCustomer(values).then(function(result){
      console.log("Customer ADDED:===", result);
      self.addCustomerForm.reset();
      self.getAllCustomers()
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }


  enableAddCustomerFlag(){
    this.addCustomerFlag = true
    this.addAccountFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addAccountTypeForm.reset()
    this.addCustomerForm.reset()
    this.paymentToSave = {
      _id:new Date().toISOString(),
      paymentnumebr:Math.floor(Math.random()*1000) + 100000,
      paymentcustomer:null,
      paymentcustomerid:null,
      paymentaccount:null,
      paymentaccountid:null,
      paymentamount:null,
      purchaseref:null,
      purchaserefid:null,
      accountref:null,
      paymenttype:null,
      accountrefid: null,
      paymentdate: null,
      paymentnotes: null,
      voucherno: null
    }
  }

  enableAddAccountFlag(){
    this.addAccountFlag = true
    this.addCustomerFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addCustomerForm.reset()
    this.addAccountTypeForm.reset()
    this.paymentToSave = {
      _id:new Date().toISOString(),
      paymentnumebr:Math.floor(Math.random()*1000) + 100000,
      paymentcustomer:null,
      paymentcustomerid:null,
      paymentaccount:null,
      paymentaccountid:null,
      paymentamount:null,
      purchaseref:null,
      purchaserefid:null,
      accountref:null,
      paymenttype:null,
      accountrefid: null,
      paymentdate: null,
      paymentnotes: null,
      voucherno: null
    }
    
  }

  showReceiptForm(){
    this.addCustomerFlag = false
    this.addAccountFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addCustomerForm.reset()
    this.addAccountTypeForm.reset()
    this.paymentToSave = {
      _id:new Date().toISOString(),
      paymentnumebr:Math.floor(Math.random()*1000) + 100000,
      paymentcustomer:null,
      paymentcustomerid:null,
      paymentaccount:null,
      paymentaccountid:null,
      paymentamount:null,
      purchaseref:null,
      purchaserefid:null,
      accountref:null,
      paymenttype:null,
      paymentdate:null,
      paymentnotes:null,
      accountrefid: null,
      voucherno: null
    }
  }

  selectAccCustomer(state){
    (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedCustomer = state
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate[0])
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this.getAllCustomers()
  }


  addAccountType(values, $event){
    var self = this;
    values._id = new Date().toISOString()
    // values.closingbalance = values.openingbalance
    if (values.accounttype === 'bank' || values.accounttype === 'cash'){
      if (values.obtype === 'Debit'){
        if (values.openingbalance != 0){
          values.closingbalance = values.openingbalance
          values.status = 'debit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }else if (values.obtype === 'Credit'){
        if (values.openingbalance != 0){
          values.openingbalance = -values.openingbalance
          values.closingbalance = values.openingbalance
          values.status = 'credit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }
      values.level = 'parent'
      values.parentid = null

      self._accountTypesService.addAccountType(values).then(function(result){
        console.log("result after adding account:---", result)
        // self.getAccountTypes()
        // self.loadSourceData()
        self._buildAddAccountTypeForm()
      }).catch(function(err){
        console.log(err)
      })
    }else{
      values.expirystatus = 'pending'
      values.customer = this.selectedCustomer
      values.customerid = values.customer._id
      values.remainbalance = values.openingbalance
      values.level = 'parent'
      values.parentid = null

      if (values.obtype === 'Debit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - values.openingbalance
          
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "voucherno": values.voucherno,
          "dctype": values.accounttype,
          "dcrefid": values._id,
          "status": null,
          "debit": null,
          "credit": null
        }
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }       
      }else if (values.obtype === 'Credit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + values.openingbalance
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "voucherno": values.voucherno,
          "dctype": values.accounttype,
          "dcrefid": values._id,
          "status": null,
          "debit": null,
          "credit": null
        }      
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        } 
      }
      // values.expirydate = values.expirydate
      // (values.expirydate)
      values.expirydate = values.expirydate.toISOString()

      console.log("VALUES FROM ADD ACCOUNT TYPE FORM:--", values)
      console.log("CUSTOMER DEBIT CREDIT OBJECT:========", self.customerDebitCredit)
      console.log("ACTUAL CUSTOMER TO UPDATE:========", self.customerToUpdate[0])
      self._accountTypesService.addAccountType(values).then(function(result){
        console.log("result after adding account:---", result)
        self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
          console.log("result after adding customer debit credit object:---", result)
          self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
            console.log("result after updating customer balance:---", result)
            // self.getAccountTypes()
            self._buildAddAccountTypeForm()
            // self.myForm.resetForm()
            // self.loadSourceData()
            self.getAllCustomers()
            self.selectedCustomer = null
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }
  }

  typeChange(value){
    (<FormGroup>this.addAccountTypeForm).patchValue({'obtype':''}, { onlySelf: true });
    (<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''}, { onlySelf: true });
    this.selectedCustomer = null
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    if (value === 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });

    }
    if (value !== 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':''}, { onlySelf: true });
    }
    if (value === 'cash'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });
    }

  }  

  getAllPurchases(customer){
    var self = this;
    self.allPurchases = [];
    self._purchaseService.customerPurchases(customer).then(function(result){
      console.log("RESULT AFTER CUSTOMER PURCHASES:=====", result);
      result.docs.forEach(function (row) { 
        self.allPurchases.push(row); 
      });
      self.invoiceStates = self.addReceiptForm.controls['invoiceref'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterInvoices(state) : self.allPurchases.slice());
      console.log("ALL PURCHASES:=====", self.allPurchases);
    }).catch(function(err){
      console.log(err);
    })
  }


  getAllCustomerAccounts(customerid){
    var self = this;
    self.allCustomerAccounts = [];
    self._accountTypesService.getCustomerCreditAccounts(customerid).then(function(result){
      console.log("ALL CUSTOMER ACCOUNTS:=====", result);
      result.docs.forEach(function (row) { 
        self.allCustomerAccounts.push(row); 
      });
      self.customerAccountStates = self.addReceiptForm.controls['customeraccref'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterCustomerAccount(state) : self.allCustomerAccounts.slice());
      console.log("ALL CUSTOMER ACCOUNTS:=====", self.allCustomerAccounts);
    }).catch(function(err){
      console.log(err);
    })
  }

  getReceiptType($event){
    console.log("RECEIPT TYPE IS:-----", $event)
    if ($event.value === 'bpcc'){
      (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':'none', 'customeraccref':''}, {onlySelf: true})
      // (<FormGroup>this.addReceiptForm).patchValue({'customeraccref':''}, {onlySelf: true})
      this.selectedPurchase = null
      this.paymentToSave.purchaseref = null
      this.paymentToSave.purchaserefid = null
    }else{
      (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':'', 'customeraccref':'none'}, {onlySelf: true})
      // (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, {onlySelf: true})
      this.selectedRefAccount = null
      this.paymentToSave.accountref = null
      this.paymentToSave.accountrefid = null

    }
    console.log("RECEIPT TOS SAVE AFTER CHANGING RECEIPT TYPE:======", this.paymentToSave)
  }

  selectCustomer(state){
    (<FormGroup>this.addReceiptForm).patchValue({'customer':''}, { onlySelf: true });
    (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, { onlySelf: true });
    this.selectedRefAccount = null
    this.paymentToSave.accountrefid = null
    this.paymentToSave.accountref = null
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedCustomer = state
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate)
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this.paymentToSave.paymentcustomer = state
    this.paymentToSave.paymentcustomerid = state._id
    console.log("RECEIPT TO SAVE IS: ------", this.paymentToSave)
    this.getAllCustomers()
    this.getAllPurchases(this.selectedCustomer)
    this.getAllCustomerAccounts(this.selectedCustomer._id)
  }

  // selectAccount(state){
  //   // console.log("STATE FREOM ACOUNT HTML:=====", state)
  //   this.selectedAccount = state
  //   this.accountToUpdate = _.filter(this.selectedAccountTypes, {"_id":this.selectedAccount._id})
  //   // console.log("ACCOUNT TO UPDATE IS: ------", this.accountToUpdate[0])
  //   // console.log("SELECTED ACCOUNT IS: ------", this.selectedAccount)
  //   this.paymentToSave.paymentaccount = this.selectedAccount
  //   this.paymentToSave.paymentaccountid = this.selectedAccount._id
  //   console.log("RECEIPT TO SAVE IS: ------", this.paymentToSave)
  // }
  selectAccount(state){
    (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    // console.log("STATE FREOM ACOUNT HTML:=====", state)
    this.selectedAccount = state
    this.accountToUpdate = _.filter(this.selectedAccountTypes, {"_id":this.selectedAccount._id})
    // console.log("ACCOUNT TO UPDATE IS: ------", this.accountToUpdate[0])
    // console.log("SELECTED ACCOUNT IS: ------", this.selectedAccount)
    this.paymentToSave.paymentaccount = this.selectedAccount
    this.paymentToSave.paymentaccountid = this.selectedAccount._id
    console.log("RECEIPT TO SAVE IS: ------", this.paymentToSave)
    this.accountTypeRefresh(this.accountTypeChangeValue)
    // this.accountStates = this.addReceiptForm.controls['account'].valueChanges
    //     .startWith(null)
    //     .map(state => state ? this.filterAccounts(state) : this.selectedAccountTypes.slice());
    // this.accou
  }

  selectInvoice(state){
    (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, { onlySelf: true });
    // console.log("INVOICE STATE FREOM HTML:=====", state)
    this.selectedPurchase = state
    this.selectedPurchase = _.filter(this.allPurchases, {"_id":this.selectedPurchase._id})
    // console.log("SELECTED INVOICE IS: ------", this.selectedPurchase)
    this.paymentToSave.purchaseref = this.selectedPurchase[0]
    this.paymentToSave.purchaserefid = this.selectedPurchase[0]._id
    console.log("RECEIPT TO SAVE IS: ------", this.paymentToSave)
    this.getAllPurchases(this.selectedCustomer)
  }
  
  selectReferenceAccount(state){
    (<FormGroup>this.addReceiptForm).patchValue({'customeraccref':''}, { onlySelf: true });
    this.selectedRefAccount = state
    this.refAccountToUpdate = _.filter(this.allCustomerAccounts, {"_id":this.selectedRefAccount._id})
    console.log("SELECTED REFERNCE ACCOUNT IS: ------", this.refAccountToUpdate)
    console.log("CHANGE ACCOUNT IS: ------", this.selectedRefAccount)
    // this.getAllCustomerAccounts(this.selectedCustomer._id)
    this.paymentToSave.accountref = this.refAccountToUpdate[0]
    this.paymentToSave.accountrefid = this.refAccountToUpdate[0]._id
    console.log("RECEIPT TO SAVE IS: ------", this.paymentToSave)
    // this.customerAccountStates = this.addReceiptForm.controls['customeraccref'].valueChanges
    //     .startWith(null)
    //     .map(state => state ? this.filterCustomerAccount(state) : this.allCustomerAccounts.slice());
    this.getAllCustomerAccounts(this.selectedCustomer._id)
  }


  accountTypeChange(value){
    (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    this.selectedAccount = null
    this.paymentToSave.paymentaccount = null
    this.paymentToSave.paymentaccountid = null
    var self = this
    // (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    self.accountTypeChangeValue = value
    self.selectedAccountTypes = []
    self._accountTypesService.getAccountSubAccounts(value).then(function(result){
      console.log("RESULT FROM SINGLE ACCOUNT ENTRIES:=======", result)
      result.docs.forEach(function(row){
        self.selectedAccountTypes.push(row)
      })
      self.accountStates = self.addReceiptForm.controls['account'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterAccounts(state) : self.selectedAccountTypes.slice());
      console.log("ALL ACCOUNTS OF SELECTED TYPE:=======", self.selectedAccountTypes)
    }).catch(function(err){
      console.log(err)
    })
  }

  paymentDateChange(e){
    console.log("RECIPT DATE CHANGES:---", e.value)
    this.paymentToSave.paymentdate = e.value.toISOString()
  }

  
  changePrice($event){
    console.log("ENETERED RECEIPT PRICE IS:---------", $event.target.value)
    if (this.selectedRefAccount){
      if ($event.target.value > this.selectedRefAccount.remainbalance){
        console.log("--:YES GREATER VALUE ENTERED:--")
        this.paymentToSave.paymentamount = null
      }
    }
  }


  accountTypeRefresh(value){
    (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    var self = this
    // self.selectedAccount = null
    // self.paymentToSave.paymentaccount = null
    // self.paymentToSave.paymentaccountid = null
    // (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    self.accountTypeChangeValue = value
    self.selectedAccountTypes = []
    self._accountTypesService.getAccountSubAccounts(value).then(function(result){
      console.log("RESULT FROM SINGLE ACCOUNT ENTRIES:=======", result)
      result.docs.forEach(function(row){
        self.selectedAccountTypes.push(row)
      })
      self.accountStates = self.addReceiptForm.controls['account'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterAccounts(state) : self.selectedAccountTypes.slice());
      console.log("ALL ACCOUNTS OF SELECTED TYPE:=======", self.selectedAccountTypes)
    }).catch(function(err){
      console.log(err)
    })
  }



  makeReceipt(value){
    this.paymentToSave.paymentcustomer = null
    this.paymentToSave.paymentcustomerid = null
    value.customer = this.selectedCustomer
    value.account = this.selectedAccount
    value.invoiceref = this.selectedPurchase
    this.paymentToSave.paymentcustomer = this.selectedCustomer
    this.paymentToSave.paymentcustomerid = this.selectedCustomer._id
    this.paymentToSave.paymentaccount = this.selectedAccount
    this.paymentToSave.paymentaccountid = this.selectedAccount._id
    this.paymentToSave.purchaseref = this.selectedPurchase[0]
    this.paymentToSave.purchaserefid = this.selectedPurchase[0]._id
    console.log("RECEIPT FORM BUTTON CLICKED:======", this.addReceiptForm.value)
    console.log("RECEIPT TOS SAVE IS:======", this.paymentToSave)
  }


  addPurchasePayment(){
    
    var self = this
    self.paymentToSave.paymentnotes = self.paymentNotes
    console.log("RECEIPT DOCUMNET TO SAVE:========", self.paymentToSave)

    self.paymentToSave.paymenttype = self.addReceiptForm.controls['receipttype'].value

    self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + self.paymentToSave.paymentamount
    self.customerDebitCredit = {
      "_id": new Date().toISOString(),
      "name": self.customerToUpdate[0].name,
      "address": self.customerToUpdate[0].address,
      "phone": self.customerToUpdate[0].phone,
      "fax": self.customerToUpdate[0].fax,
      "email": self.customerToUpdate[0].email,
      "customerid": self.customerToUpdate[0]._id,
      "customerrev": self.customerToUpdate[0]._rev,
      "openingbalance": self.customerToUpdate[0].openingbalance,
      "closingbalance": self.customerToUpdate[0].closingbalance,
      "dcref": self.paymentToSave.paymentnumebr,
      "paymentdate": self.paymentToSave.paymentdate,
      "dctype": "payment",
      "dcrefname": self.accountToUpdate[0].name,
      "voucherno":self.addReceiptForm.controls['voucherno'].value,
      "notes": self.paymentNotes,
      "status": null,
      "debit": null,
      "credit": null
    }
    
    if (self.customerToUpdate[0].closingbalance > 0){
      self.customerToUpdate[0].status = 'debit'
      self.customerDebitCredit.status = 'debit'
      self.customerDebitCredit.debit = self.paymentToSave.paymentamount
      // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
    }else if (self.customerToUpdate[0].closingbalance < 0){
      self.customerToUpdate[0].status = 'credit'
      self.customerDebitCredit.status = 'credit'
      self.customerDebitCredit.debit = self.paymentToSave.paymentamount
      // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
    } 
    console.log("CUSTOMER DEBIT CREDIT OBJECT:========", self.customerDebitCredit)
    self.accountToUpdate[0].closingbalance = self.accountToUpdate[0].closingbalance - self.paymentToSave.paymentamount
    // self.accountDebitCredit = self.accountToUpdate[0]
    self.accountDebitCredit = {
      _id:new Date().toISOString(),
      accountid:self.accountToUpdate[0]._id,
      accountnumber:self.accountToUpdate[0].accountnumber,
      accountrev:self.accountToUpdate[0]._rev,
      accounttype:self.accountToUpdate[0].accounttype,
      openingbalance:self.accountToUpdate[0].openingbalance,
      closingbalance:self.accountToUpdate[0].closingbalance,
      customer:self.customerToUpdate[0],
      customerid:self.customerToUpdate[0]._id,
      dcref: self.paymentToSave.paymentnumebr,
      paymentdate: self.paymentToSave.paymentdate,
      dctype: "payment",
      voucherno:self.addReceiptForm.controls['voucherno'].value,
      notes: self.paymentNotes,
      obtype:self.accountToUpdate[0].obtype
    }
    self.accountDebitCredit.credit = null
    self.accountDebitCredit.accountid = self.accountToUpdate[0]._id
    self.accountDebitCredit.accountrev = self.accountToUpdate[0]._rev

    if (self.accountToUpdate[0].closingbalance > 0){
      self.accountToUpdate[0].status = 'debit'
      self.accountDebitCredit.status = 'debit'
      self.accountDebitCredit.credit = -self.paymentToSave.paymentamount
      // self.customerDebitCredit.closingbalance = self.accountToUpdate[0].closingbalance
    }else if (self.accountToUpdate[0].closingbalance < 0){
      self.accountToUpdate[0].status = 'credit'
      self.accountDebitCredit.status = 'credit'
      self.accountDebitCredit.credit = -self.paymentToSave.paymentamount
      // self.customerDebitCredit.closingbalance = self.accountToUpdate[0].closingbalance
    } 
    console.log("ACCOUNT DEBIT CREDIT OBJECT:========", self.accountDebitCredit)
    if (self.refAccountToUpdate){
      self.refAccountToUpdate[0].remainbalance = self.refAccountToUpdate[0].remainbalance - self.paymentToSave.paymentamount
      if (self.refAccountToUpdate[0].remainbalance === 0){
        self.refAccountToUpdate[0].expirystatus = 'process'
        console.log("REFRENCE ACCOUNT STATUS UPDATED... YAHOOOO!!!!! :)", self.refAccountToUpdate[0])
      } 
    }
    self.paymentToSave.voucherno = self.addReceiptForm.controls['voucherno'].value
    // console.log("REFRENCE PARCHI/CARD/CHEQUE ACCOUNT TO UPDATE:========", self.refAccountToUpdate[0])
    self._purchasePaymentService.addPurchasePayment(self.paymentToSave).then(function(result){
      console.log("RESULT AFTER ADDING INVOICE RECEIPT:--------", result)
      self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
        console.log("RESULT AFTER ADDING CUSTOMER DEBIT CREDIT:--------", result)
        self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
          console.log("RESULT AFTER UPDATING CUSTOMER BALANCE:--------", result)
          self._accountDebitCreditService.addDebitCredit(self.accountDebitCredit).then(function(result){
            console.log("RESULT AFTER ADDING ACCOUNT DEBIT CREDIT:--------", result)
            self._accountTypesService.updateAccountBalance(self.accountToUpdate[0]).then(function(result){
              console.log("RESULT AFTER UPDATING ACCOUNT BALANCE:--------", result)
              if (self.refAccountToUpdate){
                self._accountTypesService.updateAccountBalance(self.refAccountToUpdate[0]).then(function(result){
                  console.log("RESULT AFTER UPDATING REFERENCE CARD PARCHI CHEQUE ACCOUNT:--------", result)
                  self.addReceiptForm.reset()
                  self.paymentToSave = {
                    _id:new Date().toISOString(),
                    paymentnumebr:Math.floor(Math.random()*1000) + 100000,
                    paymentcustomer:null,
                    paymentcustomerid:null,
                    paymentaccount:null,
                    paymentaccountid:null,
                    paymentamount:null,
                    purchaseref:null,
                    purchaserefid:null,
                    accountref:null,
                    paymenttype:null,
                    accountrefid: null,
                    paymentdate: null,
                    paymentnotes: null,
                    voucherno: null
                  }
                  self.selectedCustomer = null
                  self.customerToUpdate = null
                  self.selectedAccount = null
                  self.accountToUpdate = null
                  self.selectedPurchase = null
                  self.selectedRefAccount = null
                  self.refAccountToUpdate = null
                  self.customerDebitCredit = null
                  self.accountDebitCredit = null
                  self.paymentNotes = null
                  self.loadDateSource()
                  self.getAllCustomers()
                }).catch(function(err){
                  console.log(err)
                })
              }else{
                console.log("RESULT AFTER UPDATING REFERENCE CARD PARCHI CHEQUE ACCOUNT:--------", result)
                self.addReceiptForm.reset()
                self.paymentToSave = {
                  _id:new Date().toISOString(),
                  paymentnumebr:Math.floor(Math.random()*1000) + 100000,
                  paymentcustomer:null,
                  paymentcustomerid:null,
                  paymentaccount:null,
                  paymentaccountid:null,
                  paymentamount:null,
                  purchaseref:null,
                  purchaserefid:null,
                  accountref:null,
                  paymentdate:null,
                  paymentnotes:null,
                  paymenttype:null,
                  accountrefid: null,
                  voucherno: null
                }
                self.selectedCustomer = null
                self.customerToUpdate = null
                self.selectedAccount = null
                self.accountToUpdate = null
                self.selectedPurchase = null
                self.selectedRefAccount = null
                self.refAccountToUpdate = null
                self.customerDebitCredit = null
                self.accountDebitCredit = null
                self.paymentNotes = null
                self.loadDateSource()
                self.getAllCustomers()
              }
            }).catch(function(err){
              console.log(err)
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).then(function(err){
      console.log(err)
    })

  }


  filterCustomers(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addReceiptForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers2(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addAccountTypeForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }
  
  filterAccounts(name: string) {
    console.log("ALL ACCOUNTS FROM FILTER:=====", this.addReceiptForm.controls['account'].value);
    return this.selectedAccountTypes.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterInvoices(name: string) {
    console.log("ALL INVOICES FROM FILTER:=====", this.addReceiptForm.controls['invoiceref'].value);
    return this.allPurchases.filter(state =>
      state.purchasenumber.toString().toLowerCase().indexOf(name.toString().toLowerCase()) > -1);
  }

  filterCustomerAccount(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addReceiptForm.controls['customeraccref'].value);
    return this.allCustomerAccounts.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }
}

function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  console.log("ROW TO SHOW FOR PAYMENTS", row)
  if (row.purchaseref === null){
    if (row.accountref !== null){
      if (row.voucherno){
        return {
            "paymentnumber" : row.paymentnumebr.toString(),
            "customername" : row.paymentcustomer.name.toString(),
            "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
            "paymentaccountname" : row.paymentaccount.name.toString(),
            "paymentamount" : row.paymentamount,
            "purchaserefnumber" : null,
            "voucherno" : row.voucherno,
            "accountrefname" : row.accountref.name.toString(),
            "paymentdate" : row.paymentdate,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }else{
        return {
            "paymentnumber" : row.paymentnumebr.toString(),
            "customername" : row.paymentcustomer.name.toString(),
            "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
            "paymentaccountname" : row.paymentaccount.name.toString(),
            "paymentamount" : row.paymentamount,
            "purchaserefnumber" : null,
            "voucherno" : null,
            "accountrefname" : row.accountref.name.toString(),
            "paymentdate" : row.paymentdate,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }
    }else{
      if (row.voucherno){
        return {
            "paymentnumber" : row.paymentnumebr.toString(),
            "customername" : row.paymentcustomer.name.toString(),
            "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
            "paymentaccountname" : row.paymentaccount.name.toString(),
            "paymentamount" : row.paymentamount,
            "purchaserefnumber" : null,
            "voucherno" : row.voucherno,
            "paymentdate" : row.paymentdate,
            "accountrefname" : null,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }else{
        return {
            "paymentnumber" : row.paymentnumebr.toString(),
            "customername" : row.paymentcustomer.name.toString(),
            "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
            "paymentaccountname" : row.paymentaccount.name.toString(),
            "paymentamount" : row.paymentamount,
            "paymentdate" : row.paymentdate,
            "purchaserefnumber" : null,
            "voucherno" : null,
            "accountrefname" : null,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }
    }
  }
  if (row.accountref === null){
    if (row.voucherno){
      return {
          "paymentnumber" : row.paymentnumebr.toString(),
          "customername" : row.paymentcustomer.name.toString(),
          "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
          "paymentaccountname" : row.paymentaccount.name.toString(),
          "paymentdate" : row.paymentdate,
          "paymentamount" : row.paymentamount,
          "purchaserefnumber" : row.purchaseref.purchasenumber.toString(),
          "accountrefname" : null,
          "voucherno" : row.voucherno,
          "_id" : row._id,
          "_rev" : row._rev
      };
    }else{
      return {
          "paymentnumber" : row.paymentnumebr.toString(),
          "customername" : row.paymentcustomer.name.toString(),
          "paymentaccounttype" : row.paymentaccount.accounttype.toString(),
          "paymentaccountname" : row.paymentaccount.name.toString(),
          "paymentdate" : row.paymentdate,
          "paymentamount" : row.paymentamount,
          "purchaserefnumber" : row.purchaseref.purchasenumber.toString(),
          "accountrefname" : null,
          "voucherno" : null,
          "_id" : row._id,
          "_rev" : row._rev
      };
    }
  }  

}