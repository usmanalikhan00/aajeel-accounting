import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { StoreSettingsService } from '../../../services/storesettings.service'
import { CustomersService } from '../../../services/customers.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ItemDebitCreditService } from '../../../services/itemdebitcredit.service'
import { CustomerDebitCreditService } from '../../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { MatSidenav, MatSnackBar } from "@angular/material";
import { UUID } from 'angular2-uuid';

@Component({
    selector: 'add-purchase',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                ItemDebitCreditService, 
                CustomerDebitCreditService, 
                PurchaseService, 
                CustomersService],
    templateUrl: 'addpurchase.html',
    styleUrls: ['addpurchase.css']
})

export class addpurchase {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // public productsDB = new PouchDB('steelinvoice');
  public purchaseDB = new PouchDB('steelcustomerpurchase');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  invoiceProducts: any = [];
  allInvoices: any= [];
  
  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;
  addProductForm: FormGroup;
  
  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  cartProducts: any[] = []
  cartTotal: any = 0
  cartWeight: any = 0
  
  cartStates: Observable<any[]>;
  purchaseNotes: any = null;

  newProductFlag: boolean = false
  newProductStores: any = []

  productTypes = [
    'Weighted',
    'Single Unit'
  ];
  // formstores =  new FormControl()

  allCategories: any = [];
  allSubCategories: any = [];
  selectedCategory: any = null;
  selectedStores: any = [];
  stockFlag: boolean = false;

  customerToUpdate: any = null
  customerDebitCredit: any = null

  autoFiller: any = 1
  challanno: any = null
  billno: any = null
  othercharges: any = null
  purchasedate: any = null
  authUser: any = null
  purchaseId: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _matSnackBar: MatSnackBar, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    this._buildAddProductForm();
    // this.getAllStores()
    this.authUser = JSON.parse(localStorage.getItem('user'))

  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }

  private _buildAddProductForm(){
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      formstores:['', Validators.required]
    })
  }



  ngOnInit(){
    // this.getAllStores()
    // this.purchaseId = Math.floor(Math.random()*1000) + 100000
    this.purchaseId = UUID.UUID().substr(30, 35)
    this.getAllCustomers()
    this.getAllCategories()
    // this.allSubCategories()
    this.getCartProducts()
  }
  
  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      // console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }


  selectSubCategory($event){
    var self = this;
    // console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }


  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });

      self.customerStates = self.addCustomerForm.controls['customerCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.allCustomers.slice());
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getPurchaseNotes($event){
    this.purchaseNotes = $event
    // console.log("INVOICE NOTES ARE:-----", $event ,this.purchaseNotes)
  }
  getCartProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self.states = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
          var object = {
            "name":row.doc.name,
            "weight":row.doc.weight,
            "batchnumber":row.doc.batchnumber,
            "stockvalue":row.doc.stockvalue,
            "storeStocks":row.doc.storeStocks,
            "_id":row.doc._id,
            "_rev":row.doc._rev,
            "category":row.doc.category,
            "categoryname":row.doc.categoryname,
            "subcategory":row.doc.subcategory,
            "price":row.doc.price,
            "cartweight":row.doc.weight,
            "calcweight":0,
            "type":row.doc.type,
            "stores":row.doc.stores,
            "newstores":null,
            "cartstores":"",
            "cartquantity":0,
            "cartprice":0
          }          
          self.states.push(object)
      });
      self.allStores = []
      self.newProductStores = []
      self._storeSettingsService.allStores().then(function(result){
        result.rows.map(function (row) { 
          self.allStores.push(row.doc); 
          self.newProductStores.push(row.doc)
        });
        // console.log("ALL PRODUCTS:=====", self.invoiceProducts);
        // for (let item of self.invoiceProducts){
        //     // var productStores: any[] = self.newProductStores.push(item.stores)
        //     // var productStores: any[] = item.stores;
        //     // for (let store of self.newProductStores){
        //     //   productStores.push(store)
        //     // }
        //     // productStores = self.arrUnique(productStores)
        //     // console.log("PRODUCT STORES:-------", productStores)
        //     var object = {
        //       "name":item.name,
        //       "weight":item.weight,
        //       "batchnumber":item.batchnumber,
        //       "stockvalue":item.stockvalue,
        //       "storeStocks":item.storeStocks,
        //       "_id":item._id,
        //       "_rev":item._rev,
        //       "category":item.category,
        //       "categoryname":item.categoryname,
        //       "subcategory":item.subcategory,
        //       "price":item.price,
        //       "cartweight":item.weight,
        //       "calcweight":0,
        //       "type":item.type,
        //       "stores":item.stores,
        //       "newstores":null,
        //       "cartstores":"",
        //       "cartquantity":0,
        //       "cartprice":0
        //     }          

        //   self.states.push(object)
        // }
        var newProduct = {
          "type":"new_product",
          "name":"New Product",
          "price":"",
          "stockvalue":""

        }
        self.states.push(newProduct)
        self.filteredStates = self.addInvoiceForm.controls['stateCtrl'].valueChanges
            .startWith(null)
            .map(state => state ? self.filterStates(state) : self.states.slice());
        self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
            .startWith(null)
            .map(state => state ? self.filterStores(state) : self.allStores.slice());
        // console.log("ALL Stores:=====", self.allStores);
        // console.log("ALL NEW PRODUCT Stores:=====", self.newProductStores);
      }).catch(function(err){
        console.log(err);
      })
    }).catch(function(err){
      console.log(err);
    })
  }

  arrUnique(arr) {
    var cleaned = [];
    arr.forEach(function(itm) {
        var unique = true;
        cleaned.forEach(function(itm2) {
            if (_.isEqual(itm._id, itm2._id)){
              unique = false;
              itm.stock = 0
            } 
        });
        if (unique)  cleaned.push(itm);
    });
    return cleaned;
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self.newProductStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
        self.newProductStores.push(row.doc)
      });
      self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStores(state) : self.allStores.slice());
      // console.log("ALL Stores:=====", self.allStores);
      // console.log("ALL NEW PRODUCT Stores:=====", self.newProductStores);
    }).catch(function(err){

    })
  }
  

  addPurchase(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    var ID = Math.floor(Math.random()*1000) + 100000
    // self.selectedCustomer.balance = self.cartTotal
    // self.customerToUpdate[0].balance = self.customerToUpdate[0].balance + self.cartTotal
    self.customerDebitCredit = null
    var purchaseDoc: any = null;
    if (self.purchasedate){
      purchaseDoc = {
        "_id": new Date().toISOString(),
        "purchasenumber": Math.floor(Math.random()*1000) + 100000, 
        "purchasetotal": self.cartTotal + self.othercharges, 
        "purchaseweight": self.cartWeight, 
        "purchaseproducts": self.cartProducts, 
        "purchasecustomer": self.selectedCustomer, 
        "purchasecustomerid": self.selectedCustomer._id, 
        "status": null, 
        "createdby": user, 
        "createdat": moment().format(),
        "othercharges":self.othercharges,
        "challanno":self.challanno,
        "billno":self.billno,
        "purchasedate":self.purchasedate.toISOString(),
        "purchasenotes":self.purchaseNotes 
      }
    }else{
      purchaseDoc = {
        "_id": new Date().toISOString(),
        "purchasenumber": self.purchaseId, 
        "purchasetotal": self.cartTotal + self.othercharges, 
        "purchaseweight": self.cartWeight, 
        "purchaseproducts": self.cartProducts, 
        "purchasecustomer": self.selectedCustomer, 
        "purchasecustomerid": self.selectedCustomer._id, 
        "status": null, 
        "createdby": user, 
        "createdat": moment().format(),
        "othercharges":self.othercharges,
        "challanno":self.challanno,
        "billno":self.billno,
        "purchasedate":self.purchasedate,
        "purchasenotes":self.purchaseNotes 
      }
    }
    var storeFlag = false
    for (let doc of purchaseDoc.purchaseproducts){
      // console.log("DOC TO ADD:-----", doc)
      if (doc.cartstores === ""){
        storeFlag = true
        break
      }else{
        storeFlag = false
      }
    }
    // console.log("STORE FLAG VALUE:-----", storeFlag)

    var priceFlag = false
    for (let doc of purchaseDoc.purchaseproducts){
      // console.log("DOC TO ADD:-----", doc)
      if (doc.cartprice === 0){
        priceFlag = true
        break
      }else{
        priceFlag = false
      }
    }
    // console.log("PRICE FLAG VALUE:-----", priceFlag)
    // console.log("PURCHASE DOC TO PUT:-----", purchaseDoc)
    if (!storeFlag && !priceFlag){
      // self.selectedCustomer.balance = self.cartTotal
      self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - (self.cartTotal + self.othercharges)
      self.customerDebitCredit = {
        "_id": new Date().toISOString(),
        "name": self.customerToUpdate[0].name,
        "address": self.customerToUpdate[0].address,
        "phone": self.customerToUpdate[0].phone,
        "fax": self.customerToUpdate[0].fax,
        "email": self.customerToUpdate[0].email,
        "customerid": self.customerToUpdate[0]._id,
        "customerrev": self.customerToUpdate[0]._rev,
        "openingbalance": self.customerToUpdate[0].openingbalance,
        "closingbalance": self.customerToUpdate[0].closingbalance,
        "dcref": purchaseDoc.purchasenumber,
        "dcrefid": purchaseDoc._id,
        "dcrefdate": purchaseDoc.purchasedate,
        "dctype": "purchase",
        "challanno":self.challanno,
        "billno":self.billno,
        "status": null,
        "debit": null,
        "credit": null
      }
      if (self.customerToUpdate[0].closingbalance > 0){
        self.customerToUpdate[0].status = 'debit'
        self.customerDebitCredit.status = 'debit'
        self.customerDebitCredit.credit = -(self.cartTotal + self.othercharges)
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }else if (self.customerToUpdate[0].closingbalance < 0){
        self.customerToUpdate[0].status = 'credit'
        self.customerDebitCredit.status = 'credit'
        self.customerDebitCredit.credit = -(self.cartTotal + self.othercharges)
        self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
      }    

      // console.log("PURCHASE DOC TO PUSH:=====", purchaseDoc);
      self._purchaseService.addPurchase(purchaseDoc).then(function(result){
        // console.log("RESULT AFTER ADDING PURCHASE=======", result)
        var productDocs: any = []
        var purchaseDocs: any = []
        var itemDebitCredit: any = []
        for (let product of self.invoiceProducts){
          for (let doc of self.cartProducts){
            if (product._id === doc._id){
              if (doc.newstores != null){
                doc.newstores.stock = doc.cartquantity
                doc.stores.push(doc.newstores)
              }
              var sum = 0
              if (doc.newstores != null){
                for (let obj of doc.stores){
                  if (obj._id === doc.cartstores._id && doc.cartstores._id != doc.newstores._id){
                    obj.stock = obj.stock + doc.cartquantity
                  }  
                  sum = sum + obj.stock
                }
              }
              if (doc.newstores == null){
                for (let obj of doc.stores){
                  if (obj._id === doc.cartstores._id){
                    obj.stock = obj.stock + doc.cartquantity
                  }  
                  sum = sum + obj.stock
                }
              }

              doc.totatcartweight = doc.cartquantity * doc.weight
              doc.stockvalue = sum
              if (product.type === "Weighted"){
                doc.netweight = doc.stockvalue * doc.weight
                doc.networth = doc.stockvalue * doc.weight * doc.price
              }
              if (product.type === "Single Unit"){
                doc.networth = doc.stockvalue * product.price
              }
              var object = {
                "_id": UUID.UUID().substr(30, 35),
                "name":doc.name,
                "productid":doc._id,
                "productrev":doc._rev,
                "debit":null,
                "credit":doc.cartquantity,
                "createdat":new Date().toISOString(),
                "customer":self.selectedCustomer,
                "dcref": purchaseDoc.purchasenumber,
                "dcrefid": purchaseDoc._id,
                "dcrefdate": purchaseDoc.purchasedate,
                "dctype": "purchase",
                "challanno":self.challanno,
                "billno":self.billno,
                "stockvalue":doc.stockvalue
              }
              itemDebitCredit.push(object)
              var productDoc = {
                '_id': doc._id,
                '_rev': doc._rev,
                'name': doc.name,
                'batchnumber': doc.batchnumber,
                'weight': doc.weight,
                'price': doc.price,
                'category': doc.category,
                'categoryname': doc.categoryname,
                'stockvalue': doc.stockvalue,
                'type': doc.type,
                'subcategory': doc.subcategory,
                'stores': doc.stores,
                'storeStocks': doc.storeStocks,
                'netweight': doc.netweight,
                'networth': doc.networth
              }  
              purchaseDocs.push(productDoc)
              productDocs.push(product)
            }
          }
        }
        // console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
        // console.log("PRODUCT DOCS IN PURCHASE:=====", purchaseDocs);
        // console.log("ITEM DEBIT CREDIT:=====", itemDebitCredit);
        self._productService.updateInvoiceProducts(purchaseDocs).then(function(result){
          // console.log("RESULT AFTER UPDATEING PURCHASE PRODUCTS:--------", result)
          self.cartProducts = []
          self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
            // console.log("CUATOMER BALANCES AFTER UPDATE:-----", result)
            self._itemDebitCreditService.addDebitCredit(itemDebitCredit).then(function(result){
              // console.log("RESULT AFTER ADDING PRODUCT DEBIT CREDIT:-----", result)
              self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
                // console.log("RESULT AFTER UPDATING THE CUSTOMER DEBIT CREDIT:=====", result)
                self.selectedCustomer = null
                itemDebitCredit = []
                self.cartTotal = 0
                self.purchaseNotes = null
                self.customerDebitCredit = null
                self.customerToUpdate = null
                // self.purchaseId = Math.floor(Math.random()*1000) + 100000
                self.purchaseId = UUID.UUID().substr(30, 35)
                self.autoFiller = 1
                self.challanno = null
                self.billno = null
                self.othercharges = null
                self.purchasedate = null
                self.getCartProducts()
                self.getAllCustomers()
              }).catch(function(err){
                console.log(err)
              })
            }).catch(function(err){
              console.log(err)
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }
    if (storeFlag){
      this._matSnackBar.open('Must select a store', 'Undo', {
        duration: 3500
      });
    }
    if (priceFlag){
      this._matSnackBar.open('Price cannot be 0', 'Undo', {
        duration: 3500
      });
    }

  }

  setProductStore($event, product){
    this.selectedStore = $event.value
    var newStore = _.filter(this.allStores, {"_id": this.selectedStore})
    // console.log("Selected Store:----", $event, newStore[0])
    var exist = false
    for (let store of product.stores){
      if (store._id === $event.value){
        product.cartstores = store
        exist = true
      }
    }
    if (!exist){
      newStore[0].stock = product.cartquantity
      product.newstores = newStore[0]
      product.cartstores = newStore[0]
    }else{
      product.newstores = null
    }
    // console.log("CART PRODUCTS:----", this.cartProducts)
  }

  selectCustomer(state){

    this.selectedCustomer = state
    // console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate)
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this._buildAddCustomerForm()
    this.getAllCustomers()
  }


  closeAddProduct(){
    this.newProductFlag = false
    this._buildAddProductForm()
  }

  changeCustomer($event, selectedCustomer, query){

    selectedCustomer = $event
    // console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)

  }


  addNewProduct(values){

    var self = this;
    // console.log("VALUES FROM ADD PRODUCT FORM:", values)
    
    self.selectedStores = []
    for (let store of values.formstores){
      store.stock = 0
    }
    self.selectedStores = values.formstores

    var ID = Math.floor(Math.random()*100000000) + 800000000
    // console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    // console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    // console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    // console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
    // console.log("MULTI SELECT STORE VALUES:--", this.toppings.value);
    values._id = new Date().toISOString()
    var categoryname = _.filter(self.allCategories, {'_id':values.category})
    values.categoryname = categoryname[0].name
    // console.log("VALUES FROM ADD NEW PRODUCT:--", values);
    self._productService.addProduct(values).then(function(result){
      // console.log("RESULT AFTER ADDING THE PRODUCT:-------", result)
      self.addProductForm.reset()
      self.getCartProducts()
      self.newProductFlag = false
    }).catch(function(err){
      console.log(err)
    })
  }


  selectCartProduct(item){
    if (item.type === 'new_product'){
      this.newProductFlag = true
      this._buildAddInvoiceForm()
      this.getCartProducts()
    }else{
      // console.log("AUTO SELCT CHANGE FUNCTION", item)
      this.selecteProduct(item)
      // this._buildAddInvoiceForm()
    }

  }

  sortCart(cartProducts){

    var sortByProperty = function (property) {

        return function (x, y) {

            return ((parseInt(x[property]) === parseInt(y[property])) ? 0 : ((parseInt(x[property]) < parseInt(y[property])) ? 1 : -1));

        };

    };
    cartProducts.sort(sortByProperty('sortorder'))


  }


  selecteProduct(product){
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.cartquantity = product.cartquantity + 1
        if (product.type == 'Weighted'){
          product.calcweight = product.cartquantity * product.cartweight
          // product.cartweight = product.calcweight/product.cartquantity
          product.cartworth = product.cartquantity * product.cartweight * product.cartprice
        }else{
          product.calcweight = null
          product.cartworth = product.cartquantity * product.cartprice
        }

        // this.cartProducts.push(product)
        // this.calculateTotal(this.cartProducts)
        product.sortorder = this.autoFiller.toString()
        this.autoFiller++
        this.cartProducts.push(product)
        // this.cartProducts = _.reverse(this.cartProducts)
        // console.log("CART PRODUCTS:------", this.cartProducts)
        this.sortCart(this.cartProducts)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].cartquantity = this.cartProducts[i].cartquantity + 1
            if (this.cartProducts[i].type == 'Weighted'){
              this.cartProducts[i].calcweight = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight
              // this.cartProducts[i].cartweight = this.cartProducts[i].calcweight/this.cartProducts[i].cartquantity
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight * this.cartProducts[i].cartprice
            }else{
              this.cartProducts[i].calcweight = null
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            // console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.cartquantity = product.cartquantity + 1
          if (product.type == 'Weighted'){
            product.calcweight = product.cartquantity * product.cartweight
            // product.cartweight = product.calcweight/product.cartquantity
            product.cartworth = product.cartquantity * product.cartweight * product.cartprice
          }else{
            product.calcweight = null
            product.cartworth = product.cartquantity * product.cartprice
          }
          // this.cartProducts.push(product)
          // this.calculateTotal(this.cartProducts)
          product.sortorder = this.autoFiller.toString()
          this.autoFiller++
          this.cartProducts.push(product)
          // this.cartProducts = _.reverse(this.cartProducts)
          // console.log("CART PRODUCTS:------", this.cartProducts)
          this.sortCart(this.cartProducts)
          this.calculateTotal(this.cartProducts)
        }
      }

      this._buildAddInvoiceForm()
      this.getCartProducts()
    }
    
  }

  changeCalcWeight(calcweight, product){
    // console.log("CALC WEIGHT CAHFGES:_+_", calcweight, product)
    product.calcweight = calcweight
    product.cartweight = product.calcweight/product.cartquantity
    product.cartworth = product.calcweight * product.cartprice
    this.calculateTotal(this.cartProducts)
  }


  changeWeight(weight, product){
    product.cartweight = weight
    product.calcweight = product.cartquantity * product.cartweight
    product.cartworth = product.calcweight * product.cartprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    // console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    if (weight <= 0){
      product.cartweight = product.weight
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.calcweight * product.cartprice
      this._matSnackBar.open('Weight cannot be <= 0', 'Undo', {
        duration: 3500
      });
    }
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    // console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.cartquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        product.calcweight = product.cartquantity * product.cartweight
        // product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      this.calculateTotal(this.cartProducts)
    }
    if (quantity <= 0){
      product.cartquantity = 1
      this._matSnackBar.open('Quantity cannot be < 0', 'Undo', {
        duration: 3500
      });
    }
  }

  changePrice(price, product){
    product.cartprice = price
    if (product.type == 'Weighted'){
      // product.calcweight = product.cartquantity * product.cartweight
      product.cartweight = product.calcweight/product.cartquantity
      product.cartworth = product.cartquantity * product.cartweight * product.cartprice
    }else{
      product.calcweight = null
      product.cartworth = product.cartquantity * product.cartprice
    }

    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    if (price < 0){
      product.cartprice = 0
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight
        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }      
      this._matSnackBar.open('Price cannot be < 0', 'Undo', {
        duration: 3500
      });
    }
    this.calculateTotal(this.cartProducts)
  }




  calculateTotal(carItems){
    var total = 0  
    var weight = 0  
    for (let item of carItems){
      total = total + item.cartworth
      if (item.type == 'Weighted')
        weight = weight + item.calcweight
    }
    this.cartTotal = total
    this.cartWeight = weight
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  deleteCartItem(i, cartProducts){
    // console.log("DELETE CART ITEM CALLED:-", i, cartProducts)
    cartProducts.splice(i, 1)
    this.cartProducts = cartProducts
    this.calculateTotal(this.cartProducts)

  }


  filterStates(name: string) {
    // console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers(name: string) {
    // console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    // console.log("ALL Customers:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }

}
