import { Component, ElementRef, ViewChild, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../../../services/employees.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'add-employee',
    providers: [EmployeeService],
    templateUrl: 'addemployee.html',
    styleUrls: ['addemployee.css']
})

export class addEmployee {
  
  // public customersDB = new PouchDB('steelcustomers');
  @ViewChild('sidenav') sidenav: MatSidenav;
  @Output() addemployee = new EventEmitter();
  navMode = 'side';
  addEmployeeForm: FormGroup;

  constructor(private _employeeService: EmployeeService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddEmployeeForm();
  }

  private _buildAddEmployeeForm(){
    this.addEmployeeForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      salary: ['', Validators.required],
    })
  }

  ngOnInit(){
    var self = this;
  }

  addEmployee(values){
    var self = this;
    console.log("ADD EMPLOYEE CALLED:--", values);
    values._id = new Date().toISOString()
    self._employeeService.addEmployee(values).then(function(result){
      console.log("EMPLOYEE ADDED:===", result);
      self.addEmployeeForm.reset();
      self.addemployee.emit({'success': 'done'})
    }).catch(function(err){
      console.log(err);
    })
  }

  goBack(){
    this._router.navigate(['/admindashboard'])
  }

  // @HostListener('window:resize', ['$event'])
  //   onResize(event) {
  //       if (event.target.innerWidth < 886) {
  //           this.navMode = 'over';
  //           this.sidenav.close();
  //       }
  //       if (event.target.innerWidth > 886) {
  //          this.navMode = 'side';
  //          this.sidenav.open();
  //       }
  //   }
 
}