import { Component, ElementRef, ViewChild, HostListener, Input, Output, EventEmitter } from '@angular/core';
import {EmployeeService} from '../../services/employees.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
// import { MatSidenav } from "@angular/material";

export interface UserData {
  name ? : string;
  email ? : string;
  address ? : string;
  phone ? : string;
  salary ? : number;
  _id ? : string;
  _rev ? : string;

}


@Component({
    selector: 'employees',
    providers: [EmployeeService],
    templateUrl: 'employees.html',
    styleUrls: ['employees.css']
})

export class Employees {

  addEmployeeForm: FormGroup;
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  authUser: any = null

  displayedColumns = [
    'name', 
    'address', 
    'phone', 
    'email', 
    "salary",
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _employeeService: EmployeeService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddEmployeeForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
    // this.syncDb()
  }

  private _buildAddEmployeeForm(){
    this.addEmployeeForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      salary: ['', Validators.required],
    });
  }

  ngOnInit(){
    var self = this;
    this.loadSourceData()
  }

  loadSourceData(){
    var self = this;
    self._employeeService.allEmployees().then(function(result){
      console.log("ALL RESULTS FROM EMPLOYEES:=====", result);
      const users: UserData[] = [];
      result.rows.map(function(row){
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
    }).catch(function(err){
      console.log(err);
    })
  }


  refreshData($event){
    this.loadSourceData()
  }

}

function createNewUser(row): UserData {
  return {
    "_id": row._id,
    "_rev": row._rev,
    "name" : row.name,
    "address" : row.address,
    "phone" : row.phone,
    "salary" : row.salary,
    "email" : row.email
  };
}