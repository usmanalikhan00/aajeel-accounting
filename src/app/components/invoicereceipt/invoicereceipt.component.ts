import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import { CustomersService } from '../../services/customers.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { InvoiceService } from '../../services/invoice.service'
import { InvoiceReceiptService } from '../../services/invoicereceipt.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray, FormGroupDirective } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  receiptaccountname ? : string;
  receiptaccounttype ? : string;
  receiptnumber ? : string;
  invoicerefnumber ? : string;
  receiptamount ? : number;
  accountrefname ? : string;
  voucherno ? : string;
  customername ? : string;
  receiptdate ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'invoice-receipt',
    providers: [LoginService, 
                CustomersService,
                AccountTypesService,
                InvoiceReceiptService,
                AccountDebitCreditService,
                InvoiceService,
                ElectronService,
                CustomerDebitCreditService],
    templateUrl: 'invoicereceipt.html',
    styleUrls: ['invoicereceipt.css']
})

export class invoicereceipt {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild(FormGroupDirective) myForm;
  navMode = 'side';

  addReceiptForm: FormGroup;
  addCustomerForm: FormGroup;
  addAccountTypeForm: FormGroup;

  customerStates: Observable<any[]>;
  accCustomerStates: Observable<any[]>;
  accountStates: Observable<any[]>;
  invoiceStates: Observable<any[]>;
  customerAccountStates: Observable<any[]>;

  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  customerToUpdate: any = null
  customerDebitCredit: any = null
  allCustomerAccounts: any = []

  selectedRefAccount: any = null
  refAccountToUpdate: any = null

  accountDebitCredit: any = null
  selectedAccountTypes: any = []
  selectedAccount: any = null
  accountToUpdate: any = null
  
  allInvoices: any = []
  selectedInvoice: any = null

  accountTypeChangeValue: any = null
  authUser: any = null

  allTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    }
  ]
  allAccTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
    {
      'name':'Post Dated Cheque',
      'type':'post_dated_cheque'
    },
    {
      'name':'Parchi',
      'type':'parchi'
    },
    {
      'name':'Card',
      'type':'card'
    },
    {
      'name':'Cheque',
      'type':'cheque'
    }
  ]
  

  receiptTypes = [
    {
      'name':'Invoice',
      'type':'invoice'
    },
    {
      'name':'BPCC',
      'type':'bpcc'
    }
  ];

  receiptToSave = {
    _id:new Date().toISOString(),
    receiptnumebr:Math.floor(Math.random()*1000) + 100000,
    receiptcustomer:null,
    receiptcustomerid:null,
    receiptaccount:null,
    receiptaccountid:null,
    receiptamount:null,
    invoiceref:null,
    invoicerefid:null,
    accountref:null,
    accountrefid: null,
    receiptdate: null,
    receiptnotes: null,
    voucherno: null
  }


  addReceiptFlag: boolean = false
  addCustomerFlag: boolean = false
  addAccountFlag: boolean = false

  allInvoiceReceipts: any = []

  displayedColumns = [
    "datetime",
    "voucherno",
    'receiptnumber', 
    'customername', 
    'receiptaccounttype', 
    'receiptaccountname', 
    "receiptamount",
    "invoicerefnumber",
    "accountrefname",
    "action"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public invoicesReceiptsDB = new PouchDB('aajeelaccinvoicereceipts');
  public localInvoicesReceiptsDB = new PouchDB('http://localhost:5984/aajeelaccinvoicereceipts');
  public cloudantInvoicesReceiptsDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/aajeelaccinvoicereceipts', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  balanceTypes = ['Credit', 'Debit']
  // voucherno: any = null

  receiptdate: any = null
  receiptNotes: any = null

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _customersService: CustomersService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _accountTypesService: AccountTypesService, 
              private _invoiceReceiptService: InvoiceReceiptService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _electronService: ElectronService, 
              private _invoiceService: InvoiceService, 
              private _router: Router) {
    this._buildAddReceiptForm();
    this._buildAddCustomerForm();
    this._buildAddAccountTypeForm();
    this.loadDateSource()
    this.authUser = JSON.parse(localStorage.getItem('user'))


  }

  private _buildAddAccountTypeForm(){
    this.addAccountTypeForm = this._formBuilder.group({
      name: ['', Validators.required],
      accounttype: ['', Validators.required],
      accountnumber: ['', Validators.required],
      expirydate: ['', Validators.required],
      obtype: ['', Validators.required],
      customer: ['', Validators.required],
      refnumber: ['', Validators.required],
      openingbalance: ['', Validators.required],
      voucherno: [null]
    })
  }

  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }

  private _buildAddReceiptForm(){
    this.addReceiptForm = this._formBuilder.group({
      customer: ['', Validators.required],
      cashtype: ['', Validators.required],
      account: ['', Validators.required],
      receiptdate: [''],
      invoiceref: [''],
      receipttype: [''],
      customeraccref:[''],
      voucherno: [null]
    })
  }

  ngOnInit(){
    this.getAllCustomers()
    // this.getAllInvoiceReceipts()
    // this.getAllInvoices()
    // this.syncDb()
  }

  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.invoicesReceiptsDB, self.ibmInvoicesReceiptsDB, {
    //   live: true,
    //   retry: true
    // })
    var sync = self.invoicesReceiptsDB.sync(self.cloudantInvoicesReceiptsDB, {
              live: true,
              retry: true
    }).on('change', function (info) {
      console.log("CHANGE EVENT", info)
      if (info.direction === 'pull' || info.direction === 'push'){
        self.loadDateSource()
      }
      // handle change
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
      console.log("********PAUSE EVENT FROM INVOICE RTECEIPTS", err)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
      console.log("ACTIVE ACTIVE !!")
    }).on('denied', function (err) {
      // a document failed to replicate (e.g. due to permissions)
      console.log("DENIED DENIED !!", err)
    }).on('complete', function (info) {
      // handle complete
      console.log("COMPLETED !!", info)
    }).on('error', function (err) {
      // handle error
      console.log("ERROR ERROR !!", err)
    });    
  }

  getAllInvoiceReceipts(){
    var self = this
    self._invoiceReceiptService.allInvocieReceipts().then(function(result){
      console.log("RESULT AFTER ALL INVOICE RECEIPTS:---", result)
      self.allInvoiceReceipts = []
      result.rows.map(function(row){
        self.allInvoiceReceipts.push(row.doc)
      })
      console.log("ALL INVOICE RECEIPTS:---", self.allInvoiceReceipts)
    }).catch(function(err){
      console.log(err)
    })
  }

  goToPrint(row){
    // console.log("GO TO PRINT RESULT:000", row)
    // this._router.navigate(['printreceipt', row._id])
    this._electronService.ipcRenderer.send('ReceiptPrint', row._id)
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  loadDateSource(){
    var self = this
    self._invoiceReceiptService.allInvocieReceipts().then(function(result){
      console.log("RESULT AFTER ALL INVOICE RECEIPTS:---", result)
      // self.allInvoiceReceipts = []
      const users: UserData[] = [];
      result.rows.map(function(row){
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      // console.log("ALL INVOICE RECEIPTS:---", self.allInvoiceReceipts)
    }).catch(function(err){
      console.log(err)
    })
  }

  getAllInvoices(customer){
    var self = this;
    self.allInvoices = [];
    self._invoiceService.customerInvoices(customer).then(function(result){
      console.log("RESULT AFTER CUSTOMER INVOICES:=====", result);
      result.docs.forEach(function (row) { 
        self.allInvoices.push(row); 
      });
      self.invoiceStates = self.addReceiptForm.controls['invoiceref'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterInvoices(state) : self.allInvoices.slice());
      console.log("ALL INVOICES:=====", self.allInvoices);
    }).catch(function(err){
      console.log(err);
    })
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      self.customerStates = self.addReceiptForm.controls['customer'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.allCustomers.slice());
      self.accCustomerStates = self.addAccountTypeForm.controls['customer'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers2(state) : self.allCustomers.slice());
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  addCustomer(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }
    console.log("ADD CUSTOMER CALLED:--", values);
    self._customersService.addCustomer(values).then(function(result){
      console.log("Customer ADDED:===", result);
      self.addCustomerForm.reset();
      self.getAllCustomers()
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }


  enableAddCustomerFlag(){
    this.addCustomerFlag = true
    this.addAccountFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addAccountTypeForm.reset()
    this.addCustomerForm.reset()
    this.receiptToSave = {
      _id:new Date().toISOString(),
      receiptnumebr:Math.floor(Math.random()*1000) + 100000,
      receiptcustomer:null,
      receiptcustomerid:null,
      receiptaccount:null,
      receiptaccountid:null,
      receiptamount:null,
      invoiceref:null,
      invoicerefid:null,
      accountref:null,
      accountrefid: null,
      receiptdate: null,
      receiptnotes: null,
      voucherno: null
    }
  }

  enableAddAccountFlag(){
    this.addAccountFlag = true
    this.addCustomerFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addCustomerForm.reset()
    this.addAccountTypeForm.reset()
    this.receiptToSave = {
      _id:new Date().toISOString(),
      receiptnumebr:Math.floor(Math.random()*1000) + 100000,
      receiptcustomer:null,
      receiptcustomerid:null,
      receiptaccount:null,
      receiptaccountid:null,
      receiptamount:null,
      invoiceref:null,
      invoicerefid:null,
      accountref:null,
      accountrefid: null,
      receiptdate: null,
      receiptnotes: null,
      voucherno: null
    }
  }

  showReceiptForm(){
    this.addCustomerFlag = false
    this.addAccountFlag = false
    this.selectedCustomer = null
    this.customerToUpdate = null
    this.selectedRefAccount = null
    this.refAccountToUpdate = null
    this.selectedAccount = null
    this.accountToUpdate = null
    this.customerAccountStates = null
    this.addReceiptForm.reset()
    this.addCustomerForm.reset()
    this.addAccountTypeForm.reset()
    this.receiptToSave = {
      _id:new Date().toISOString(),
      receiptnumebr:Math.floor(Math.random()*1000) + 100000,
      receiptcustomer:null,
      receiptcustomerid:null,
      receiptaccount:null,
      receiptaccountid:null,
      receiptamount:null,
      invoiceref:null,
      invoicerefid:null,
      receiptdate:null,
      receiptnotes:null,
      accountref:null,
      accountrefid: null,
      voucherno: null
    }
  }

  selectAccCustomer(state){
    (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedCustomer = state
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate[0])
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this.getAllCustomers()
  }


  addAccountType(values, $event){
    var self = this;
    values._id = new Date().toISOString()
    // values.closingbalance = values.openingbalance
    if (values.accounttype === 'bank' || values.accounttype === 'cash'){
      if (values.obtype === 'Debit'){
        if (values.openingbalance != 0){
          values.closingbalance = values.openingbalance
          values.status = 'debit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }else if (values.obtype === 'Credit'){
        if (values.openingbalance != 0){
          values.openingbalance = -values.openingbalance
          values.closingbalance = values.openingbalance
          values.status = 'credit'
        }else{
          values.closingbalance = values.openingbalance
          values.status = null
        }
      }
      values.level = 'parent'
      values.parentid = null
      console.log("VALUES FOR ACCOUNT TO ADD:---", values)
      values.voucherno = null
      self._accountTypesService.addAccountType(values).then(function(result){
        console.log("result after adding account:---", result)
        // self.getAccountTypes()
        // self.loadSourceData()
        self._buildAddAccountTypeForm()
      }).catch(function(err){
        console.log(err)
      })
    }else{
      values.expirystatus = 'pending'
      values.customer = this.selectedCustomer
      values.customerid = values.customer._id
      values.remainbalance = values.openingbalance
      values.level = 'parent'
      values.parentid = null

      if (values.obtype === 'Debit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - values.openingbalance
          
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "dctype": values.accounttype,
          "dcrefid": values._id,
          "voucherno": values.voucherno,
          "status": null,
          "debit": null,
          "credit": null
        }
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.credit = -values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }       
      }else if (values.obtype === 'Credit'){
        self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance + values.openingbalance
        self.customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": self.customerToUpdate[0].name,
          "address": self.customerToUpdate[0].address,
          "phone": self.customerToUpdate[0].phone,
          "fax": self.customerToUpdate[0].fax,
          "email": self.customerToUpdate[0].email,
          "customerid": self.customerToUpdate[0]._id,
          "customerrev": self.customerToUpdate[0]._rev,
          "openingbalance": self.customerToUpdate[0].openingbalance,
          "closingbalance": self.customerToUpdate[0].closingbalance,
          "dcref": values.name,
          "dctype": values.accounttype,
          "dcrefid": values._id,
          "voucherno": values.voucherno,
          "status": null,
          "debit": null,
          "credit": null
        }      
        if (self.customerToUpdate[0].closingbalance > 0){
          self.customerToUpdate[0].status = 'debit'
          self.customerDebitCredit.status = 'debit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        }else if (self.customerToUpdate[0].closingbalance < 0){
          self.customerToUpdate[0].status = 'credit'
          self.customerDebitCredit.status = 'credit'
          self.customerDebitCredit.debit = values.openingbalance
          // self.customerDebitCredit.closingbalance = self.customerToUpdate[0].closingbalance
        } 
      }
      // values.expirydate = values.expirydate
      // (values.expirydate)
      values.expirydate = values.expirydate.toISOString()

      console.log("VALUES FROM ADD ACCOUNT TYPE FORM:--", values)
      console.log("CUSTOMER DEBIT CREDIT OBJECT:========", self.customerDebitCredit)
      console.log("ACTUAL CUSTOMER TO UPDATE:========", self.customerToUpdate[0])
      self._accountTypesService.addAccountType(values).then(function(result){
        console.log("result after adding account:---", result)
        self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
          console.log("result after adding customer debit credit object:---", result)
          self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
            console.log("result after updating customer balance:---", result)
            // self.getAccountTypes()
            self._buildAddAccountTypeForm()
            // self.myForm.resetForm()
            // self.loadSourceData()
            self.getAllCustomers()
            self.selectedCustomer = null
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }
  }

  reciptDateChange(e){
    console.log("RECIPT DATE CHANGES:---", e.value)
    this.receiptdate = e.value.toISOString()
    console.log("DATE TO ENTER:---", this.receiptdate)
    this.receiptToSave.receiptdate = this.receiptdate
  }

  typeChange(value){
    (<FormGroup>this.addAccountTypeForm).patchValue({'obtype':''}, { onlySelf: true });
    (<FormGroup>this.addAccountTypeForm).patchValue({'openingbalance':''}, { onlySelf: true });
    this.selectedCustomer = null
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    if (value === 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });

    }
    if (value !== 'bank'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':''}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':''}, { onlySelf: true });
    }
    if (value === 'cash'){
      (<FormGroup>this.addAccountTypeForm).patchValue({'accountnumber':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'expirydate':'1111-11-11'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'customer':'none'}, { onlySelf: true });
      (<FormGroup>this.addAccountTypeForm).patchValue({'refnumber':'none'}, { onlySelf: true });
    }

  }

  showAddReceipt(){
    this.addReceiptFlag = true
  }

  hideAddReceipt(){
    this.addReceiptFlag = false
    this.loadDateSource()
  }


  getReceiptType($event){
    console.log("RECEIPT TYPE IS:-----", $event)
    if ($event.value === 'bpcc'){
      (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':'none', 'customeraccref':''}, {onlySelf: true})
      // (<FormGroup>this.addReceiptForm).patchValue({'customeraccref':''}, {onlySelf: true})
      this.selectedInvoice = null
      this.receiptToSave.invoiceref = null
      this.receiptToSave.invoicerefid = null
    }else{
      (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':'', 'customeraccref':'none'}, {onlySelf: true})
      // (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, {onlySelf: true})
      this.selectedRefAccount = null
      this.receiptToSave.accountref = null
      this.receiptToSave.accountrefid = null

    }
    console.log("RECEIPT TOS SAVE AFTER CHANGING RECEIPT TYPE:======", this.receiptToSave)
  }

  getAllCustomerAccounts(customerid){
    var self = this;
    self.allCustomerAccounts = [];
    self._accountTypesService.getCustomerAccounts(customerid).then(function(result){
      console.log("ALL CUSTOMER ACCOUNTS:=====", result);
      result.docs.forEach(function (row) { 
        self.allCustomerAccounts.push(row); 
      });
      self.customerAccountStates = self.addReceiptForm.controls['customeraccref'].valueChanges
        .startWith(null)
        .map(state => state ? self.filterCustomerAccount(state) : self.allCustomerAccounts.slice());
      console.log("ALL CUSTOMER ACCOUNTS:=====", self.allCustomerAccounts);
    }).catch(function(err){
      console.log(err);
    })
  }

  selectCustomer(state){
    (<FormGroup>this.addReceiptForm).patchValue({'customer':''}, { onlySelf: true });
    (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, { onlySelf: true });
    // console.log("SELECETD STATE FROM CUSTOMER:----", state)
    this.selectedCustomer = state
    this.customerToUpdate = _.filter(this.allCustomers, {"_id": this.selectedCustomer._id})
    // console.log("CUSTOMER TO UPDATE IS: ------", this.customerToUpdate)
    // console.log("SELECTED CUSTOMER IS: ------", this.selectedCustomer)
    this.receiptToSave.receiptcustomer = state
    this.receiptToSave.receiptcustomerid = state._id
    console.log("RECEIPT TO SAVE IS: ------", this.receiptToSave)
    this.getAllCustomers()
    this.getAllInvoices(this.selectedCustomer)
    this.getAllCustomerAccounts(this.selectedCustomer._id)
  }

  selectAccount(state){
    (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    // console.log("STATE FREOM ACOUNT HTML:=====", state)
    this.selectedAccount = state
    this.accountToUpdate = _.filter(this.selectedAccountTypes, {"_id": this.selectedAccount._id})
    // console.log("ACCOUNT TO UPDATE IS: ------", this.accountToUpdate[0])
    // console.log("SELECTED ACCOUNT IS: ------", this.selectedAccount)
    this.receiptToSave.receiptaccount = this.selectedAccount
    this.receiptToSave.receiptaccountid = this.selectedAccount._id
    console.log("RECEIPT TO SAVE IS: ------", this.receiptToSave)
    this.accountTypeChange(this.accountTypeChangeValue)
    // this.accountStates = this.addReceiptForm.controls['account'].valueChanges
    //     .startWith(null)
    //     .map(state => state ? this.filterAccounts(state) : this.selectedAccountTypes.slice());
    // this.accou
  }
  
  selectInvoice(state){
    (<FormGroup>this.addReceiptForm).patchValue({'invoiceref':''}, { onlySelf: true });
    // console.log("INVOICE STATE FREOM HTML:=====", state)
    this.selectedInvoice = state
    this.selectedInvoice = _.filter(this.allInvoices, {"_id":this.selectedInvoice._id})
    // console.log("SELECTED INVOICE IS: ------", this.selectedInvoice)
    this.receiptToSave.invoiceref = this.selectedInvoice[0]
    this.receiptToSave.invoicerefid = this.selectedInvoice[0]._id
    console.log("RECEIPT TO SAVE IS: ------", this.receiptToSave)
    this.getAllInvoices(this.selectedCustomer)
    // this.allCustomerAccounts(this.selectedCustomer._id)
    // this.invoiceStates = this.addReceiptForm.controls['invoiceref'].valueChanges
    //     .startWith(null)
    //     .map(state => state ? this.filterInvoices(state) : this.allInvoices.slice());
  }

  selectReferenceAccount(state){
    (<FormGroup>this.addReceiptForm).patchValue({'customeraccref':''}, { onlySelf: true });
    this.selectedRefAccount = state
    this.refAccountToUpdate = _.filter(this.allCustomerAccounts, {"_id":this.selectedRefAccount._id})
    console.log("SELECTED REFERNCE ACCOUNT IS: ------", this.refAccountToUpdate)
    console.log("CHANGE ACCOUNT IS: ------", this.selectedRefAccount)
    // this.getAllCustomerAccounts(this.selectedCustomer._id)
    this.receiptToSave.accountref = this.refAccountToUpdate[0]
    this.receiptToSave.accountrefid = this.refAccountToUpdate[0]._id
    console.log("RECEIPT TO SAVE IS: ------", this.receiptToSave)
    // this.customerAccountStates = this.addReceiptForm.controls['customeraccref'].valueChanges
    //     .startWith(null)
    //     .map(state => state ? this.filterCustomerAccount(state) : this.allCustomerAccounts.slice());
    this.getAllCustomerAccounts(this.selectedCustomer._id)
  }


  accountTypeChange(value){
    (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    // (<FormGroup>this.addReceiptForm).patchValue({'account':''}, { onlySelf: true });
    var self = this
    console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    self.accountTypeChangeValue = value
    self.selectedAccountTypes = []
    self._accountTypesService.getAccountSubAccounts(value).then(function(result){
      console.log("RESULT FROM SINGLE ACCOUNT ENTRIES:=======", result)
      result.docs.forEach(function(row){
        self.selectedAccountTypes.push(row)
      })
      self.accountStates = self.addReceiptForm.controls['account'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterAccounts(state) : self.selectedAccountTypes.slice());
      console.log("ALL ACCOUNTS OF SELECTED TYPE:=======", self.selectedAccountTypes)
    }).catch(function(err){
      console.log(err)
    })
  }

  changePrice($event){
    console.log("ENETERED RECEIPT PRICE IS:---------", $event.target.value)
    if (this.selectedRefAccount){
      if ($event.target.value > this.selectedRefAccount.remainbalance){
        console.log("--:YES GREATER VALUE ENTERED:--")
        this.receiptToSave.receiptamount = null
      }
    }
  }

  getReceiptNotes($event){
    console.log("RECEIPT NOTES:---", $event)
    this.receiptToSave.receiptnotes = $event
  }

  makeReceipt(value){
    this.receiptToSave.receiptcustomer = null
    this.receiptToSave.receiptcustomerid = null
    value.customer = this.selectedCustomer
    value.account = this.selectedAccount
    value.invoiceref = this.selectedInvoice
    this.receiptToSave.receiptcustomer = this.selectedCustomer
    this.receiptToSave.receiptcustomerid = this.selectedCustomer._id
    this.receiptToSave.receiptaccount = this.selectedAccount
    this.receiptToSave.receiptaccountid = this.selectedAccount._id
    this.receiptToSave.invoiceref = this.selectedInvoice[0]
    this.receiptToSave.invoicerefid = this.selectedInvoice[0]._id
    console.log("RECEIPT FORM BUTTON CLICKED:======", this.addReceiptForm.value)
    console.log("RECEIPT TOS SAVE IS:======", this.receiptToSave)
  }


  addInvoiceReceipt(){
    
    var self = this
    self.receiptToSave.receiptnotes = self.receiptNotes
    console.log("RECEIPT DOCUMNET TO SAVE:========", self.receiptToSave)
    // CUSTOMER DETAILS FOR UPDATION
    self.customerToUpdate[0].closingbalance = self.customerToUpdate[0].closingbalance - self.receiptToSave.receiptamount
    self.customerDebitCredit = {
      "_id": new Date().toISOString(),
      "name": self.customerToUpdate[0].name,
      "address": self.customerToUpdate[0].address,
      "phone": self.customerToUpdate[0].phone,
      "fax": self.customerToUpdate[0].fax,
      "email": self.customerToUpdate[0].email,
      "customerid": self.customerToUpdate[0]._id,
      "customerrev": self.customerToUpdate[0]._rev,
      "openingbalance": self.customerToUpdate[0].openingbalance,
      "closingbalance": self.customerToUpdate[0].closingbalance,
      "dcref": self.receiptToSave.receiptnumebr,
      "dcrefid": self.receiptToSave._id,
      "receiptdate": self.receiptToSave.receiptdate,
      "dcrefname": self.accountToUpdate[0].name,
      "dctype": "receipt",
      "voucherno":self.addReceiptForm.controls['voucherno'].value,
      "notes": self.receiptNotes,
      "status": null,
      "debit": null,
      "credit": null
    }
    
    if (self.customerToUpdate[0].closingbalance > 0){
      self.customerToUpdate[0].status = 'debit'
      self.customerDebitCredit.status = 'debit'
      self.customerDebitCredit.credit = -self.receiptToSave.receiptamount
    }else if (self.customerToUpdate[0].closingbalance < 0){
      self.customerToUpdate[0].status = 'credit'
      self.customerDebitCredit.status = 'credit'
      self.customerDebitCredit.credit = -self.receiptToSave.receiptamount
    } 
    console.log("CUSTOMER DEBIT CREDIT OBJECT:========", self.customerDebitCredit)
    console.log("ACTUAL CUSTOMER TO UPDATE:========", self.customerToUpdate[0])
    // ACCOUNT DETAILS FOR UPDATE
    self.accountToUpdate[0].closingbalance = self.accountToUpdate[0].closingbalance + self.receiptToSave.receiptamount
    self.accountDebitCredit = {
      _id:new Date().toISOString(),
      accountid:self.accountToUpdate[0]._id,
      accountnumber:self.accountToUpdate[0].accountnumber,
      accountrev:self.accountToUpdate[0]._rev,
      accounttype:self.accountToUpdate[0].accounttype,
      openingbalance:self.accountToUpdate[0].openingbalance,
      closingbalance:self.accountToUpdate[0].closingbalance,
      customer:self.customerToUpdate[0],
      customerid:self.customerToUpdate[0]._id,
      dcref: self.receiptToSave.receiptnumebr,
      dcrefid: self.receiptToSave._id,
      receiptdate: self.receiptToSave.receiptdate,
      voucherno:self.addReceiptForm.controls['voucherno'].value,
      notes: self.receiptNotes,
      dctype: "receipt",            
      obtype:self.accountToUpdate[0].obtype
    }
    self.accountDebitCredit.credit = null
    self.accountDebitCredit.accountid = self.accountToUpdate[0]._id
    self.accountDebitCredit.accountrev = self.accountToUpdate[0]._rev

    if (self.accountToUpdate[0].closingbalance > 0){
      self.accountToUpdate[0].status = 'debit'
      self.accountDebitCredit.status = 'debit'
      self.accountDebitCredit.debit = self.receiptToSave.receiptamount
    }else if (self.accountToUpdate[0].closingbalance < 0){
      self.accountToUpdate[0].status = 'credit'
      self.accountDebitCredit.status = 'credit'
      self.accountDebitCredit.debit = self.receiptToSave.receiptamount
    }
    console.log("ACCOUNT DEBIT CREDIT OBJECT:========", self.accountDebitCredit)
    console.log("ACTUAL ACCOUNT TO UPDATE:========", self.accountToUpdate[0])
    // CHEQUE CARD PARCHI ACCOUN TDETAILS FOR UPDATE
    if (self.refAccountToUpdate){
      self.refAccountToUpdate[0].remainbalance = self.refAccountToUpdate[0].remainbalance - self.receiptToSave.receiptamount
      if (self.refAccountToUpdate[0].remainbalance === 0){
        self.refAccountToUpdate[0].expirystatus = 'process'
        console.log("REFRENCE ACCOUNT STATUS UPDATED... YAHOOOO!!!!! :)", self.refAccountToUpdate[0])
      } 
    }
    self.receiptToSave.voucherno = self.addReceiptForm.controls['voucherno'].value
    // QUERIES AND RESULTS, SETTING THE TEMPLATE DEFAULTS 
    self._invoiceReceiptService.addInvoiceReceipt(self.receiptToSave).then(function(result){
      console.log("RESULT AFTER ADDING INVOICE RECEIPT:--------", result)
      self._customerDebitCreditService.addDebitCredit(self.customerDebitCredit).then(function(result){
        console.log("RESULT AFTER ADDING CUSTOMER DEBIT CREDIT:--------", result)
        self._customersService.updateCustomerBalance(self.customerToUpdate[0]).then(function(result){
          console.log("RESULT AFTER UPDATING CUSTOMER BALANCE:--------", result)
          self._accountDebitCreditService.addDebitCredit(self.accountDebitCredit).then(function(result){
            console.log("RESULT AFTER ADDING ACCOUNT DEBIT CREDIT:--------", result)
            self._accountTypesService.updateAccountBalance(self.accountToUpdate[0]).then(function(result){
              console.log("RESULT AFTER UPDATING ACCOUNT BALANCE:--------", result)
              if (self.refAccountToUpdate){
                self._accountTypesService.updateAccountBalance(self.refAccountToUpdate[0]).then(function(result){
                  console.log("CHEQUE PARCHI CARD ACCOUNT UPDATED:--------", result)
                  // this.accountTypeChange(this.accountTypeChangeValue)
                  self.resetDefaults()
                }).catch(function(err){
                  console.log(err)
                })  
              }else{
                self.resetDefaults()
              }
            }).catch(function(err){
              console.log(err)
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).then(function(err){
      console.log(err)
    })
  }


  resetDefaults(){
    var self = this
    self.addReceiptForm.reset()
    self.receiptToSave = {
      _id:new Date().toISOString(),
      receiptnumebr:Math.floor(Math.random()*1000) + 100000,
      receiptcustomer:null,
      receiptcustomerid:null,
      receiptaccount:null,
      receiptaccountid:null,
      receiptamount:null,
      invoiceref:null,
      invoicerefid:null,
      accountref: null,
      accountrefid: null,
      receiptdate: null,
      receiptnotes: null,
      voucherno: null
    }
    self.selectedCustomer = null
    self.customerToUpdate = null
    self.accountToUpdate = null
    self.selectedAccount = null
    self.selectedInvoice = null
    self.selectedRefAccount = null
    self.refAccountToUpdate = null
    self.accountTypeChangeValue = null
    self.accountDebitCredit = null
    self.customerDebitCredit = null
    self.receiptNotes =  null
    self.loadDateSource()
    self.getAllCustomers()
  }

  filterCustomers(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addReceiptForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }
  
  filterCustomers2(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addAccountTypeForm.controls['customer'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomerAccount(name: string) {
    console.log("ALL Customers FROM FILTER:=====", this.addReceiptForm.controls['customeraccref'].value);
    return this.allCustomerAccounts.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }
  
  filterAccounts(name: string) {
    console.log("ALL ACCOUNTS FROM FILTER:=====", this.addReceiptForm.controls['account'].value);
    return this.selectedAccountTypes.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterInvoices(name: string) {
    console.log("ALL INVOICES FROM FILTER:=====", this.addReceiptForm.controls['invoiceref'].value);
    return this.allInvoices.filter(state =>
      state.invoicenumber.toString().toLowerCase().indexOf(name.toString().toLowerCase()) > -1);
  }


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {
  // console.log("ROW TO SHOW FOR PAYMENTS", row)
  if (row.invoiceref === null){
    if (row.accountref === null){
      if (row.voucherno){

        return {
            "receiptnumber" : row.receiptnumebr.toString(),
            "customername" : row.receiptcustomer.name.toString(),
            "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
            "receiptaccountname" : row.receiptaccount.name.toString(),
            "receiptamount" : row.receiptamount,
            "invoicerefnumber" : null,
            "accountrefname" : null,
            "voucherno" : row.voucherno,
            "receiptdate" : row.receiptdate,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }else{
        return {
            "receiptnumber" : row.receiptnumebr.toString(),
            "customername" : row.receiptcustomer.name.toString(),
            "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
            "receiptaccountname" : row.receiptaccount.name.toString(),
            "receiptamount" : row.receiptamount,
            "receiptdate" : row.receiptdate,
            "invoicerefnumber" : null,
            "accountrefname" : null,
            "voucherno" : null,
            "_id" : row._id,
            "_rev" : row._rev
        };


      }
    }else{
      if (row.voucherno){

        return {
            "receiptnumber" : row.receiptnumebr.toString(),
            "customername" : row.receiptcustomer.name.toString(),
            "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
            "receiptaccountname" : row.receiptaccount.name.toString(),
            "receiptamount" : row.receiptamount,
            "invoicerefnumber" : null,
            "accountrefname" : row.accountref.name.toString(),
            "receiptdate" : row.receiptdate,
            "voucherno" : row.voucherno,
            "_id" : row._id,
            "_rev" : row._rev
        };
      }else{
        return {
            "receiptnumber" : row.receiptnumebr.toString(),
            "customername" : row.receiptcustomer.name.toString(),
            "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
            "receiptaccountname" : row.receiptaccount.name.toString(),
            "receiptdate" : row.receiptdate,
            "receiptamount" : row.receiptamount,
            "invoicerefnumber" : null,
            "voucherno" : null,
            "accountrefname" : row.accountref.name.toString(),
            "_id" : row._id,
            "_rev" : row._rev
        };


      }
    }
  }
  if (row.accountref === null){
    if (row.voucherno){

      return {
          "receiptnumber" : row.receiptnumebr.toString(),
          "customername" : row.receiptcustomer.name.toString(),
          "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
          "receiptaccountname" : row.receiptaccount.name.toString(),
          "receiptdate" : row.receiptdate,
          "receiptamount" : row.receiptamount,
          "invoicerefnumber" : row.invoiceref.invoicenumber.toString(),
          "accountrefname" : null,
          "voucherno": row.voucherno,
          "_id" : row._id,
          "_rev" : row._rev
      };
    }else{
      return {
          "receiptnumber" : row.receiptnumebr.toString(),
          "customername" : row.receiptcustomer.name.toString(),
          "receiptaccounttype" : row.receiptaccount.accounttype.toString(),
          "receiptaccountname" : row.receiptaccount.name.toString(),
          "receiptdate" : row.receiptdate,
          "receiptamount" : row.receiptamount,
          "invoicerefnumber" : row.invoiceref.invoicenumber.toString(),
          "accountrefname" : null,
          "voucherno": null,
          "_id" : row._id,
          "_rev" : row._rev
      };


    }
  } 

}