import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import {InvoiceService} from '../../services/invoice.service'
import {PurchaseService} from '../../services/purchase.service'
import {ProductService} from '../../services/product.service'
// import { User } from '../../models/model-index'
import { AccountTypesService } from '../../services/accounttypes.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

import * as _ from 'lodash'

@Component({
    selector: 'admin-dashboard',
    providers: [LoginService,
                InvoiceService,
                PurchaseService,
                ProductService,
                CustomerDebitCreditService,
                AccountTypesService],
    templateUrl: 'admindashboard.html',
    styleUrls: ['admindashboard.css']
})

export class admindashboard {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  users: any = [];
  allAccounts: any = [];
  allProducts: any = [];
  allInvoices: any = [];
  allInvoicesDC: any = [];
  allPurchases: any = [];
  allPurchasesDC: any = [];
  totalStockValue: any = 0
  totalInvoice: any = 0
  totalPurchases: any = 0

  addProductForm: FormGroup;
  authUser: any = null

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _productService: ProductService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _accountTypesService: AccountTypesService, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){
    
  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.authUser = JSON.parse(localStorage.getItem('user'))
    console.log("AUTH USER:-------", this.authUser)
    this.getAllAccounts()
    this.profitLossAccount()
    this.getCustomerInvoiceDC()
    this.getCustomerPurchaseDC()
  }


  getCustomerInvoiceDC(){
    var self = this
    var invoiceDC = 0
    self._customerDebitCreditService.getInvoiceDebitCredits().then(function(result){
      self.allInvoicesDC = result.docs
      for (let obj of self.allInvoicesDC){
        var dateKey = _.filter(self.allInvoices, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].invoicedate
        if (dateKey[0])
          invoiceDC++
      }
      console.log("ALL DEBIT/CREDIT FOR INVOICE:-----", self.allInvoicesDC)
      console.log("UPDATED INVOICE COUNT:-----", invoiceDC)
      // self.updateInvoiceDC(self.allInvoicesDC)
    }).catch(function(err){
      console.log(err)
    })
  }


  updateInvoiceDC(allInvoicesDC){
    var self = this
    self._customerDebitCreditService.updateDebitCredit(allInvoicesDC).then(function(result){
      console.log("RESULT AFTER UPDATING ALL INVOICES DEBIT/CREDIT:---", result)
    }).then(function(err){
      console.log(err)
    })
  }

  getCustomerPurchaseDC(){
    var self = this
    var purchaseDC = 0
    self._customerDebitCreditService.getPurchaseDebitCredits().then(function(result){
      self.allPurchasesDC = result.docs
      for (let obj of self.allPurchasesDC){
        var dateKey = _.filter(self.allPurchases, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].purchasedate
        if (dateKey[0])
          purchaseDC++
      }
      console.log("ALL DEBIT/CREDIT FOR PUCRHASE:-----", self.allPurchasesDC)
      console.log("UPDATED PURCHASE COUNT:-----", purchaseDC)
      // self.updateInvoiceDC(self.allPurchasesDC)
    }).catch(function(err){
      console.log(err)
    })


  }

  getAllAccounts(){
    var self = this
    self._accountTypesService.getPendingAccounts().then(function(result){
      console.log("ALL PENDING ACCOUNTS:--------", result)
      self.allAccounts = []
      result.docs.forEach(function(row){
        // if ()
        let diffLeadtime = moment(row.expirydate).diff(moment().format('YYYY-MM-DD'),'days');
        console.log("DIFF IN LEAD TIME:--", diffLeadtime)
        row.notice = diffLeadtime
        self.allAccounts.push(row)
        console.log("ALL ACCOUNTS AFTER DIFFERENCE WITH CURRENT TIME:--------", self.allAccounts)
      })
    }).catch(function(err) {
      console.log(err)
    })
  }

  profitLossAccount(){
    var self = this
    self._productService.allProducts().then(function(result){
      var stockSum = 0
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc)
        stockSum = stockSum + row.doc.networth 
      });
      self.totalStockValue = stockSum
      console.log("ALL PRODCTS * SUM:--", self.allProducts, self.totalStockValue)
      self._invoiceService.allInvocies().then(function(result){
        var stockSum = 0
        result.rows.map(function (row) { 
          self.allInvoices.push(row.doc); 
          stockSum = stockSum + row.doc.invoicetotal 
        });
        self.totalInvoice = stockSum
        console.log("ALL INVOICES:--", self.allInvoices, self.totalInvoice)
        self._purchaseService.allPurchases().then(function(result){
          var stockSum = 0
          result.rows.map(function (row) { 
            self.allPurchases.push(row.doc); 
            stockSum = stockSum + row.doc.purchasetotal 
          });
          self.totalPurchases = stockSum
          console.log("ALL PURCHASES:--", self.allPurchases, self.totalPurchases)
        }).catch(function(err) {
          console.log(err)
        })
      }).catch(function(err) {
        console.log(err)
      })    
    }).catch(function(err) {
      console.log(err)
    })
  }

  goToAccounts(){
    this._router.navigate(['/cashsettings'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }  
}